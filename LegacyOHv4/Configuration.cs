using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;


namespace OH.DAL.LegacyOHv4
{
    using System.Data.Entity.Migrations;

    internal sealed class LegacyOHv4Config : DbMigrationsConfiguration<OH.DAL.LegacyOHv4Context>
    {
        public LegacyOHv4Config()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"LegacyOHv4";
        }

        protected override void Seed(OH.DAL.LegacyOHv4Context context)
        {

        }
    }


}
