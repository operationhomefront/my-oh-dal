namespace OH.DAL.LegacyOHv4
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.USZipCodes",
                c => new
                    {
                        ZipCode = c.String(nullable: false, maxLength: 250),
                        City = c.String(maxLength: 250),
                        State = c.String(maxLength: 250),
                        County = c.String(maxLength: 250),
                        Location = c.Geography(),
                    })
                .PrimaryKey(t => t.ZipCode);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.USZipCodes");
        }
    }
}
