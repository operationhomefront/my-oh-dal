﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.DAL
{
    public class LegacyOHv4Repository<T> : ILegacyOHv4Repository<T> where T : class
    {
        private readonly LegacyOHv4Context _legacyOHv4Context;
        private bool _disposed;

        public LegacyOHv4Repository(LegacyOHv4Context legacyOHv4Context)
        {
            _legacyOHv4Context = legacyOHv4Context;
        }

        protected DbSet<T> DbSet
        {
            get { return _legacyOHv4Context.Set<T>(); }
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

        public T GetById(string id)
        {
            return DbSet.Find(id);
        }


        public string AddOrUpdate(T entity)
        {
            var idProperty = entity.GetType().GetProperty("ZipCode");

            var id = idProperty.GetValue(entity);

            //Testing this out, there should no longer be any entity instances with
            //a null or empty Guid
            //An entity id should never be equal to an empty guid
            //if (id.Equals(Guid.Empty))
            //{
            //    throw new Exception(String.Format("The id cannot be equal to {0}", id));
            //}

            //We could check for empty guid to make this check faster
            //but checking if the entity exists in the Context is safer
            //if ((Guid)id == Guid.Empty)
            if (DbSet.Find(id) == null)
            {
                //Testing this out, there should no longer be any entity instances with
                //a null or empty Guid
                //if (id.Equals(Guid.Empty))
                //{
                //    idProperty.SetValue(entity, Guid.NewGuid());
                //}

                DbSet.Add(entity);
            }
            else
            {

                _legacyOHv4Context.Entry(entity).State = EntityState.Modified;
                _legacyOHv4Context.SaveChanges();
            }

            return (string)idProperty.GetValue(entity);
        }

        public void Delete(string id)
        {
            T entity = DbSet.Find(id);
            DbSet.Remove(entity);
        }

        public void Save()
        {
            try
            {
                _legacyOHv4Context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }

        }

        public string GetLastInsertedId()
        {
            //var val = DbSet.Max(x=>x.)
            //return DbSet.FirstOrDefault(x => x.ClusterId == val);
            return string.Empty;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _legacyOHv4Context.Dispose();
                }
            }
            _disposed = true;
        }

        //TODO: Get By ClassGuid is incomplete
        //public IEnumerable<object> GetAllEntitiesByGuid(object guid)
        //{
        //    //Type type = typeof(object);

        //    //var propertyInfoArray = type.GetProperties().Where(t => t.PropertyType.Name.Contains("DbSet"));           

        //    //var propertyInfoArray  = type.AllEntities.Where(t=>t.GetType().GetProperties
        //    //    BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

        //    //foreach (var propertyInfo in propertyInfoArray)
        //    //{
        //    //    myOHContext(propertyInfo.GetValue(myOHContext);

        //    //}

        //    //foreach (var item in myOHContext.AllEntities)
        //    //{
        //    //    //var propertyInfoArray = item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);  
        //    //    //var type = typeof(item);
        //    //    //propertyInfoArray = propertyInfoArray;
        //    //    var propertyInfo = item.GetType().GetProperties();
        //    //    propertyInfo = propertyInfo;
        //    }

        //    //myOHContext.AllEntities.Where(t=>t.GetType().GetProperties
        //    //    BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly)

        //    return null;
        //}

    }
}
