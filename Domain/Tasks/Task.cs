﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Tasks
{
    [Serializable]
    [Table("Tasks")] 
    public class Task
    {

        public Task()
        {
            Id = Guid.NewGuid();
            Start = DateTime.UtcNow;
            End = DateTime.UtcNow;
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("7df85419-7e3c-4160-9b50-fb629b31a0e1"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? OwnerId { get; set; }
        public bool IsAllDay { get; set; }
        public string RecurrenceRule { get; set; }
        public int? RecurrenceId { get; set; }
        public string RecurrenceException { get; set; }
        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }

        public virtual ICollection<Task> SubTasks { get; set; }
        public virtual Task ParentTask { get; set; }
    }
}
