﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.Categories;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.ChangeLogs
{
    [Serializable]
    [Table("ChangeLogs")]
    public class ChangeLog
    {
        public ChangeLog()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid { get { return new Guid("C80B43C5-F696-43CE-8AB3-0103B85418DB"); } }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public Guid EntityGuid { get; set;}

        [Required]
        public TimeStamp TimeStamp { get; set; }

        [MaxLength(2500)]
        public string Comment { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }
    }
}
