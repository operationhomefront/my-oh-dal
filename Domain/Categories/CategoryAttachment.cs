﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Attachments;

namespace OH.DAL.Domain.Categories
{
    //TODO: Rename this this class to CategoryAttachment 
    [Serializable]
    [Table("CategoryAttachmentMapping")]
    public class CategoryAttachment
    {
        public CategoryAttachment()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid { get { return new Guid("F72D496C-668F-4141-8FEA-700AAABD41BA"); } }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public Guid CategoryId { get; set; } 

        public virtual Category Category { get; set; }

        public Guid AttachmentId { get; set; }       

        public virtual Attachment Attachment { get; set; }        
    }
}