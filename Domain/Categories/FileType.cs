﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Categories
{
    [Serializable]
    [Table("FileTypes")]
    public class FileType
    {
        public FileType()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid { get { return new Guid("E0974042-1779-4AEC-B401-39620925FAF5"); } }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public string Description { get; set; }

        public string Extension { get; set; }

        public string CssClass { get; set; }
    }
}
