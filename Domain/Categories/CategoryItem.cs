﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Categories
{
    [Serializable]
    [Table("CategoryItems")] //TODO: Pluralize the table name
    public class CategoryItem
    {

        public CategoryItem()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("728b2d47-6d90-4fa1-b25d-8eb6ff9ae766"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        
        public string Text { get; set; }
        public string SpriteCssClass { get; set; }
        public string ImageUrl { get; set; }
        public bool Expanded { get; set; }
        public bool HasChildren { get; set; }
        
        public virtual CategoryItem ParentCategoryItem { get; set; }
        
        [ForeignKey("ParentCategoryItem")]
        public virtual IEnumerable<CategoryItem> Items { get; set; }

    }
}
