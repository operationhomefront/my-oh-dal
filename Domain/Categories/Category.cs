﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using OH.DAL.Domain.Applications;
using OH.DAL.Domain.ChangeLogs;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Categories
{
    [Serializable]
    [Table("Categories")]
    public class Category
    {
        public Category()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("6C2CC673-495E-469A-8583-9E071FD078B0"); }
        }


        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid? ParentCategoryId { get; set; }
        public virtual Category ParentCategory { get; set; }

        public String Name { get; set; }

        public virtual Person Owner { get; set; }

        public virtual ICollection<Application> Applications { get; set; }

        public virtual ChangeLog ChangeLog { get; set; }

        public virtual ICollection<CategoryAttachment> CategoryAttachments { get; set; }

        public virtual ICollection<Category> SubCategories { get; set; }

        [NotMapped]
        public IEnumerable<TreeViewItemViewModel> TreeViewItems

        {
            get
            {
                return SubCategories.Select(c => c.TreeViewItem).Union(
                    CategoryAttachments.Select(
                        ca =>
                            new TreeViewItemViewModel
                            {
                                id = ca.Attachment.Id.ToString(),
                                text = ca.Attachment.FileName,
                                hasChildren = false,
                                spriteCssClass = GetCssClassByExtension(ca.Attachment.FileExtension)
                            })
                    ).AsEnumerable();
            }
        }

        [NotMapped]
        public TreeViewItemViewModel TreeViewItem

        {
            get
            {
                return new TreeViewItemViewModel
                {
                    id = Id.ToString(),
                    text = Name,
                    hasChildren = SubCategories.Any(),
                    items = TreeViewItems,
                    spriteCssClass = ParentCategory == null ? "rootfolder" : "folder"
                };
            }
        }

        public string GetCssClassByExtension(string itemText)
        {
     

            switch (itemText)
            {
                case ".pdf":
                    return "pdf";
                case ".html":
                    return "html";
                case ".txt":
                    return "html";
                case ".jpeg":
                    return "image";
                default:
                    return "folder";
            }
        }
    }

    public class TreeViewItemViewModel
    {
        public string id { get; set; }
        public string text { get; set; }
        public string spriteCssClass { get; set; }
        public string imageUrl { get; set; }
        public bool expanded { get; set; }
        public bool hasChildren { get; set; }
        public IEnumerable<TreeViewItemViewModel> items { get; set; }


        public TreeViewItemViewModel Clone()
        {
            var clone = new TreeViewItemViewModel
            {
                id = id,
                imageUrl = imageUrl,
                spriteCssClass = spriteCssClass,
                text = text,
                expanded = expanded,
                hasChildren = hasChildren
            };
            return clone;
        }      
    }
}

//TODO: Delete
//    items = CategoryAttachments.Select(
//            ca =>
//                new TreeViewItemViewModel
//                {
//                    id = ca.Attachment.Id.ToString(),
//                    text = ca.Attachment.FileName,
//                    hasChildren = false
//                })

//};

//return new TreeViewItemViewModel
//{
//    id = Id.ToString(),
//    text = Name,
//    hasChildren = SubCategories.Any(),
//    items = 

//    SubCategories.Select(
//    ca =>
//                new TreeViewItemViewModel
//                {
//                    id = ca.Id.ToString(),
//                    text = ca.Name,
//                    hasChildren = true
//                }
//    ).Union(
//        CategoryAttachments.Select(
//            ca =>
//                new TreeViewItemViewModel
//                {
//                    id = ca.Attachment.Id.ToString(),
//                    text = ca.Attachment.FileName,
//                    hasChildren = true
//                })
//    )

//};