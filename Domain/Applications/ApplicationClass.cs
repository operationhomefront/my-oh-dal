﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Applications
{
    [Serializable]
    [Table("ApplicationClasses")] 
    public class ApplicationClass
    {

        public ApplicationClass()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("7de95c69-0a36-442e-9add-25621a46806e"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public Guid ApplicationId { get; set; }
        public virtual Application Application { get; set; }
        
        public Guid EntityClassGuid { get; set; }

        [NotMapped]
        public string EntityName { get; set; }
    }
}
