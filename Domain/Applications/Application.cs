﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Categories;

namespace OH.DAL.Domain.Applications
{
    [Serializable]
    [Table("Applications")]
    public class Application
    {

        public Application()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("71d0545f-fa07-4968-a237-8cf0f5116cf3"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string NameAbbreviation { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

    }
}
