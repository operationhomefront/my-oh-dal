﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Events
{
    [Serializable]
   
    public class EventTypes :ListType
    {

        public EventTypes()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("d9661e96-0dd0-4a19-a29b-db402870492c"); }
        }

     
    }
}
