﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Events
{
    [Serializable]
    public class RegistationStatus : ListType
    {

        public RegistationStatus()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("8352d76a-2ec2-4b68-a3ff-99389c8c30d8"); }
        }

       
    }
}
