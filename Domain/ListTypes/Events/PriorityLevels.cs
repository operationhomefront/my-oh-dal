﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Events
{
    [Serializable]
    public class PriorityLevels : ListType
    {

        public PriorityLevels()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("f52f12bd-7a15-4385-a358-42d59079e72c"); }
        }

       

    }
}
