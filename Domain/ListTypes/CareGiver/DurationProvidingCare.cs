﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.CareGiver
{
    [Serializable]
    public class DurationProvidingCare :ListType
    {

        public DurationProvidingCare()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("bdd3ae54-e8eb-426f-9f70-2e766e171b5e"); }
        }

     
    }
}
