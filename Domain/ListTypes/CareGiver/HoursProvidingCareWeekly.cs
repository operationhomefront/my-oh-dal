﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.CareGiver
{
    [Serializable]
    public class HoursProvidingCareWeekly : ListType
    {

        public HoursProvidingCareWeekly()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("bebc166c-366d-4a5a-96ed-edeb40a88190"); }
        }

      

    }
}
