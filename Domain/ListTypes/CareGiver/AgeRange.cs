﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.CareGiver
{
    [Serializable]
    public class AgeRange :ListType
    {

        public AgeRange()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("f930f021-be01-4122-ade6-951c8b08ffe7"); }
        }

       
    }
}
