﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Person
{
    [Serializable]
    public class Relationship : ListType
    {

        public Relationship()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("c8997a19-f6d8-4adb-96a5-c5d989ea8c4e"); }
        }

    
    }
}
