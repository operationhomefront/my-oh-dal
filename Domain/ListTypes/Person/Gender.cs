﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Person
{
    [Serializable]
    public class Gender :ListType
    {

        public Gender()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("a1b36136-9d9c-4670-ab4c-a9f03dfc3a5f"); }
        }


    }
}
