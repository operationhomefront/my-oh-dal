﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Military
{
    [Serializable]
    public class ListMilitaryRank : ListType
    {

        public ListMilitaryRank()
        {
            base.Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("31dc412a-0f70-4de1-b854-9a092144760a"); }
        }

        


    }
}
