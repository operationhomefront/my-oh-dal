﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Policy;

namespace OH.DAL.Domain.ListTypes.Military
{
    [Serializable]
    public class ListMilitaryVeteranStatus : ListType
    {

        public ListMilitaryVeteranStatus()
        {
            base.Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("28741c44-bcae-4e4f-bffc-995c40881175"); }
        }
    }
}
