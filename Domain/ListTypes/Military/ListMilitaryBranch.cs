﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Policy;

namespace OH.DAL.Domain.ListTypes.Military
{
    [Serializable]
    
    public class ListMilitaryBranch: ListType
    {

        public ListMilitaryBranch()
        {
            Id = Guid.NewGuid();
            OptionalValue = false.ToString();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("b78b7763-218f-4f6d-b746-6266ace69341"); }
        }
        
       

    }
}
