﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Military
{
    [Serializable]
    public class ListMilitaryDeploymentStatus : ListType
    {

        public ListMilitaryDeploymentStatus()
        {
            base.Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("b35e6c02-3858-423f-9bc0-248199409e0a"); }
        }

      

        
    }
}
