﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.ListTypes;

namespace OH.DAL.Domain.ListTypes.Military
{
    [Serializable]
    
   public class ListMilitaryCombatTheater : ListType
    {

        public ListMilitaryCombatTheater()
        {
            base.Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("7c207f54-e800-464d-984f-0e1440bbc82a"); }
        }

      

    }
}
