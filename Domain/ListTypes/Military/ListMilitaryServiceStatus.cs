﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Military
{
    [Serializable]
    public class ListMilitaryServiceStatus : ListType
    {

        public ListMilitaryServiceStatus()
        {
            base.Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("fc30933d-6414-4838-86ce-a37351cb7527"); }
        }

       
      

    }
}
