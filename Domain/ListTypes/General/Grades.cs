﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.General
{
    [Serializable]
    public class Grades :ListType
    {

        public Grades()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("96360689-c617-44dc-8b27-7ee288758cc2"); }
        }

    }
}
