﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.General
{
    [Serializable]
    public class ShirtSizes : ListType
    {

        public ShirtSizes()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("26bc6c02-6f0c-4d68-9dcb-70c157bb57d3"); }
        }

       

    }
}
