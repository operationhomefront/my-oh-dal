﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Categories;
using OH.DAL.Repositories.Categories;

namespace OH.DAL.Domain.ListTypes
{
    [Serializable]
    [Table("ListTypes")]
    public class ListType
    {
        public ListType()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("387B2347-E0DB-4DA7-A09F-E7270F3BA2AE"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }
      
        public string Name { get; set; }

        public int? SortOrder { get; set; }

        public bool? ShowInCombo { get; set; }

        public int? EnumValue { get; set; }

        public Guid? CategoryListTypeId { get; set; }

        public string OptionalValue { get; set; }
       
        public object ObjectSource { get; set; }

    }
}
