﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Injury
{
    public class ListInjuryInflictionCategory : ListType
    {

        public ListInjuryInflictionCategory()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("6dbeab3e-fabf-4270-962c-95dcdedbf290"); }
        }

        

    }
}
