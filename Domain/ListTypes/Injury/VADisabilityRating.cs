﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ListTypes.Injury
{
    [Serializable]
    public class VADisabilityRating :ListType
    {

        public VADisabilityRating()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("6e478c86-be78-4f98-8ded-7fc4576cb101"); }
        }


    }
}
