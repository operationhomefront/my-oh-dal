﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.ProgramProfile.Injury;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Persons
{
    [Serializable]
    [Table("PersonMilitary")] //TODO: Pluralize the table name
    public class PersonMilitary
    {
        public PersonMilitary()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("a32f36af-f389-4e2a-8377-fe323055b821"); }
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid? BranchOfServiceId { get; set; }
        public string CommandPost { get; set; }
        public Guid DeploymentStatusId { get; set; }
        public ICollection<InjuryInfliction> InjuryInflictions { get; set; }
        public Guid? MilitaryRankId { get; set; }
        public bool NeedsUpdataing { get; set; }
        public Guid? ServiceStatusId { get; set; }
        public virtual TimeStamp TimeStamp { get; set; }
        public Guid? VeteranStatusId { get; set; }
        public bool Wounded { get; set; }
        public int YearsServed { get; set; }
        public Guid PersonId { get; set; }
        public virtual Person MilitaryPerson { get; set; }
    }
}
