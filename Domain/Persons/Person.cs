﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Events;
using OH.DAL.Domain.Phones;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Persons
{
    //[Serializable]
    [Table("Persons")]
    public class Person
    {
        public Person()
        {
            Id = Guid.NewGuid();
           
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("7758ED58-E93E-46DA-85C0-33E5F626CE92"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        
        public string FirstName { get; set; }

        public string Lastname { get; set; }

        public string MiddleName { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateOfBirth { get; set; }

        public string Title { get; set; }

        public string Suffix { get; set; }

        public string Gender { get; set; }

        //TODO: Make sure this is hashed in the database
        public string DriverLicenceNumber { get; set; }

        public string DriverLicenseState { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DriverLicenseExpirationDate { get; set; }

        public string Last4Ssn { get; set; }

        //TODO: Make sure this is hashed in the database
        public string SocialSecurityNumber { get; set; }

        public Guid? PrimaryEmailId { get; set; }
        public virtual EmailAddress PrimaryEmail { get; set; }

        public Guid? PrimaryAddressId { get; set; }
        public virtual Address PrimaryAddress { get; set; }
        //public virtual PersonAddress PhysicalAddress { get; set; }
        //public virtual PersonAddress MailingAddress { get; set; }

        public Guid? PrimaryPhoneId { get; set; }
        public virtual Phone PrimaryPhone { get; set; }

        public virtual TimeStamp TimeStamp { get; set; }
        
        public virtual ICollection<PersonPhone> Phones { get; set; }

        public ICollection<PersonAddress> Addresses { get; set; }

        public ICollection<PersonEmailAddress> EmailAddresses { get; set; }

        public Guid? ShirtSizeId { get; set; }

    }
}
