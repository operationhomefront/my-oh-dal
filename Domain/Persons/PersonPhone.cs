﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Phones;

namespace OH.DAL.Domain.Persons
{
    [Serializable]
    [Table("PersonPhoneMapping")]
    public class PersonPhone
    {
        public PersonPhone()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("56CE3397-0C4B-41CA-9413-C8864228E053"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid PhoneId { get; set; }

        public virtual Phone Phone { get; set; }

        public Guid PersonId { get; set; }

        public virtual Person Person { get; set; }       
    }
}
