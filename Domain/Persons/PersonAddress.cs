﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Addresses;

namespace OH.DAL.Domain.Persons
{
    [Serializable]
    [Table("PersonAddressMapping")]
    public class PersonAddress
    {
        public PersonAddress()
        {
            Id = Guid.NewGuid();
            //NOTE:cannot force a new address here, 
            //if pulling from existing person address 
            //then it was overwriting the existing virtual address!!!
            //var address = new Address();//do not instantiate new address in DAL constructor
            
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("9D89926B-4B48-4F50-A90F-63AA0FB7A3AC"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }
        
        public Guid AddressId { get; set; }
        public virtual Address Address { get; set; }
        
        public Guid PersonId { get; set; }
        public virtual Person Person { get; set; }
        
        public Guid? AddressType { get; set; }
    }
}
