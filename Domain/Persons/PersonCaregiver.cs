﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.ProgramProfile.Caregiver;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Persons
{
    [Serializable]
    [Table("PersonCaregiver")] //TODO: Pluralize the table name
    public class PersonCaregiver 
    {
        public PersonCaregiver()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("a8a5a119-db1a-4449-aa0f-542fb7efef8e"); }
        }


        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        


         public string AgeRange { get; set; } //todo: possibly remove this later
         //public virtual IEnumerable<Attachments> Attachments { get; set; } todo:add later
         public Guid? DurationOfTimeProvidingCare { get; set; }
         public string GreatestChallengeAsCaregiver { get; set; }
         public bool HasChildren { get; set; }
         public string HobbiesOrInterests { get; set; }
         public Guid? HoursProvidingCareWeekly { get; set; }
         public int HowManyChildren { get; set; }
         public bool NeedsUpdataing { get; set; }
         public bool NotifyIfOpportunity { get; set; }
         public string Occupation { get; set; }
         public bool ProfileActive { get; set; }
         public bool RegisteredCaregiverWithVa { get; set; }
         public Guid? RelationToVeteran { get; set; }
         public bool SendNewsletter { get; set; } //todo: should be moved to default workflow status 
         public string TellAboutWork { get; set; }
         public virtual TimeStamp TimeStamp { get; set; }
         public bool WorkOutsideHome { get; set; }
         public Guid PersonId { get; set; }
         public virtual Person Person { get; set; }

    }
}



