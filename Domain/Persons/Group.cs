﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Persons
{
    [Serializable]
    [Table("Groups")] 
    public class Group
    {

        public Group()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("8c4ad8b2-fbb0-44c1-a0bc-6c5bd6947e1e"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public virtual ICollection<Person> Members { get; set; }

        public virtual ICollection<Person> Leaders { get; set; }



    }
}
