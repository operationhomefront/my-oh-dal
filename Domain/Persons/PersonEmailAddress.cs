﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.EmailAddresses;

namespace OH.DAL.Domain.Persons
{
    [Serializable]
    [Table("PersonEmailAddresses")]
    public class PersonEmailAddress
    {
        public PersonEmailAddress()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("8BFC567C-4929-45E5-9019-FF6D16863248"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }

        public Guid EmailAddressId { get; set; }

        public virtual EmailAddress EmailAddress { get; set; }

        public Guid PersonId { get; set; }

        public virtual Person Person { get; set; }        
    }
}