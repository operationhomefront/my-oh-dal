﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormFileUploadProperties")] //TODO: Pluralize the table name
    public class OHFormFileUploadProperties
    {

        public OHFormFileUploadProperties()
        {
            Id = Guid.NewGuid();
            AllowedExtensions = "";
            UploadButtonText = "Select a file to upload.";
            MaxKbFileSize = 1024;
            UploaderHtml = "";
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("2005b0a5-52b3-4917-85ef-d6fc7260801e"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        
        public String AllowedExtensions { get; set; }
        public String UploadButtonText { get; set; }
        public Int32 MaxKbFileSize { get; set; }
        public String UploaderHtml { get; set; }

    }
}
