﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormValue")] //TODO: Pluralize the table name
    public class OHFormValue
    {

        public OHFormValue()
        {
            Id = Guid.NewGuid();
            DataValue = "";
            DataName = "";
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("8abe487d-10a4-4637-bf4a-9a75ef823764"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public virtual OHForm Form { get; set; }
        public String DataValue { get; set; }
        public String DataName { get; set; }
        public virtual OHFormControl Control { get; set; }

    }
}
