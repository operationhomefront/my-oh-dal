﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    public enum OHFormNumberType
    {
        None = 0,
        Integer = 1,
        Currency = 2
    }
}
   
