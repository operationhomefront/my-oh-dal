﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormDescriptionProperties")] //TODO: Pluralize the table name
    public class OHFormDescriptionProperties
    {

        public OHFormDescriptionProperties()
        {
            Id = Guid.NewGuid();
            Description = "";
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("4b4a99a6-9287-4b43-8d50-fe5b442de10a"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        //[UIHint(@"tinymce_jquery_full"), AllowHtml]
        public string Description { get; set; }
    }
}
