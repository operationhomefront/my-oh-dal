﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormUmbracoProperties")] //TODO: Pluralize the table name
    public class OHFormUmbracoProperties
    {

        public OHFormUmbracoProperties()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("914dfc03-86bb-4d22-920b-6a997e3823e0"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        public int UmbracoId { get; set; }
    }
}
