﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormDateTimeProperties")] //TODO: Pluralize the table name
    public class OHFormDateTimeProperties
    {

        public OHFormDateTimeProperties()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("0acbcd4f-2923-4b51-be01-99f1dd891764"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public String DateType { get; set; }
        public DateTime DefaultDate { get; set; }


    }
}
