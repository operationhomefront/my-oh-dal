﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormSelectListProperties")] //TODO: Pluralize the table name
    public class OHFormSelectListProperties
    {

        public OHFormSelectListProperties()
        {
            Id = Guid.NewGuid();
            SelectListOptions = "Item1,Item2,Item3,Item4";
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("365f32db-e7ab-4958-a63c-33143f991458"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        public String SelectListOptions { get; set; }

    }
}
