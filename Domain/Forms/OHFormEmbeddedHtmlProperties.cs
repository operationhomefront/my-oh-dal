﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormEmbeddedHtmlProperties")] //TODO: Pluralize the table name
    public class OHFormEmbeddedHtmlProperties
    {

        public OHFormEmbeddedHtmlProperties()
        {
            Id = Guid.NewGuid();
            RawHtml = "";
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("d97a039c-4668-41db-bebb-8eea4766910a"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        public String RawHtml { get; set; }


    }
}
