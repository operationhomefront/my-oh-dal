﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormNumberProperties")] //TODO: Pluralize the table name
    public class OHFormNumberProperties
    {

        public OHFormNumberProperties()
        {
            Id = Guid.NewGuid();
            Type = OHFormNumberType.Integer;
            MinValue = 0;
            MaxValue = 1000000;
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("acea515b-6941-4aa6-94ec-989ca270793c"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public OHFormNumberType Type { get; set; }
        public Int32 MinValue { get; set; }
        public Int32 MaxValue { get; set; }
    }
}
