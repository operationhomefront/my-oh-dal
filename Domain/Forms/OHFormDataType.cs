﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    //[Serializable]
    //[Table("OHFormDataType")] //TODO: Pluralize the table name
    //public class OHFormDataType
    //{

    //    public OHFormDataType()
    //    {
    //        Id = Guid.NewGuid();
    //    }

    //    [NotMapped]
    //    public static Guid ClassGuid
    //    {
    //        get { return new Guid("8361610e-5e9c-4eb4-b5e0-1641a1180851"); }
    //    }

    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public int ClusterId { get; set; }

    //    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    //    public Guid Id { get; private set; }

    //}


    public enum OHFormDataType
    {
        String,
        Int,
        Double,
        Decimal,
        File
    }

    public static class OHFormDataTypes
    {

        public static String Name(this OHFormDataType d)
        {
            var name = "";
            switch (d)
            {
                case OHFormDataType.String:
                    name = "String";
                    break;
                case OHFormDataType.Int:
                    name = "Int";
                    break;
                case OHFormDataType.Double:
                    name = "Double";
                    break;
                case OHFormDataType.Decimal:
                    name = "Decimal";
                    break;
                case OHFormDataType.File:
                    name = "File";
                    break;
            }
            return name;
        }
        public static Dictionary<String, Enum> ComboList()
        {
            var comboList = new Dictionary<String, Enum>
								{
									{"String"	    , OHFormDataType.String},
									{"Int"		    , OHFormDataType.Int},
									{"Double"		, OHFormDataType.Double},
									{"Decimal"		, OHFormDataType.Decimal},
									{"File"		    , OHFormDataType.File},
								};
            return comboList;
        }

    }


}
