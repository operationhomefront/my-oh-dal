﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormSection")] //TODO: Pluralize the table name
    public class OHFormSection
    {

        public OHFormSection()
        {
            Id = Guid.NewGuid();
            SectionTitle = "New Section";
            SectionDescription = "";
            SectionControls = new List<OHFormControl>(); 
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("17ea9d9f-007d-428b-b262-f653031143aa"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }


        public String SectionTitle { get; set; }
        public String SectionDescription { get; set; }

        public virtual List<OHFormControl> SectionControls { get; set; }

    }
}
