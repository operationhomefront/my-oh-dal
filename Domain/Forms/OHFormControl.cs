﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormControl")]   //TODO: Pluralize the table name
    public class OHFormControl
    {

        public OHFormControl()
        {
            Id = Guid.NewGuid();
            Type = OHFormControlType.Undefined;
            FormDataType = OHFormDataType.String;
            Required = false;
            OriginalValue = null;
            Label = "";
            DataName = "";
            FullWidthProperties = false;
            TimeStamp = new TimeStamp();
            SequencerId = 0;
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("c105777c-7f99-4ffe-beb8-2b7b2b3ce8d9"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }


        public OHFormControlType Type { get; set; }
        public int SequencerId { get; set; }
        public OHFormDataType FormDataType { get; set; }
        public Boolean Required { get; set; }
        public Object OriginalValue { get; set; }
        public String Label { get; set; }
        public String DataName { get; set; }
        public Boolean FullWidthProperties { get; set; }
        public dynamic Properties { get; set; }
        public TimeStamp TimeStamp { get; set; }
        public String Fields { get; set; }
        public virtual OHForm Form { get; set; }
        public virtual OHFormSection Section { get; set; }
        public virtual OHFormDescriptionProperties DescriptionProperties { get; set; }
        public virtual OHFormEmbeddedHtmlProperties EmbeddedHtmlProperties { get; set; }
        public virtual OHFormCheckBoxProperties CheckBoxProperties { get; set; }
        public virtual OHFormRadioButtonsProperties RadioButtonsProperties { get; set; }
        public virtual OHFormServiceMemberProperties ServiceMemberProperties { get; set; }
        public virtual OHFormUmbracoProperties UmbracoProperties { get; set; }
        public virtual OHFormDateTimeProperties DateTimeProperties { get; set; }
        public virtual OHFormSelectListProperties SelectListProperties { get; set; }
        public virtual OHFormNumberProperties NumberProperties { set; get; }

        public virtual OHFormFileUploadProperties FileUploadProperties  { set; get; }
        public Boolean Validate { get; set; }
           


        public enum OHFormControlType
        {
            Undefined = 0,
            Address,
            CheckBox,
            CreditCard,
            DateTime,
            Description,
            EmailAddress,
            EmbeddedHtml,
            FileUpload,
            ShoppingCartItem,
            ShortTextAnswer,
            LongTextAnswer,
            Name,
            Number,
            PhoneNumber,
            RadioButtons,
            SelectList,
            ServiceMember,
            Location,
            Section,
            PhotoRelease,
            LiabilityRelease,
            Dependents,
            Timeslots,
            Registrant,
            GiftAgreement,
            Agreement
        };

      


    }
}
