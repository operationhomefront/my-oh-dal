﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHForm")]   //TODO: Pluralize the table name
    public class OHForm
    {

        public OHForm()
        {
            Id = Guid.NewGuid();
             Name                = "";
            SubmitButtonText    = "Submit";
            CancelButtonText    = "Cancel";
            Theme               = new OHFormTheme();
            TimeStamp           = new TimeStamp();
            Controls            = new List<OHFormControl>();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("A02F9723-9004-41D4-A34B-1A804E2E0D83"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }


        [Required]
        public String           Name                { get; set; }
        public String           SubmitButtonText    { get; set; }
        public String           CancelButtonText    { get; set; }
        public OHFormTheme        Theme               { get; set; }
        public TimeStamp        TimeStamp           { get; set; }

        //public virtual List<Section> Sections { get; set; } 
        public virtual List<OHFormControl> Controls { get; set; }

       

    }
}
