﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormCheckBoxProperties")] //TODO: Pluralize the table name
    public class OHFormCheckBoxProperties
    {

        public OHFormCheckBoxProperties()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("8d0d2bdd-e8db-496a-a6bb-128f0b367f21"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public String Text { get; set; }
        public Boolean Checked { get; set; }
    }
}
