﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Forms
{
    [Serializable]
    [Table("OHFormRadioButtonsProperties")] //TODO: Pluralize the table name
    public class OHFormRadioButtonsProperties
    {

        public OHFormRadioButtonsProperties()
        {
            Id = Guid.NewGuid();
            Orientation = ButtonOrientation.Vertical;
            RadioName = "RadioButton1";
            RadioValues = "Option1,Option2,Option3";
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("8726a432-53fc-4d02-aa82-b7902cb1fe70"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public ButtonOrientation Orientation { get; set; }
        //public List<RbProperty>     RbList      { get; set; }
        public String RadioName { get; set; }
        public String RadioValues { get; set; }

    }
    public enum ButtonOrientation
    {
        Horizontal,
        Vertical
    }
}
