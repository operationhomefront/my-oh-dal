﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using OH.DAL.Domain.TimeStamps;
using System.Drawing;
using System.Web;
namespace OH.DAL.Domain.Forms
{

    [Serializable]
    [Table("OHFormTheme")] //TODO: Pluralize the table name
    public class OHFormTheme
    {

        public OHFormTheme()
        {
            Id = Guid.NewGuid();
            Name = "default";
            BackgroundColor = Color.FromName("transparent");
            ShowBorder = true;
            BorderColor = ColorTranslator.FromHtml("#DFD9C9");
            TopOfFormImage = "";
            CornerRadius = 5;
            FormRowEven = Color.FromName("transparent");
            FormRowOdd = Color.FromName("transparent");
            FormRowSelected = ColorTranslator.FromHtml("#EAE4D5");
            FontColor = Color.FromName("black");
            FontName = "Arial";
            FontSize = "12pt";
            SectionBgColor = ColorTranslator.FromHtml("#F5F2ED");
            ShowSectionBorder = true;
            SectionBorderColor = ColorTranslator.FromHtml("#DFD9C9");
            SectionFontColor = Color.FromName("black");
            SectionFontName = "Arial";
            SectionFontSize = "11pt";
            ButtonColor = ColorTranslator.FromHtml("#13558A");
            ButtonFontColor = Color.FromName("white");
            TimeStamp = new TimeStamp();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("df2db038-19a1-471c-8b1d-92ecdfce3a2d"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }


        public String Name { get; set; }
        //
        // Form
        public Color BackgroundColor { get; set; }
        public Boolean ShowBorder { get; set; }
        public Color BorderColor { get; set; }
        public String TopOfFormImage { get; set; }
        public Int32 CornerRadius { get; set; }
        //
        // Row
        public Color FormRowEven { get; set; }
        public Color FormRowOdd { get; set; }
        public Color FormRowSelected { get; set; }
        //
        // Labels
        public Color FontColor { get; set; }
        public String FontName { get; set; }
        public String FontSize { get; set; }
        //
        // Sections
        public Color SectionBgColor { get; set; }
        public Boolean ShowSectionBorder { get; set; }
        public Color SectionBorderColor { get; set; }
        public Color SectionFontColor { get; set; }
        public String SectionFontName { get; set; }
        public String SectionFontSize { get; set; }
        public TimeStamp TimeStamp { get; set; }
        //
        // Buttons
        public Color ButtonColor { get; set; }
        public Color ButtonFontColor { get; set; }


        public Boolean CreateCss(String folderPath)
        {
            //
            return false;

            //if (String.IsNullOrEmpty(Name))
            //    return false;
            ////
            //// Convert to physical file path
            //var fileSysDirPath = HttpContext.Current.Server.MapPath(folderPath);
            //var cssFilePath = String.Format(@"{0}\{1}.css", fileSysDirPath, Name);

            ////
            //// Try and create the file path if it doesn't exist.
            //try
            //{
            //    if (!Directory.Exists(fileSysDirPath))
            //        Directory.CreateDirectory(fileSysDirPath);
            //}
            //catch
            //{
            //    return false;
            //}

            //try
            //{
            //    using (var sw = new StreamWriter(cssFilePath))
            //    {
            //        //
            //        // Form background color, border, border color, border radius
            //        if (ShowBorder)
            //            sw.WriteLine(@".form-theme {{ background-color:{0}; border: 1px solid {1}; border-radius:{2}px; }}", BackgroundColor.ToRgbaHtml(), BorderColor.ToRgbaHtml(), CornerRadius);
            //        else
            //            sw.WriteLine(@".form-theme {{ background-color:{0}; border: none; border-radius:{1}px; }}", ColorTranslator.ToHtml(BackgroundColor), CornerRadius);
            //        //
            //        // Selected Form Row
            //        sw.WriteLine(@".selected-theme {{ background-color: {0} !important; border: 1px solid {1}; border-radius:{2}px; }}", FormRowSelected.ToRgbaHtml(), BorderColor.ToRgbaHtml(), CornerRadius);

            //        //
            //        // Even Form Rows
            //        if (FormRowEven != Color.FromName("transparent"))
            //            sw.WriteLine(@".form-row:nth-child(even) {{ background-color: {0}; }}", FormRowEven.ToRgbaHtml());

            //        //
            //        // Odd Form Rows
            //        if (FormRowOdd != Color.FromName("transparent"))
            //            sw.WriteLine(@".form-row:nth-child(odd) {{ background-color: {0}; }}", FormRowOdd.ToRgbaHtml());

            //        //
            //        // Form Row Label
            //        sw.WriteLine(@".label-theme {{ font-family:{0}; font-size:{1}; color:{2}; }}", FontName, FontSize, FontColor.ToRgbaHtml());

            //        //
            //        // Section background color, border, border color, border radius
            //        if (ShowSectionBorder)
            //            sw.WriteLine(@".section-theme {{ background-color: {0}; border: 1px solid {1}; border-radius: {2}px; }}", SectionBgColor.ToRgbaHtml(), SectionBorderColor.ToRgbaHtml(), CornerRadius);
            //        else
            //            sw.WriteLine(@".section-theme {{ background-color: {0}; border: none; border-radius: {1}px; }}", SectionBgColor.ToRgbaHtml(), CornerRadius);

            //        //
            //        // Section title
            //        sw.WriteLine(@".ctl-section-theme {{ font-family:{0}; font-size:{1}; color:{2}; }}", SectionFontName, SectionFontSize, SectionFontColor.ToRgbaHtml());

            //        //
            //        // Submit/Cancel Buttons
            //        sw.WriteLine(@".button-theme {{ background-color:{0}; color:{1}; border: none; cursor:pointer; border-radius:{2}px; }}", ButtonColor.ToRgbaHtml(), ButtonFontColor.ToRgbaHtml(), CornerRadius);
            //    }
            //    return true;
            //}
            //catch (Exception) { }

            //return false;
        }
    }
}
