﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.ProgramProfile.Military
{
    [Serializable]
    [Table("ProfileMilitaryAttachments")] 
    public class ProfileMilitaryAttachment
    {

        public ProfileMilitaryAttachment()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("28550723-c143-4100-b032-ca0691706e12"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        public Guid ProfileMilitaryId { get; set; }
        public virtual ProfileMilitary ProfileMilitary { get; set; }
        public Guid AttachmentId { get; set; }
        public virtual Attachment Attachment { get; set; }
        public TimeStamp TimeStamp { get; set; }
    }
}
