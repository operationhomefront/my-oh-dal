﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.WorkflowTemplates;

namespace OH.DAL.Domain.ProgramProfile.Military
{
    [Serializable]
    [Table("ProfileMilitaryWorkflowItems")] //TODO: Pluralize the table name
    public class ProfileMilitaryWorkflowItems
    {

        public ProfileMilitaryWorkflowItems()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("bb516395-abe4-4234-8c75-ac10df6de24a"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public Guid WorkflowItemId { get; set; }
        public virtual WorkflowItem WorkflowItems { get; set; }

        public Guid ProfileMilitaryId { get; set; }

        public virtual ProfileMilitary  ProfileMilitary { get; set; }
    }
}



