﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.ProgramProfile.Injury;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Domain.WorkflowTemplates;

namespace OH.DAL.Domain.ProgramProfile.Military
{
    [Serializable]
    [Table("ProfileMilitary")] //TODO: Pluralize the table name
    public class ProfileMilitary
    {

        public ProfileMilitary()
         {
             Id = Guid.NewGuid();
             WorkFlowItems = new HashSet<WorkflowItem>();
         }

         [NotMapped]
         public static Guid ClassGuid
         {
             get { return new Guid("43B70F68-6B6E-4807-A0A5-9687C5B780F5"); }
         }

         [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
         public int ClusterId { get; set; }

         [DatabaseGenerated(DatabaseGeneratedOption.None)]
         public Guid Id { get; set; }

         public virtual IEnumerable<ProfileMilitaryAttachment> Attachments { get; set; }
         public Guid MilitaryProfileId { get; set; }
         public virtual PersonMilitary MilitaryProfile { get; set; }
         public virtual TimeStamp TimeStamp { get; set; }
         public ICollection<WorkflowItem> WorkFlowItems { get; set; }
    }
}
