﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.ProgramProfile.Military;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ListTypes.Injury;
namespace OH.DAL.Domain.ProgramProfile.Injury
{
    [Serializable]
    [Table("PersonMilitaryInjuryInflictions")] //TODO: Pluralize the table name
    public class InjuryInfliction
    {
        public InjuryInfliction()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("32BF23DD-60F6-4B83-88A6-3B195E605151"); }
        }

       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public int ClusterId { get; set; }

       [DatabaseGenerated(DatabaseGeneratedOption.None)]
       public Guid Id { get; set; }

       public string BriefDescription { get; set; }
       public string DetailDescription { get; set; }
       public Guid? TypeOfInjury { get; set; }
       public string Cause { get; set; }

       public Guid? LocationId { get; set; }
       public ListMilitaryCombatTheater Location { get; set; }

       public Guid InjuryInflictionCategoryId { get; set; }
       public ListInjuryInflictionCategory ListInjuryInflictionCategory { get; set; }
       
        public DateTime? DateOfOccurrence { get; set; }
       public Guid? Disposition { get; set; }

       public Guid ProfileMilitaryId { get; set; }
       public ProfileMilitary ProfileMilitary { get; set; }

       public virtual TimeStamp TimeStamp { get; set; }


    }
}
