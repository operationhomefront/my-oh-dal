﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.ProgramProfile.Caregiver
{
    [Serializable]
    [Table("ProfileCaregiverAttachments")] 
    public class ProfileCaregiverAttachment
    {

        public ProfileCaregiverAttachment()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("43B70F68-6B6E-4807-A0A5-9687C5B780F5"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }
        public Guid AttachmentId { get; set; }
        public virtual Attachment Attachment { get; set; }
        public Guid ProfileCaregiverId { get; set; }
        public virtual ProfileCaregiver ProfileCaregiver { get; set; }
        public TimeStamp TimeStamp { get; set; }
    }
}
