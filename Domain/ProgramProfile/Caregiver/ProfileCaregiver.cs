﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.Persons;

using OH.DAL.Domain.TimeStamps;

using OH.DAL.Domain.WorkflowTemplates;

namespace OH.DAL.Domain.ProgramProfile.Caregiver
{
    [Serializable]
    [Table("ProfileCaregiver")] 
    public class ProfileCaregiver
    {
         public ProfileCaregiver()
         {
             Id = Guid.NewGuid();
             WorkFlowItems = new HashSet<WorkflowItem>();
         }

         [NotMapped]
         public static Guid ClassGuid
         {
             get { return new Guid("43B70F68-6B6E-4807-A0A5-9687C5B780F5"); }
         }

         [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
         public int ClusterId { get; set; }

         [DatabaseGenerated(DatabaseGeneratedOption.None)]
         public Guid Id { get; set; }

         public virtual IEnumerable<ProfileCaregiverAttachment> Attachments { get; set; }
         public Guid? ProfileId { get; set; }
         public virtual PersonCaregiver Profile { get; set; }
         public virtual TimeStamp TimeStamp { get; set; }
         public ICollection<WorkflowItem> WorkFlowItems { get; set; }
    }
}
