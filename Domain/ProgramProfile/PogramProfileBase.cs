﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.ProgramProfile
{
    [Serializable]
    [Table("PogramProfilesBase")] //TODO: Pluralize the table name
    public class PogramProfileBase
    {

        public PogramProfileBase()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("36245103-5f4b-4efb-a2d3-9b186a5d23f0"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }



    }
}
