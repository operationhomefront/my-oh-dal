﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Meetings
{
    [Serializable]
    [Table("MeetingRooms")] //TODO: Pluralize the table name
    public class MeetingRoom
    {

        public MeetingRoom()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("4a431655-2542-4c54-b38b-885c945f2672"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Number { get; set; }

        public int Capacity { get; set; }

        public Guid MeetingLocationId { get; set; }

        public virtual MeetingLocation MeetingLocation { get; set; }

    }
}
