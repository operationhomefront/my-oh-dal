﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Meetings
{
    [Serializable]
    [Table("Meetings")] 
    public class Meeting
    {
        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("15b2f7d7-0e87-447f-bf61-97b54e64d526"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Meeting()
        {
            Id = Guid.NewGuid();
            Start = DateTime.UtcNow;
            End = DateTime.UtcNow;

        }        
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public Guid? MeetingLocationId { get; set; }
        public MeetingLocation MeetingLocation { get; set; }
        public Guid? MeetingRoomId { get; set; }

        public bool IsAllDay { get; set; }
        public string RecurrenceRule { get; set; }
        public int? RecurrenceId { get; set; }
        public string RecurrenceException { get; set; }
        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }

        public ICollection<MeetingAttendee> MeetingAttendees { get; set; }    

        public ICollection<Meeting> SubMeetings { get; set; }
        public Meeting ParentMeeting { get; set; }
    }
}
