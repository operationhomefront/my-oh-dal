﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Meetings
{
    [Serializable]
    [Table("MeetingAttendees")] //TODO: Pluralize the table name
    public class MeetingAttendee
    {

        public MeetingAttendee()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("e5791c90-a592-4e80-a5ec-a63437774797"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public Guid MeetingId { get; set; }       

        public virtual Meeting Meeting { get; set; }

        public Guid PersonId { get; set; }

        public virtual Person Person { get; set; }
    }
}
