﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.Phones;

namespace OH.DAL.Domain.Meetings
{
    [Serializable]
    [Table("MeetingLocations")] //TODO: Pluralize the table name
    public class MeetingLocation
    {

        public MeetingLocation()
        {
            Id = Guid.NewGuid();
            LocationEmailAddresses = new HashSet<EmailAddress>();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("5a62520e-cf84-4540-b2a5-30ba0bc058bf"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid AddressId { get; set; }

        public virtual Address Address { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<MeetingRoom> Rooms { get; set; }

        public virtual ICollection<Phone> LocationPhoneNumbers { get; set; }

        public ICollection<EmailAddress> LocationEmailAddresses { get; set; }

        public virtual ICollection<Person> ContactPersons { get; set; }



    }
}
