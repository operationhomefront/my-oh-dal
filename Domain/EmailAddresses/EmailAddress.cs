﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Meetings;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.EmailAddresses
{
    [Serializable]
    [Table("EmailAddresses")]
    public class EmailAddress
    {
        public EmailAddress()
        {
            Id = Guid.NewGuid();
            MeetingLocations = new HashSet<MeetingLocation>();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("330443DB-8958-4550-8521-8CCD19C2CD15"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }

        public string Email { get; set; }

        public bool DoNotSend { get; set; }

        public ICollection<MeetingLocation> MeetingLocations { get; set; }
    }
}