﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Security
{
    [Serializable]
    [Table("UserRoles")] 
    public class UserRole
    {

        public UserRole()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("0513cd6b-1e71-4238-860d-f05bb6c12030"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }

        public string Name { get; set; }

        public virtual UserGroup Group { get; set; }

        public virtual ICollection<UserAccount> Users { get; set; }

    }
}
