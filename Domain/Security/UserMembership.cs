﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Security
{
    [Serializable]
    [Table("UserMemberships")] 
    public class UserMembership
    {

        public UserMembership()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("6fbd1504-25bb-4b13-bcf6-bc3878799f3e"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public virtual UserAccount User { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ConfirmationToken { get; set; }
        public bool Confirmed { get; set; }
        public DateTime? LastLoginFailed { get; set; }
        public int LoginFailureCounter { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }

        public DateTime? PasswordChanged { get; set; }

        public string VerificationToken { get; set; }
        public DateTime? VerificationTokenExpires { get; set; }


    }
}
