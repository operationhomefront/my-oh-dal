﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Security
{
    [Serializable]
    [Table("UserOAuthMemberships")] 
    public class UserOAuthMembership
    {

        public UserOAuthMembership()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("741fd324-9e8f-4fc8-8e71-07344e4a79d9"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public virtual UserAccount User { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }

    }
}
