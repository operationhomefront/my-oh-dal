﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Applications;

namespace OH.DAL.Domain.Security
{
    [Serializable]
    [Table("UserGroups")] 
    public class UserGroup
    {

        public UserGroup()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("3ae322c0-5da9-4a6a-b289-2a5fa83a0e5d"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public string Name { get; set; }
        public Guid ApplicationId { get; set; }

        public virtual Application Application { get; set; }

    }
}
