﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Security
{
    [Serializable]
    [Table("UserAccounts")]
    public class UserAccount 
    {

        public UserAccount()
        {
            Id = Guid.NewGuid();
            ExternalLogins = new Collection<UserOAuthMembership>();
            Roles = new Collection<UserRole>();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("b890094e-3f5b-40b4-a7c1-f3f511e5faf3"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public virtual string Email { get; set; }

        public UserMembership PasswordLogin { get; set; }
        public virtual ICollection<UserOAuthMembership> ExternalLogins { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }

    }


}
