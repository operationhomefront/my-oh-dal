﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.TimeStamps
{
    [Serializable]
    [Table("TimeStamps")]
    public class TimeStamp
    {
        public TimeStamp()
        {
            Id = Guid.NewGuid();
            UtcCreatedDate = DateTime.UtcNow;
            UtcLastModifiedDate = DateTime.UtcNow;
        }
        
        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("480AAB2F-7098-4277-941C-07E403E8F78F"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid RefId { get; set; }

        public String CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime UtcCreatedDate { get; set; }

        public String LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime UtcLastModifiedDate { get; set; }
    }
}