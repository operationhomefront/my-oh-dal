﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Reports
{
    [Serializable]
    [Table("Reports")] //TODO: Pluralize the table name
    public class Report
    {

        public Report()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("6e36e541-238e-4047-af09-f5d89b9f44fe"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public string ReportId { get; set; }
        public string ReportName { get; set; }
        public int SelectLimit { get; set; }
        public string StoredProcedureName { get; set; }
        public virtual Guid ApplicationId { get; set; }
    }
}
