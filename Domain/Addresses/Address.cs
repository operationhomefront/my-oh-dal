﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Addresses
{
    [Serializable]
    [Table("Addresses")]
    public class Address
    {
        public Address()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("03F36EF9-8742-4A7B-934D-4E44D93166B5"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public DbGeography Location { get; set; }
        public string Verified { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public virtual TimeStamp TimeStamp { get; set; }
        public bool IsActive { get; set; }
       
    }
}