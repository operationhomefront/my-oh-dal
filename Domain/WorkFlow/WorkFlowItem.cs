﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Configuration;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.WorkFlow
{
    [Serializable]
    [Table("WorkFlowItem")] //TODO: Pluralize the table name
    public class WorkFlowItem
    {
        //todo: will only include one resource per task for now
        public WorkFlowItem()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("9c974a05-bc2b-40a7-ba29-276370ffc455"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }


        private DateTime End { get; set; }
        public bool Expanded { get; set; }
        public int OrderId { get; set; }
        public Guid? ParentWorkFlowItemId { get; set; }
        public decimal PercentComplete { get; set; }
        private DateTime Start { get; set; }
        public bool Summary { get; set; }
        public TimeStamp TimeStamp { get; set; }
        public string Title { get; set; }
        
        [NotMapped]
        public virtual WorkFlowItem WorkFlowItem1 { get; set; }
        [NotMapped]
        public virtual ICollection<WorkFlowItem> WorkFlowItems1 { get; set; }
        public Guid WorkFlowId { get; set; }
        public virtual WorkFlow WorkFlow { get; set; }
    }
}
