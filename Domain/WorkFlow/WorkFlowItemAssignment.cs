﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.WorkFlow
{
    [Serializable]
    [Table("WorkFlowItemAssignment")] //TODO: Pluralize the table name
    public class WorkFlowItemAssignment
    {

        public WorkFlowItemAssignment()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("d07feeab-be13-479f-a6f8-bfa58b07f150"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        
        public Guid ResourceId { get; set; }
        public virtual Person Resource { get; set; }
        public TimeStamp TimeStamp { get; set; }
        public decimal Units { get; set; }
        public Guid WorkFlowItemId { get; set; }
        public virtual WorkFlowItem WorkFlowItem { get; set; }


    }
}
