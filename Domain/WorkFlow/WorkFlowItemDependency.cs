﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.TimeStamps;


namespace OH.DAL.Domain.WorkFlow
{
    [Serializable]
    [Table("WorkFlowItemDependency")] //TODO: Pluralize the table name
    public class WorkFlowItemDependency
    {

        public WorkFlowItemDependency()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("aaa0de44-3cfd-4f3c-aa0e-6b84c7bd2197"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }


        public Guid DependencyId { get; set; }
        public Guid PredecessorId { get; set; }
        public Guid SuccessorId { get; set; }
        public TimeStamp TimeStamp { get; set; }
        public int Type { get; set; }


    }
}
