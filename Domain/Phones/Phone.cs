﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Meetings;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Phones
{
    public class Phone
    {
        public Phone()
        {
            Id = Guid.NewGuid();
            Persons = new HashSet<Person>();
            MeetingLocations = new HashSet<MeetingLocation>();

        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("03F36EF9-8742-4A7B-934D-4E44D93166B5"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }

        public string PhoneNumber { get; set; }

        public string Extension { get; set; }

        public string PhoneType { get; set; }

        public ICollection<Person> Persons { get; set; }
        public ICollection<MeetingLocation> MeetingLocations { get; set; }

    }
}
