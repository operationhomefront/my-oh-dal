﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.ChangeLogs;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Attachments
{
    [Serializable]
    [Table("Attachments")]
    public class Attachment
    {
        public Attachment()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("DE6BF3A5-E6FC-4408-B7E7-C8101EC0264D"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public String OriginalFileName { get; set; }
   
        public String FileName { get; set; }

        public Guid OwnerId { get; set; }

        public Guid RefId { get; set; }

        public String FileExtension { get; set; }
        //TODO: Add to separate table, and also add thumbnail 
        public byte[] FileStream { get; set; }

        public  ChangeLog ChangeLog { get; set; }


    }
}
