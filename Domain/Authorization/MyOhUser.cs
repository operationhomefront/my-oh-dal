using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Authorization
{
    [Serializable]
    [Table("MyOHUsers")]
    public class MyOhUser
    {


        public MyOhUser()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("77D1B411-7B41-4000-89EC-5E694C8BAAD2"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public virtual string Email { get; set; }

        public virtual MyOhUserMembership PasswordLogin { get; set; }
        public virtual ICollection<MyOhoAuthMembership> ExternalLogins { get; set; }

        public virtual ICollection<MyOhRole> Roles { get; set; }

    }
}