using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Authorization
{
    public class MyOhUserMembership
    {
        public MyOhUserMembership()
        {
            Id = Guid.NewGuid();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

  
        public virtual MyOhUser User { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string ConfirmationToken { get; set; }
        public bool Confirmed { get; set; }
        public DateTime? LastLoginFailed { get; set; }
        public int LoginFailureCounter { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime? PasswordChanged { get; set; }
        public String VerificationToken { get; set; }
        public DateTime? VerificationTokenExpires { get; set; }
    }
}