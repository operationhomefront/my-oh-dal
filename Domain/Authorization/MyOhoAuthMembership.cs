using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Authorization
{
    [Serializable]
    [Table("MyOHOAuthMemberships")]
    public class MyOhoAuthMembership
    {
        public MyOhoAuthMembership()
        {
            Id = Guid.NewGuid();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public virtual MyOhUser User { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
    }
}