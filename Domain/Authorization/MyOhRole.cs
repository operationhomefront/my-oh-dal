using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Authorization
{
    [Serializable]
    [Table("MyOHRoles")]
    public class MyOhRole
    {
        public MyOhRole()
        {
            Id = Guid.NewGuid();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string RoleName { get; set; }

        public virtual MyOhSecurityGroup Group { get; set; }

        public virtual ICollection<MyOhUser> Users { get; set; }

    }
}