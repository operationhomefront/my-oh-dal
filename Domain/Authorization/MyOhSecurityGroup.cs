using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Authorization
{
    [Serializable]
    [Table("MyOHSecurityGroups")]
    public class MyOhSecurityGroup
    {
        public MyOhSecurityGroup()
        {
            Id = Guid.NewGuid();
        }

        //public static MyOhSecurityGroup GetGroup(Guid id, string Name, Guid application)
        //{
        //    return new MyOhSecurityGroup { Id = id, Name = Name, ApplicationId = application };
        //}



        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string Name { get; set; }
        public Guid ApplicationId { get; set; }

        //public virtual Application Application { get; set; }
    }
}