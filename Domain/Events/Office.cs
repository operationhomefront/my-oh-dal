﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("Offices")] //TODO: Pluralize the table name
    public class Office
    {

        public Office()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("8938e0af-f203-4550-bf4d-d5ad54bb9b2d"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public String InternalName { get; set; }


    }
}
