﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Repositories.Events;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("CollectionEvents")] 
    public class CollectionEvent : Event
    {

        public CollectionEvent()
        {
          
        }

        public Int32 RegistrationRadius { get; set; }
        public Int32 MaxStoresPerVolunteer { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CollectionBeginsUtc { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? CollectionEndsUtc { get; set; }


        public virtual ICollection<CollectionLocation> Locations { get; set; }
        public virtual ICollection<CollectionVolunteer> Volunteers { get; set; }

        public virtual ICollection<Office> Participants { get; set; }

    }
}
