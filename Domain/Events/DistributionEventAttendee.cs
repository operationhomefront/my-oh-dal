﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("DistributionEventAttendees")] 
    public class DistributionEventAttendee 
    {

        public DistributionEventAttendee()
        {
            Id = Guid.NewGuid();
        }
        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("6D876F0E-AEB1-4ABC-A99E-2AE9B6EC7656"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
        public Guid? Shirtsize { get; set; }
        public virtual Person Attendee { get; set; }

    }
}
