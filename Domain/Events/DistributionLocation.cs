﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Meetings;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("DistributionLocations")] //TODO: Pluralize the table name
    public class DistributionLocation:MeetingLocation
    {

        public DistributionLocation()
        {
            Id = Guid.NewGuid();
        }

        public String Place { get; set; }
        public String EventDetails { get; set; }
        public String LocationDetails { get; set; }
        public Int32 StartingInventory { get; set; }
        public Int32 RegistrationRadius { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? StartDateTimeUtc { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? EndDateTimeUtc { get; set; }

        public Boolean? RemoveFromWeb { get; set; }
        public Boolean? AutoConfirm { get; set; }
        public Boolean? WaitList { get; set; }
        public Int32 WaitListMax { get; set; }
        public Boolean? CloseOnFull { get; set; }
        public Boolean? LocationClosed { get; set; }
        public Boolean? EnableTimeSlots { get; set; }
        public Int32 TimeSlotMinutes { get; set; }
        public Guid PriorityLevelId { get; set; }
        public String RegAckEmailText { get; set; }
        public String RegWaitListEmailText { get; set; }
        public String RegConfirmEmailText { get; set; }
        public String RegReminderEmailText { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder1Date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder2Date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder3Date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder1Sent { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder2Sent { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder3Sent { get; set; }
        public virtual TimeStamp TimeStamp { get; set; }

        public DateTime? RegistrationOpensUtc { get; set; }
        public DateTime? RegistrationClosesUtc { get; set; }

        public string UmbracoContentId { get; set; }

        public string LocationContact { get; set; }



        public virtual ICollection<DistributionEventRegistrant> Registrants { get; set; }
        

    }
}
