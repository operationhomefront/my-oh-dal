﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("DistributionEvents")] //TODO: Pluralize the table name
    public class DistributionEvent:Event
    {

        public DistributionEvent()
        {
           
        }

        public Boolean? AutoConfirm { get; set; }
        public Boolean? WaitList { get; set; }
        public Int32 WaitListMax { get; set; }
        public Boolean? CloseOnFull { get; set; }
        public String EtapCampaign { get; set; }
        public String EtapApproach { get; set; }
        public String EtapGiftOwner { get; set; }
        public String EtapFund { get; set; }
        //
        // Email And Reminders
        //                  
        public String RegAckEmailText { get; set; }
        public String RegWaitListEmailText { get; set; }
        public String RegConfirmEmailText { get; set; }
        public String RegReminderEmailText { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder1Date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder2Date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder3Date { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Reminder1SentDate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder2SentDate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? Reminder3SentDate { get; set; }



        public int CountScheme { get; set; }

        //Registration Requirements
        public Boolean? ServiceMember { get; set; }
        public Boolean? Mother { get; set; }
        public Boolean? Father { get; set; }
        public Boolean? Brother { get; set; }
        public Boolean? Sister { get; set; }
        public Boolean? Son { get; set; }
        public Boolean? Daughter { get; set; }
        public Boolean? Spouse { get; set; }
        public Boolean? Friend { get; set; }
        public Boolean? StepSon { get; set; }
        public Boolean? StepDaughter { get; set; }
        public Boolean? Other { get; set; }
        public Boolean? PrivateEvent { get; set; }

        public Int32 AgeMin { get; set; }
        public Int32 AgeMax { get; set; }

        public Guid? GradeMinId { get; set; }
        
        public Guid? GradeMaxId { get; set; }

        public virtual ICollection<DistributionEventRegistrant> Registrants { get; set; }
        public virtual ICollection<DistributionLocation> Locations { get; set; }
        public virtual ICollection<Office> Participants { get; set; }

    }
}
