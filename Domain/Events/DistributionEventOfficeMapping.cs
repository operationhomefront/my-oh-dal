﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("DistributionEventOfficeMapping")] 
    public class DistributionEventOfficeMapping
    {
        public DistributionEventOfficeMapping()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("7956E4E1-C61D-4209-9F4C-56F5712BCB79"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        public Guid DistributionEventId { get; set; }
        public virtual DistributionEvent DistributionEvent { get; set; }
        public Guid  DistributionEventOfficeId { get; set; }
        public virtual Office DistributionEventOffice { get; set; }
    }
}
