﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("CollectionLocationMapping")] //TODO: Pluralize the table name
    public class CollectionLocationMapping
    {

        public CollectionLocationMapping()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("4d51434f-bc1a-4263-9c74-349f4ed3892c"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public Guid CollectionEventId { get; set; }
        public virtual CollectionEvent CollectionEvent { get; set; }

        public Guid CollectionLocationId { get; set; }
        public virtual CollectionLocation CollectionLocation { get; set; }

    }
}
