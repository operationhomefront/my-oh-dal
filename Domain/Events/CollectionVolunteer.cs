﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("CollectionVolunteers")] 
    public class CollectionVolunteer 
    {

        public CollectionVolunteer()
        {
            Id = Guid.NewGuid();
        }


        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("7E291FD8-DE2A-4956-B17D-989FC3668BA4"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Int32 PickupRadiusLimit { get; set; }
        public Int32 MaxLocationsAllowed { get; set; }
        public String AffiliatedWith { get; set; }
        public String DistributingTo { get; set; }
        public String MilitaryBase { get; set; }
        public String Unit { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime PledgeConfirmedDate { get; set; }
        public Guid PersonId { get; set; }
        public virtual Person Person { get; set; }

    }
}
