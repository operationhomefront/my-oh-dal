﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("CollectionEventOfficeMapping")] 
    public class CollectionEventOfficeMapping
    {
        public CollectionEventOfficeMapping()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("726ECFF3-3934-423E-B9CD-157FC3266D76"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        public Guid CollectionEventId { get; set; }
        public virtual CollectionEvent CollectionEvent { get; set; }
        public Guid CollectionOfficeId { get; set; }

        public virtual Office CollectionOffice { get; set; }
    }
}
