﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("DistributionEventRegistrants")] 
    public class DistributionEventRegistrant:Registrant
    {

        public DistributionEventRegistrant()
        {
            Id = Guid.NewGuid();
        }
        public Guid? DistributionShirtSizeId { get; set; }

    }
}
