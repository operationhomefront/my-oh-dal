﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Meetings;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("CollectionLocations")]
    public class CollectionLocation 
    {

        public CollectionLocation()
        {
            Id = Guid.NewGuid();
        }
        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("4A23B330-52B0-4663-833B-0C7AC24C2450"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Guid AddressId { get; set; }

        public virtual Address Address { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        public string PointOfInterest { get; set; }
        public string PointOfContact { get; set; }
        public int StoreNumber { get; set; }
        public double HoursOfOperation { get; set; }
        public bool AddressValid { get; set; }
        public bool NotAvailable { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime OpeningOnUtc { get; set; }
        public virtual TimeStamp TimeStamp { get; set; }
        public virtual Collection<CollectionVolunteer> Volunteers { get; set; }
    }
}
