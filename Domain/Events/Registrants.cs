﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("DistributionEventRegistrants")] //TODO: Pluralize the table name
    public class DistributionEventRegistrant
    {

        public DistributionEventRegistrant()
        {
            Id = Guid.NewGuid();
        }
        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("DB73F164-30AB-4343-866A-45F30E5D53BD"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? RegistrationEmailSent { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? ConfirmationEmailSent { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? ReminderEmailSent { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? TimeSlot { get; set; }
        public Guid RegistrationStatusId { get; set; }
        
        public string RegistrantFirstName { get; set; }
        public string RegistrantLastname { get; set; }
        public String RegistrantEmail { get; set; }
        public Guid PersonId { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<DistributionEventAttendee> Attendees { get; set; }

    }
}
