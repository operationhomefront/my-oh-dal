﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("CollectionVolunteerMapping")] //TODO: Pluralize the table name
    public class CollectionVolunteerMapping
    {

        public CollectionVolunteerMapping()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("f9b0b166-d871-492d-94c6-c56979dbacd2"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }

        public Guid CollectionEventId { get; set; }
        public virtual CollectionEvent CollectionEvent { get; set; }
        public Guid CollectionVolunteerId { get; set; }
        public virtual CollectionVolunteer CollectionVolunteer { get; set; }

    }
}
