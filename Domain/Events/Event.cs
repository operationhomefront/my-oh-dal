﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Meetings;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.Events
{
    [Serializable]
    [Table("Events")] 
    public class Event 
    {

        public Event()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("39D9E451-EAB9-4A5E-B172-2999840F4AD1"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }

        public string Partner { get; set; }

        public Guid EventTypeId { get; set; }
        public bool? PublishToEventCalendar { get; set; }
        public string FormNbr { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? PublishToWebUtc { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? UnpublishFromWebUtc { get; set; }


        [Column(TypeName = "datetime2")]
        public DateTime RegistrationOpensUtc { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime RegistrationClosesUtc { get; set; }

        public bool EventClosed { get; set; }
        public bool? RemoveFromWeb { get; set; }
        public string ContactListName { get; set; }
        public virtual TimeStamp TimeStamp { get; set; }

        public Guid OwnerId { get; set; }
        public virtual Office Owner { get; set; }
        public string ParentEventKey { get; set; }

       

        public virtual Meeting Meeting { get; set; }

    }
}
