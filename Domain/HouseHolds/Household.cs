﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL.Domain.Households
{
    [Serializable]
    [Table("Households")]
    public class Household
    {
        public Household()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("FCD72D03-4381-4426-895F-96E336D7C5CA"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public String Name { get; set; }

        public virtual ICollection<HouseholdMember> HouseholdMembers { get; set; }
    }
}
