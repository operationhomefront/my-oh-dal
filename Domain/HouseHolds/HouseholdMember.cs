﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Domain.Households
{
    [Serializable]
    [Table("HouseholdMembers")]
    public class HouseholdMember
    {
        public HouseholdMember()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("F2855BEE-FFFD-4B47-98BC-A727306AF830"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }
        
        public string Grade { get; set; }
        public Guid HouseholdId { get; set; }
        public virtual  Household Household { get; set; }
        public Guid HouseholdMemberType { get; set; }
        public bool IsDependent { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsServiceMember { get; set; }
        public string LivesWith { get; set; }
        public Guid PersonId { get; set; }
        public virtual Person Person { get; set; }
        
    }
}