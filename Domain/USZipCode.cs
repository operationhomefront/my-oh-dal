﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace OH.DAL.Domain
{
    [Serializable]
    [Table("USZipCodes")] 
    public class USZipCode 
    {
        [Key]
		public String		ZipCode     { get; set; }
		public String		City        { get; set; }
		public String		State       { get; set; }
		public String		County      { get; set; }
        public DbGeography  Location    { get; set; }

        //[NotMapped]
        //public virtual Guid Id { get; set; }
        public USZipCode()
        {
            ZipCode         = "";
            City            = "";
            State           = "";
            County          = "";
        }
	}
}
