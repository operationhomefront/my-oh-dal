﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net.Configuration;
using OH.DAL.Domain.ProgramProfile.Caregiver;
using OH.DAL.Domain.ProgramProfile.Military;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.WorkflowTemplates
{
    [Serializable]
    [Table("WorkflowItem")] //TODO: Pluralize the table name
    public class WorkflowItem
    {
        //todo: will only include one resource per task for now
        public WorkflowItem()
        {
            Id = Guid.NewGuid();
            ProfilesMilitary = new HashSet<ProfileMilitary>();
            ProfilesCaregiver = new HashSet<ProfileCaregiver>();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("9c974a05-bc2b-40a7-ba29-276370ffc455"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }


        public DateTime End { get; set; }
        public bool Expanded { get; set; }
        public int OrderId { get; set; }
        public Guid? ParentWorkFlowItemId { get; set; }
        public decimal PercentComplete { get; set; }
        public DateTime Start { get; set; }
        public bool Summary { get; set; }
        public TimeStamp TimeStamp { get; set; }
        public string Title { get; set; }
        
        [NotMapped]
        public virtual WorkflowItem WorkflowItem1 { get; set; }
        [NotMapped]
        public virtual ICollection<WorkflowItem> WorkFlowItems1 { get; set; }
        public Guid WorkFlowId { get; set; }
        public virtual Workflow Workflow { get; set; }

        public ICollection<ProfileMilitary> ProfilesMilitary { get; set; }
        public ICollection<ProfileCaregiver> ProfilesCaregiver { get; set; }
    }
}
