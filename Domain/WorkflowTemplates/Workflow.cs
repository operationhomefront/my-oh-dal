﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Domain.WorkflowTemplates
{
    [Serializable]
    [Table("Workflow")] //TODO: Pluralize the table name
    public class Workflow
    {
        public Workflow()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("aedf1f5f-b348-4510-8787-5ba3a6f26def"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get;  set; }
        
        public Guid? Owner { get; set; }
        public string Summary { get; set; }
        public TimeStamp TimeStamp { get; set; }
        public string Title { get; set; }
        public ICollection<WorkflowItem> WorkFlowItems { get; set; }
    }
}
