﻿using System;
using System.Collections.Generic;

namespace OH.DAL
{
    public interface ILegacyOHv4Repository<T> : IDisposable where T : class
    {
        T GetById(string id);
        IEnumerable<T> GetAll();
        //IEnumerable<object> GetAllEntitiesByGuid(object guid);
        string AddOrUpdate(T entity);
        void Delete(string id);
        void Save();
        
        string GetLastInsertedId();

    }
}
