using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Applications;
using OH.DAL.Domain.Categories;
using OH.DAL.Domain.Events;
using OH.DAL.Domain.Households;
using OH.DAL.Domain.ListTypes;
using OH.DAL.Domain.ListTypes.CareGiver;
using OH.DAL.Domain.ListTypes.Events;
using OH.DAL.Domain.ListTypes.General;
using OH.DAL.Domain.ListTypes.Injury;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ListTypes.Person;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.ProgramProfile;
using OH.DAL.Domain.Reports;
using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class MyOHDbConfig : DbMigrationsConfiguration<OH.DAL.MyOhContext> 
    {
        public MyOHDbConfig()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
            MigrationsDirectory = @"Migrations";
        }

        protected override void Seed(OH.DAL.MyOhContext context)
        {
            //public enum EventType
            //{
            //    Distribution = 1,
            //    Collection = 2
            //}

            context.ListTypes.AddOrUpdate(new EventTypes { Id = new Guid("B1B81060-5F78-400C-937F-181A8F00B900"), EnumValue = 1, SortOrder = 1, Name = "Distribution"});
            context.ListTypes.AddOrUpdate(new EventTypes { Id = new Guid("DE4A56BF-0E98-49B4-9CF8-1C9DFD2B91DA"), EnumValue = 2, SortOrder = 2, Name = "Collection" });
         

            context.Addresses.Add(new Address { Id = new Guid("5F576B9F-EFEF-4483-BFEF-2539A4EE815F"), Address1 = "1234 Medical Drive", City = "San Antonio", State = "TX" });
            context.Addresses.Add(new Address { Id = new Guid("9E7049CB-77F3-4CE8-8D97-5DF2AC5B7D97"), Address1 = "567 Medical Drive", City = "San Antonio", State = "TX" });

            context.ListTypes.AddOrUpdate(new ListMilitaryCombatTheater { Id = new Guid("E1CAC9ED-CB90-4E26-813A-067226C4149C"),SortOrder=1, Name = "OCONUS, combat related" });
            context.ListTypes.AddOrUpdate(new ListMilitaryCombatTheater { Id = new Guid("20EA8736-B177-4F63-9ED6-63837427F04C"), SortOrder = 2, Name = "OCONUS, non-combat related" });
            context.ListTypes.AddOrUpdate(new ListMilitaryCombatTheater { Id = new Guid("EDD1E992-415C-4D81-BC23-7DF415DC409D"), SortOrder = 3, Name = "CONUS" });


            
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("FD2921A6-9B29-48B4-A75F-295361B7071E"), Name = "None", EnumValue = 0, SortOrder = 1, OptionalValue = "None" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("A0FD20DB-0242-4721-BEF0-46F127D529A7"), Name = "Spouse", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("8E690DC8-C63C-4E74-BC91-57123AD5D866"), Name = "Son", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative:Dependent" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("CC2C5C38-4CB2-4A8A-844A-527FC7EB8026"), Name = "Step Son", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative:Dependent" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("5EEAFF6A-1493-4A06-8DFB-ADD236478A1E"), Name = "Step Daughter", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative:Dependent" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("E8AB478E-1757-4ADB-B931-74AD65103F89"), Name = "Daughter", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative:Dependent" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("0C6E959A-5A86-4CDC-B5AE-7C5AF1D70A89"), Name = "Brother", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative:Dependent" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("A73AA984-E9BF-4AC0-802B-7C9A0DAB5FEF"), Name = "Sister", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("C31581D3-F267-4ED4-A5A7-8BDFE68B9C00"), Name = "Partner", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("F59CF7FD-574B-45CD-92B3-904B9B03A702"), Name = "Significant Other", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("14657D34-3923-4EC9-A954-94D97B114002"), Name = "Mother", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("2EBF5A78-B620-41A7-92FB-A646E218B862"), Name = "Father", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("8F87189F-992E-4A0F-9CDA-B80C49CED669"), Name = "Step Father", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("F64D664E-D916-4924-96CA-C06874A0D436"), Name = "Step Mother", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("BAD84F4B-6090-48EE-B16F-DC0E5D494405"), Name = "Legal Guardian", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" });
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("3D94588F-EBE1-4B7A-923C-E72F0543BF01"), Name = "Other Relative", EnumValue = 0, SortOrder = 1, OptionalValue = "Relative" }); //includes friend
            context.ListTypes.AddOrUpdate(new Relationship { Id = new Guid("F9EEBBE3-6C50-43DD-A762-FBCEBD67A3FE"), Name = "Other Non-Relative", EnumValue = 0, SortOrder = 1, OptionalValue = "Contact" });

            /*
            *  public enum HoursProvidingCareWeekly
                {
                    None
                    , HrsLessThan1
                    , Hrs1To4
                    , Hrs5To8
                    , Hrs9To20
                    , Hrs21To30
                    , Hrs31To40
                    , Hrs41To60
                    , Hrs61To80
                    , HrsMoreThan80
                }
            * */
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("C2E24245-14D6-4748-86B0-0AC5F024847A"), Name = "None", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("16229C9D-B6BD-42B3-8DA5-2605C12B9FF7"), Name = "Less than one hour", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("7FE8F7F0-CCB5-4C39-8D60-4B1201149C6D"), Name = "1 to 4 hours", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("26BD92E3-367F-4EBA-A7CE-5E7E110852FD"), Name = "5 to 8 hours", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("DBADD723-5A2C-4976-AA1B-696C167AF4DC"), Name = "9 to 20 hours", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("CA3A030A-96E4-48B4-AABC-6C66BC94D476"), Name = "21 to 30 hours", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("BAE4E4D6-9D67-489B-9A24-7EBFCFCC618A"), Name = "31 to 40 hours", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("21843D19-9581-4606-880D-AA3A4A45D712"), Name = "41 to 60 hours", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("8346248E-A883-4100-B91C-D8196C88458C"), Name = "61 to 80 hours", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new HoursProvidingCareWeekly { Id = new Guid("490DCA00-83DF-40C2-A777-E97B01BB16F1"), Name = "More than 80 hours", EnumValue = 0, SortOrder = 1 });

   /*
             * public enum AgeRange
                {
                    None
                    , Age_18_24
                    , Age_25_34
                    , Age_35_44
                    , Age_45_54
                    , Age_55_Older
                }
             * */
            context.ListTypes.AddOrUpdate(new AgeRange { Id = new Guid("E332D1B1-3CE7-4077-B564-0732A1869783"), Name = "18 - 24 years", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new AgeRange { Id = new Guid("3B3B12E3-C1AF-48C4-8D35-288C146D714C"), Name = "25 - 34 years", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new AgeRange { Id = new Guid("C85CD786-4BCB-49AD-838F-5262912EB0E3"), Name = "35 - 44 years", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new AgeRange { Id = new Guid("D12047B3-171F-4618-AAFC-5A9446446B24"), Name = "45 - 54 years", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new AgeRange { Id = new Guid("4B9F1C4E-9716-40BC-B035-8DB58E885465"), Name = "55 years or older", EnumValue = 0, SortOrder = 1 });




            //duration providing care 
            context.ListTypes.AddOrUpdate(new DurationProvidingCare { Id = new Guid("370D793F-738B-4D65-BDB9-1F5D1F272D54"), Name = "Less than 6 months", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new DurationProvidingCare { Id = new Guid("0ACA4D0F-BCFE-4EA3-A65A-BD070B8521D4"), Name = "6 months to 1 year", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new DurationProvidingCare { Id = new Guid("E53AF03A-42DE-4D66-83A3-C540B76E34F2"), Name = "1 to 4 years", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new DurationProvidingCare { Id = new Guid("2565572F-6190-441A-9BBC-C70C1B1349A3"), Name = "5 to 9 years", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new DurationProvidingCare { Id = new Guid("A736AF09-DE4D-4694-9951-EA3DD2F2DB1A"), Name = "10 years or more", EnumValue = 0, SortOrder = 1 });
            
            //Vas disability rating
            context.ListTypes.AddOrUpdate(new VADisabilityRating { Id = new Guid("F6328E9D-2EA1-44A7-AE41-182198CF1AE4"), Name = "None", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new VADisabilityRating { Id = new Guid("8FCB85C6-6726-4C7A-8087-3462CC795C19"), Name = "10-20%", EnumValue = 10, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new VADisabilityRating { Id = new Guid("B962051D-471D-4317-9851-347395BE2EA9"), Name = "21-49%", EnumValue = 20, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new VADisabilityRating { Id = new Guid("75FE2CEC-319F-48F9-84CA-699F5D3A8595"), Name = "50-59%", EnumValue = 50, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new VADisabilityRating { Id = new Guid("75B5EF56-6379-4991-B7D3-79F52D90571C"), Name = "60-69%", EnumValue = 60, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new VADisabilityRating { Id = new Guid("68AE9B39-59CE-4A21-989F-DFA9AAC8C9E4"), Name = "70-79%", EnumValue = 70, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new VADisabilityRating { Id = new Guid("99F07CA9-7A48-4B89-8588-EF17C4A7C09D"), Name = "80-89%", EnumValue = 80, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new VADisabilityRating { Id = new Guid("FE6D0AF2-CB0D-4202-B62B-F499392774D9"), Name = "90-100%", EnumValue = 90, SortOrder = 1 });


            //ActiveDuty                = 1,
            //MedicallySeparatedRetired = 2,
            //MedicalHold               = 3,
            //Tdrl                      = 4,
            //Discharged                = 5,
            //Other                     = 6,
            //NonActivatedGuardReserve  = 7
            //ActivatedGuardReserve     = 8

            //Add the default Duty Status
            context.ListTypes.AddOrUpdate(new ListMilitaryServiceStatus() { Id = new Guid("EB176090-A876-4F50-8CC2-C784EE45B30C"), Name = "ActiveDuty", EnumValue = 1, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new ListMilitaryServiceStatus() { Id = new Guid("4BAC59F2-FB45-4CA5-BA8A-AAAB6337CFEC"), Name = "Activated Guard/Reserve", EnumValue = 8, SortOrder = 2 });
            context.ListTypes.AddOrUpdate(new ListMilitaryServiceStatus() { Id = new Guid("2D9A6634-52F6-4819-8AFF-6E4BFF97D09C"), Name = "Non Activated Guard Reserve", EnumValue = 7, SortOrder = 3 });
            context.ListTypes.AddOrUpdate(new ListMilitaryServiceStatus() { Id = new Guid("12897392-854A-427E-A218-3EB0188FBC16"), Name = "Discharged", EnumValue = 5, SortOrder = 4 });
            context.ListTypes.AddOrUpdate(new ListMilitaryServiceStatus() { Id = new Guid("006B7C95-56D3-4FB2-BEF1-A3F6F0F799CB"), Name = "Tdrl", EnumValue = 4, SortOrder = 5 });
            context.ListTypes.AddOrUpdate(new ListMilitaryServiceStatus() { Id = new Guid("B30EBC08-6E76-4109-AE05-C1E6442EAEEB"), Name = "Medical Hold", EnumValue = 3, SortOrder = 6 });
            context.ListTypes.AddOrUpdate(new ListMilitaryServiceStatus() { Id = new Guid("4D30B760-3C91-494E-86F2-60A25116A7BE"), Name = "Medically Separated/Retired", EnumValue = 2, SortOrder = 7 });
            context.ListTypes.AddOrUpdate(new ListMilitaryServiceStatus() { Id = new Guid("A56DC320-9943-4C56-9FCA-1D2C5F00F09D"), Name = "Other", EnumValue = 6, SortOrder = 8 });


            //NotDeployed  = 1,
            //Deployed     = 2,
            //DeployingSoon = 3
            context.ListTypes.AddOrUpdate(new ListMilitaryDeploymentStatus() { Id = new Guid("1C630F3E-7F55-488A-9047-2F0DC2307E67"), Name = "NotDeployed", EnumValue = 1, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new ListMilitaryDeploymentStatus() { Id = new Guid("D743F2F4-A042-48D6-A31A-E5F81A1285C7"), Name = "Deployed", EnumValue = 1, SortOrder = 2 });
            context.ListTypes.AddOrUpdate(new ListMilitaryDeploymentStatus() { Id = new Guid("FEE547A4-24AD-49FC-B473-1118E333F8CD"), Name = "DeployingSoon", EnumValue = 1, SortOrder = 3 });


            //Unknown = 1,
            //E1      = 2,
            //E2      = 3,
            //E3      = 4,
            //E4      = 5,
            //E5      = 6,
            //E6      = 7,
            //E7      = 8,
            //E8      = 9,
            //E9      = 10,
            //W1      = 11,
            //W2      = 12,
            //W3      = 13,
            //W4      = 14,
            //W5      = 15,
            //O1e     = 16,
            //O2e     = 17,
            //O3e     = 18,
            //O1      = 19,
            //O2      = 20,
            //O3      = 21,
            //O4      = 22,
            //O5      = 23,
            //O6      = 24,
            //O7      = 25,
            //O8      = 26,
            //O9      = 27,
            //O10     = 28

            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("F3402913-F889-471F-8F11-42E4C4593A5B"), Name = "Unknown", EnumValue = 1, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("77686722-A589-4D06-92B7-3625A5D2A28A"), Name = "E-1", EnumValue = 2, SortOrder = 2 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("982E46DC-5493-4DC9-A1C7-9824EFF8F81F"), Name = "E-2", EnumValue = 3, SortOrder = 3 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("6283B0C7-8E87-4F29-A4E1-DC49F381B9CD"), Name = "E-3", EnumValue = 4, SortOrder = 4 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("2B7E7CB2-5016-42E8-8405-AB28627B7DF4"), Name = "E-4", EnumValue = 5, SortOrder = 5 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("9EF50BEE-97A4-4EBC-8CB4-5BDCF081FEBE"), Name = "E-5", EnumValue = 6, SortOrder = 6 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("6C8DC54B-F40F-4AA2-B1C4-BA5DFC945373"), Name = "E-6", EnumValue = 7, SortOrder = 7 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("DF18625B-938F-4658-9B89-29F993C1B37E"), Name = "E-7", EnumValue = 8, SortOrder = 8 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("68509470-E161-4022-BA33-C9D8E591E9EA"), Name = "E-8", EnumValue = 9, SortOrder = 9 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("8126230D-ED1F-41E7-9372-4B46F85AD64E"), Name = "E-9", EnumValue = 10, SortOrder = 10 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("AB3961BD-9D2D-432C-8746-1B5D8F5B0D62"), Name = "WO-1", EnumValue = 11, SortOrder = 11 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("9548B05A-F492-41CC-874B-0E713937F41C"), Name = "WO-2", EnumValue = 12, SortOrder = 12 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("743AE350-DECA-40B8-B252-EEE62A67C38E"), Name = "WO-3", EnumValue =13, SortOrder = 13 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("5035D8BE-D8FD-4F02-8290-05EC8E320BDD"), Name = "WO-4", EnumValue = 14, SortOrder = 14 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("3ADA8506-5EF3-458E-88DF-A9D3F0242F0B"), Name = "WO-5", EnumValue = 15, SortOrder = 15 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("6BC82950-BBC9-4921-8973-D8E3CBCAA2D7"), Name = "O-1e", EnumValue = 16, SortOrder = 16 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("30000031-B173-4B79-B914-828F53FB6508"), Name = "O-2e", EnumValue = 17, SortOrder = 17 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("E5ED1D76-C260-4040-AB56-9D81750E932D"), Name = "O-3e", EnumValue = 18, SortOrder = 18 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("91394F35-45B6-4F72-831F-33A8D39535D1"), Name = "O-1", EnumValue = 19, SortOrder = 19 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("2CF292D0-DE74-4C3F-96EC-355EB6B0415D"), Name = "O-2", EnumValue = 20, SortOrder = 20 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("5AC4514A-5ABC-40BC-BFF7-6901A325EB7F"), Name = "O-3", EnumValue = 21, SortOrder = 21 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("5F052433-9802-45BF-99EC-F1FC9A9891D6"), Name = "O-4", EnumValue = 22, SortOrder = 22 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("FDD4BB04-520A-44F1-B1A6-F10A22D7D6A3"), Name = "O-5", EnumValue = 23, SortOrder = 23 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("AC6B3850-88D7-49E4-9603-EC6DD9AE0891"), Name = "O-6", EnumValue = 24, SortOrder = 24 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("233D68E4-597D-4E31-8DE6-1DE62894532C"), Name = "O-7", EnumValue = 25, SortOrder = 25 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("01FE6F93-D993-4227-BAFE-C3C7A75448D3"), Name = "O-8", EnumValue = 26, SortOrder = 26 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("FC31618F-F214-43BC-9914-626281C032A2"), Name = "O-9", EnumValue = 27, SortOrder = 27 });
            context.ListTypes.AddOrUpdate(new ListMilitaryRank { Id = new Guid("3E7695FF-8A42-442F-9BB7-04B34400CA7C"), Name = "O-10", EnumValue = 28, SortOrder = 28 });

            //None                = 0,
            //Unknown             = 1,
            //AirNationalGuard    = 2,
            //AirForce            = 3,
            //Army                = 4,
            //CoastGuard          = 5,
            //MarineCorps         = 6,
            //Navy                = 7,
            //AirForceReserves    = 8,
            //ArmyReserves        = 9,
            //MarineCorpsReserves = 10,
            //CoastGuardReserves  = 11,
            //ArmyNationalGuard   = 12,
            //NavyReserves        = 13

            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("154D6024-C557-4FF2-BA0F-75D3090DFFA3"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "None", EnumValue = 0, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("4A54E320-4BB3-4E70-ABD3-D573B0043D7C"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Unknown", EnumValue = 1, SortOrder = 2 });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("2C51BCD0-2E05-48C9-B7BF-CCFBFD695E9C"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Air Force", EnumValue = 3, SortOrder = 3, OptionalValue = true.ToString() });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("74D4E350-9556-4359-9C0B-19CE6FFFDF5F"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Army", EnumValue = 4, SortOrder = 4, OptionalValue = true.ToString() });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("453F4418-783B-45D5-8C4C-DEA1D440B7D9"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Marine Corps", EnumValue = 6, SortOrder = 5, OptionalValue = true.ToString() });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("A02D6B7D-BCB0-43FD-80D6-6654705225D0"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Navy", EnumValue = 7, SortOrder = 6, OptionalValue = true.ToString() });

            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("127BFDDC-E4D2-4B45-8DCA-AF283B4B6F99"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Air National Guard", EnumValue = 2, SortOrder = 7 });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("E46A61D6-B4F6-4730-A3CC-66444E3A87F9"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Army National Guard", EnumValue = 12, SortOrder = 8 });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("8423AB9B-84CD-4556-B234-FB2E9E61B9CF"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Coast Guard", EnumValue = 5, SortOrder = 9 });

            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("91D874C7-45E7-4749-BC11-CADC1A8696BC"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Air Force Reserves", EnumValue = 8, SortOrder = 10 });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("0AA7898B-3CDD-4EC6-A566-8D59B9C522F1"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Army Reserves", EnumValue = 9, SortOrder = 11 });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("706A938A-B5E3-4DF0-8C3B-17238E13D244"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Marine Corps Reserves", EnumValue = 10, SortOrder = 12 });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("AE90EE85-0B8E-4A0B-AC45-50897B56629A"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Coast Guard Reserves", EnumValue = 11, SortOrder = 13 });
            context.ListTypes.AddOrUpdate(new ListMilitaryBranch { Id = new Guid("E63D3B63-2D2D-4542-B6EB-7CF593415631"), CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"), Name = "Navy Reserves", EnumValue = 13, SortOrder = 14 });
            

            //NotApplicable         = 1,
            //VaClaimPending        = 2,
            //VaClaimNotFiled       = 3,
            //ReceivingVaDisability = 4,
            //VaAppealOpen          = 5
            context.ListTypes.AddOrUpdate(new ListMilitaryVeteranStatus { Id = new Guid("44ECA156-3C17-4CA1-87CA-2F32BBFECE6A"), Name = "Not Applicable", EnumValue = 1, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new ListMilitaryVeteranStatus { Id = new Guid("CC2ADF09-ACC6-4EBC-919E-53BF098B3A4B"), Name = "VA Claim Pending", EnumValue = 2, SortOrder = 2 });
            context.ListTypes.AddOrUpdate(new ListMilitaryVeteranStatus { Id = new Guid("68D523CE-4113-414B-BC96-8D34C719E8F5"), Name = "VA Claim Not Filed", EnumValue = 3, SortOrder = 3 });
            context.ListTypes.AddOrUpdate(new ListMilitaryVeteranStatus { Id = new Guid("49725515-D429-474A-99A7-3027B95D46BD"), Name = "Receiving VA Disability", EnumValue = 4, SortOrder = 4 });
            context.ListTypes.AddOrUpdate(new ListMilitaryVeteranStatus { Id = new Guid("4689B300-36A2-48F5-B1C3-5700B9B1D535"), Name = "VA Appeal Open", EnumValue = 5, SortOrder = 5 });


            ////Iraq        = 1,
            ////Afghanistan = 2,
            ////Other       = 3
            //context.ListTypes.AddOrUpdate(new ListMilitaryCombatTheater { Id = new Guid("869E2412-F6EF-462D-9BA5-CC6B501951B6"), CategoryListTypeId = new Guid("47FFEF52-6933-4787-B4F4-504D4484D429"), Name = "Iraq", EnumValue = 1, SortOrder = 1 });
            //context.ListTypes.AddOrUpdate(new ListMilitaryCombatTheater { Id = new Guid("4F3691DB-B6F2-4A82-828A-56F7CC401086"),CategoryListTypeId = new Guid("47FFEF52-6933-4787-B4F4-504D4484D429"), Name = "Afghanistan", EnumValue = 2, SortOrder = 2 });
            //context.ListTypes.AddOrUpdate(new ListMilitaryCombatTheater { Id = new Guid("B6289330-D517-44D0-A167-EC3A9C621838"),CategoryListTypeId = new Guid("47FFEF52-6933-4787-B4F4-504D4484D429"), Name = "Other", EnumValue = 3, SortOrder = 3 });

            ////InCombatTheaterOrSupportRegion            = 1,
            ////EnrouteToCombatTheaterOrSupportRegion     = 2,
            ////ReturnedFromCombatTheaterOrSupportRegion  = 3,CategoryListTypeId = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"),
            ////ReturningFromCombatTheaterOrSupportRegion = 4,
            ////NoneOfTheAbove   
            //todo: roy fix this                         = 5
            context.ListTypes.AddOrUpdate(new ListInjuryInflictionCategory { Id = new Guid("F38A6F91-504A-482F-B53D-8A1D42415D0B"), Name = "In Combat Theater Or Support Region", EnumValue = 1, SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new ListInjuryInflictionCategory { Id = new Guid("21E1A145-1125-4810-98C6-B1CB58ED7CBE"), Name = "Enroute To Combat Theater Or Support Region", EnumValue = 2, SortOrder = 2 });
            context.ListTypes.AddOrUpdate(new ListInjuryInflictionCategory { Id = new Guid("C14744BF-C192-4984-A153-53B5E1C71E19"), Name = "Returned From Combat Theater Or Support Region", EnumValue = 3, SortOrder = 3 });
            context.ListTypes.AddOrUpdate(new ListInjuryInflictionCategory { Id = new Guid("19B8C35B-49DF-4683-B74B-5F7C7B22AD76"), Name = "Returning From Combat Theater Or Support Region", EnumValue = 4, SortOrder = 4 });
            context.ListTypes.AddOrUpdate(new ListInjuryInflictionCategory { Id = new Guid("DC254B57-DC1D-4511-9051-20A8A8E50485"), Name = "None Of The Above", EnumValue = 5, SortOrder = 5 });

            //System Lookup tables
            context.ListTypes.AddOrUpdate(new Gender { Id = new Guid("90DF131F-E386-4E0F-BF45-302995293C19"), Name = "Male" });
            context.ListTypes.AddOrUpdate(new Gender { Id = new Guid("66CF1D64-35B1-4153-8913-E6CC367035AA"), Name = "Female" });

            context.ListTypes.AddOrUpdate(new ListType { Id = new Guid("C0A9FD9A-7D1C-4C44-8B06-25D4E4503575"), Name = "Mailing Address" });
            context.ListTypes.AddOrUpdate(new ListType { Id = new Guid("249EF9CC-8EC1-432D-BDDE-763F2F4DAFA2"), Name = "Physical Address" });
            context.ListTypes.AddOrUpdate(new ListType { Id = new Guid("01A8EF12-9562-4BBB-B261-88CC199096EB"), Name = "Temporary Address" });

          
            

        context.Households.AddOrUpdate(h => h.Name, new Household(){ Name = "Adams Family"}, new Household(){ Name = "Urquieta Family" }, new Household(){ Name = "Serna "});

        ICollection<Category> iCategories = new Collection<Category>();
        Category parentCategory = new Category
        {
            Id = new Guid("9AA83B36-CB9C-431F-8322-18D263FAAA1C"),
            Name = "Hearts of Valor Documents",
            ParentCategoryId = null

        };
        iCategories.Add(parentCategory);



        /************************** Default PersonCaregiver Attachment Categoies ***********************************/
        Category careCategory = new Category
        {
            Id = new Guid("2FD45319-341D-463E-944D-D11219116486"),
            Name = "PersonCaregiver",
            ParentCategory = parentCategory
        };
        iCategories.Add(careCategory);


        Category careDocsCategory = new Category
        {
            Id = new Guid("F55AE2C5-F70B-40B8-BC9F-95761EF4045B"),
            Name = "Documents",
            ParentCategory = careCategory
        };
        iCategories.Add(careDocsCategory);


        Category careOtherCategory = new Category
        {
            Id = new Guid("71BA2DA8-746B-42B9-94D6-A39AA8ECF7DB"),
            Name = "Other",
            ParentCategory = careCategory
        };
        iCategories.Add(careOtherCategory);

        /************************** Default Service Member Attachment Categoies ***********************************/
        Category serviceCategory = new Category
        {
            Id = new Guid("1CEDC98F-60FB-45BF-A6A8-ADC0E9478814"),
            Name = "Service Member",
            ParentCategory = parentCategory
        };
        iCategories.Add(serviceCategory);

        //service member docs 
        Category serviceDocsCategory = new Category
        {
            Id = new Guid("52FF59DF-3DD3-47AF-8B44-07474702DAE0"),
            Name = "Documents",
            ParentCategory = serviceCategory
        };
        iCategories.Add(serviceDocsCategory);

        Category serviceVACategory = new Category
        {
            Id = new Guid("A379C37A-207F-42A6-B065-A5FCE16E17A4"),
            Name = "VA Rating",
            ParentCategory = serviceCategory
        };
        iCategories.Add(serviceVACategory);

        Category serviceOtherCategory = new Category
        {
            Id = new Guid("6C280255-F50C-490A-99F8-FB1C194F8422"),
            Name = "Other",
            ParentCategory = serviceCategory
        };
        iCategories.Add(serviceOtherCategory);


        context.Applications.AddOrUpdate(new Application
        {
            Id = new Guid("173C79ED-2CF9-4B79-8CD4-6B2E6C14A090"),
            Name = "Hearts of Valor Application",
            Categories = iCategories,
            NameAbbreviation = "HOV",
            Description = "Hearts of valor application level document"

        });



        ICollection<Category> systemCetgories = new Collection<Category>();
        Category systemList = new Category
        {
            Id = new Guid("E383CED9-F9F9-465B-B4AC-51C6446F3EA8"),
            Name = "System Lists",
            ParentCategoryId = null

        };
        systemCetgories.Add(systemList);

        Category profileList = new Category
        {
            Id = new Guid("E7E2DEAF-5A22-4348-A2B1-11FC297F7B55"),
            Name = "Military Branches",
            ParentCategoryId = new Guid("E383CED9-F9F9-465B-B4AC-51C6446F3EA8")

        };
        systemCetgories.Add(profileList);


        Category listMilitaryCombatTheater = new Category
        {
            Id = new Guid("47FFEF52-6933-4787-B4F4-504D4484D429"),
            Name = "Military Combat Theater",
            ParentCategoryId = new Guid("E383CED9-F9F9-465B-B4AC-51C6446F3EA8")

        };
        systemCetgories.Add(profileList);

        context.Applications.AddOrUpdate(new Application
        {
            Id = new Guid("46EDF063-1BA0-453E-BA90-774FB958C915"),
            Name = "System",
            Categories = systemCetgories,
            NameAbbreviation = "SYS",
            Description = "System Level lookup tables"

        });


        //People
        context.Persons.AddOrUpdate(new Person
        {
            Id = new Guid("78989229-9919-4713-9DEA-3304BA9A2D99"),
            FirstName = "Roy",
            Lastname = "Serna",
            Last4Ssn = "1234",
            TimeStamp = new TimeStamp()
        }
        );

            //public enum PriorityLevel
            //{
            //    Priority1 = 1,
            //    Priority2 = 2,
            //    Priority3 = 3,
            //    Priority4 = 4
            //}

            context.ListTypes.AddOrUpdate(new PriorityLevels { Id = new Guid("37910897-E2BE-4E2F-8A26-91EA8C25F51C"), EnumValue = 1, Name = "1: Active duty, E1-E6 or post 9/11 wounded, ill or injured, all ranks", SortOrder = 1 });
            context.ListTypes.AddOrUpdate(new PriorityLevels { Id = new Guid("BCE2026C-1DF2-4458-B0A7-041CDD1AA1E5"), EnumValue = 2, Name = "2: Active duty, active guard or reserves, E1-E6 or post 9/11 wounded, ill or injured, all ranks", SortOrder = 2 });
            context.ListTypes.AddOrUpdate(new PriorityLevels { Id = new Guid("E518DD0A-D17D-4282-BCE1-D81E21482845"), EnumValue = 3, Name = "3: Active duty, active guard or reserves or post 9/11 wounded, ill or injured, all ranks", SortOrder = 3 });
            context.ListTypes.AddOrUpdate(new PriorityLevels { Id = new Guid("627FE187-787C-427F-869A-C505D128C7E6"), EnumValue = 4, Name = "4: Active duty, active and inactive guard or reserves, or post 9/11 wounded, ill or injured, all ranks", SortOrder = 4 });




            //public enum Grade
            //{
            //    NotApplicable = -1,
            //    PreK = 0,
            //    Kinder = 1,
            //    First = 2,
            //    Second = 3,
            //    Third = 4,
            //    Fourth = 5,
            //    Fifth = 6,
            //    Sixth = 7,
            //    Seventh = 8,
            //    Eighth = 9,
            //    Ninth = 10,
            //    Tenth = 11,
            //    Eleventh = 12,
            //    Twelfth = 13,
            //    None = 14
            //}
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("F524E737-0EA9-474C-B648-5AF52C0A0751"), EnumValue = -1, SortOrder = -1, Name = "N/A" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("E4D9AC38-8B8B-4C14-80BA-1B9C43CC0891"), EnumValue = 0, SortOrder = 0, Name = "PreK" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("1A5889C9-F6D7-4C9F-B34D-E41C8AA4E994"), EnumValue = 1, SortOrder = 1, Name = "Kinder" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("2BA8F69B-387F-49BE-A2E9-97E2258D1C41"), EnumValue = 2, SortOrder = 2, Name = "First" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("6F7D4545-AA97-4AEB-B7F3-E387F6EF3CE2"), EnumValue = 3, SortOrder = 3, Name = "Second" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("B121CC08-2C80-411A-A3D7-C5062755D5F0"), EnumValue = 4, SortOrder = 4, Name = "Third" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("85C5289D-692C-4FAF-9E2D-A64F1A3B6219"), EnumValue = 5, SortOrder = 5, Name = "Fourth" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("BBC56A1E-2E39-4A14-BAD2-876DD7CC3DE4"), EnumValue = 6, SortOrder = 6, Name = "Fifth" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("A3A51FAB-BC69-4C70-B1A7-AB3541AE0B4E"), EnumValue = 7, SortOrder = 7, Name = "Sixth" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("11B37827-911F-451A-9271-964D893706D2"), EnumValue = 8, SortOrder = 8, Name = "Seventh" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("4CDE3021-E7A9-4001-B7A3-4B037EC179D4"), EnumValue = 9, SortOrder = 9, Name = "Eighth" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("D28D4CB2-4490-44AA-B645-607BD3D94D84"), EnumValue = 10, SortOrder = 10, Name = "Ninth" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("A664A280-651A-42C6-8A0D-811433F69389"), EnumValue = 11, SortOrder = 11, Name = "Tenth" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("942E9327-0A8E-43CA-BFC2-5E33AFCD6F33"), EnumValue = 12, SortOrder = 12, Name = "Eleventh" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("952ECA77-C8D0-4BA7-BBA4-F7BC2A65FFE9"), EnumValue = 13, SortOrder = 13, Name = "Twelfth" });
            context.ListTypes.AddOrUpdate(new Grades { Id = new Guid("E341E509-D1FC-4CC7-8150-20F429EC5AFE"), EnumValue = 14, SortOrder = 14, Name = "None" });



            //public enum DistributionMemberRegistrationStatus
            //{
            //    None = 0,
            //    Registered = 1,
            //    Confirmed = 2,
            //    WaitListed = 3,
            //    Denied = 4,
            //    CancelledByStaff = 5,
            //    CancelledByUser = 6,
            //    LocationFull = 7,
            //    RegistrationAllocated = 8

            //}
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("22B7CFE5-E3FC-43A1-A733-11D39C3DC141"), EnumValue = 0, SortOrder = 0, Name = "None" });
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("79A30E23-F389-4236-AABA-73FCAF1B3191"), EnumValue = 1, SortOrder = 1, Name = "Registered Unconfirmed" });
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("94DCC691-E468-4B6D-83E6-3584CE378342"), EnumValue = 2, SortOrder = 2, Name = "Confirmed" });
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("471D88EA-BCBA-4445-AAB9-55DDF1FE9778"), EnumValue = 3, SortOrder = 3, Name = "Wait Listed" });
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("A6E2E26C-1447-49AF-9E25-9231D5271B27"), EnumValue = 4, SortOrder = 4, Name = "Denied" });
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("3E61AD0C-1D48-408E-BB7A-9F261A03A580"), EnumValue = 5, SortOrder = 5, Name = "Cancelled By Staff" });
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("73D17308-4621-4049-8F81-CADBC94FB501"), EnumValue = 6, SortOrder = 6, Name = "Cancelled By User" });
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("121FD2E6-6BCD-438C-9B86-A1CC44EE9977"), EnumValue = 7, SortOrder = 7, Name = "Location Full" });
            context.ListTypes.AddOrUpdate(new RegistationStatus { Id = new Guid("128C7819-17E0-4F61-BCB0-B8411148DBFF"), EnumValue = 8, SortOrder = 8, Name = "Registration Allocated for the form" });


            //public enum ShirtSize
            //{
            //    None = 0,
            //    SmallYouthShirt = 1,
            //    MediumYouthShirt = 2,
            //    SmallAdultShirt = 3,
            //    LargeYouthShirt = 4,
            //    MediumAdultShirt = 5,
            //    LargeAdultShirt = 6,
            //    XlAdultShirt = 7,
            //    XxlAdultShirt = 8
            //}
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("81F66AF8-8BDB-4635-B379-F22689E50263"), EnumValue = 0, SortOrder = 0, Name = "None" });
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("A8298BDE-C14C-489F-B0B4-365421F38FCD"), EnumValue = 1, SortOrder = 1, Name = "Small Youth Shirt" });
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("0DACF9F2-133B-4C65-83A5-C3F522373B8D"), EnumValue = 2, SortOrder = 2, Name = "Medium Youth Shirt" });
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("FFF337CE-7C84-4E80-9559-BCEB974F576B"), EnumValue = 3, SortOrder = 3, Name = "Small Adult Shirt" });
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("9DEDBFA3-FACC-4DDD-B640-259FD2A18BF7"), EnumValue = 4, SortOrder = 4, Name = "Large Youth Shirt" });
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("00B39A51-5D53-4A50-8B53-B0FCD3AC3CAD"), EnumValue = 5, SortOrder = 5, Name = "Medium Adult Shirt" });
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("4E633B45-77E4-43E5-8A83-72CA7EB6B6E9"), EnumValue = 6, SortOrder = 6, Name = "Large Adult Shirt" });
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("0927B355-5D79-44DB-9473-7188AE2514E2"), EnumValue = 7, SortOrder = 7, Name = "XL Adult Shirt" });
            context.ListTypes.AddOrUpdate(new ShirtSizes { Id = new Guid("E7663507-5F51-4721-BD72-1796431267AF"), EnumValue = 8, SortOrder = 8, Name = "XXL Adult Shirt" });

            var location1 = context.CollectionLocations.Add(new CollectionLocation() { AddressValid = true, OpeningOnUtc = DateTime.UtcNow, NotAvailable = false, AddressId = new Guid("5F576B9F-EFEF-4483-BFEF-2539A4EE815F") });
            var location2 = context.CollectionLocations.Add(new CollectionLocation() { AddressValid = true, OpeningOnUtc = DateTime.UtcNow, NotAvailable = false, AddressId = new Guid("9E7049CB-77F3-4CE8-8D97-5DF2AC5B7D97") });
            //var volunteer1 = (CollectionVolunteer)context.Persons.Add(new CollectionVolunteer() { FirstName = "Happy", Lastname = "Gilmore", ShirtSizeId = new Guid("E7663507-5F51-4721-BD72-1796431267AF") });
            //var volunteer2 = (CollectionVolunteer)context.Persons.Add(new CollectionVolunteer() { FirstName = "Big", Lastname = "Momma", ShirtSizeId = new Guid("E7663507-5F51-4721-BD72-1796431267AF") });
            //var volunteer3 = (CollectionVolunteer)context.Persons.Add(new CollectionVolunteer() { FirstName = "Ruth", Lastname = "Chris", ShirtSizeId = new Guid("E7663507-5F51-4721-BD72-1796431267AF") });
            var office1 = context.Offices.Add(new Office() { InternalName = "San Antonio" });
            var office2 = context.Offices.Add(new Office() { InternalName = "Austin" });
            var office3 = context.Offices.Add(new Office() { InternalName = "National" });
            var office4 = context.Offices.Add(new Office() { InternalName = "Dallas" });
            var office5 = context.Offices.Add(new Office() { InternalName = "Ft. Worth" });
            var office6 = context.Offices.Add(new Office() { InternalName = "Houston" });

            var _CollectionEvent1 = new CollectionEvent();
            _CollectionEvent1.EventClosed = true;
            _CollectionEvent1.Owner = office1;
            //_CollectionEvent1.Locations = new List<CollectionLocationMapping>();
            //_CollectionEvent1.Locations.Add(new CollectionLocationMapping() { CollectionEvent = _CollectionEvent1, CollectionLocation = location1 });
            //_CollectionEvent1.Locations.Add(new CollectionLocationMapping() { CollectionEvent = _CollectionEvent1, CollectionLocation = location2 });
            //CollectionEvent1.Volunteers = new List<CollectionVolunteerMapping>();
            //_CollectionEvent1.Volunteers.Add(new CollectionVolunteerMapping() { CollectionEvent = _CollectionEvent1, CollectionVolunteer = volunteer1 });
            //_CollectionEvent1.Volunteers.Add(new CollectionVolunteerMapping() { CollectionEvent = _CollectionEvent1, CollectionVolunteer = volunteer2 });
            //_CollectionEvent1.Volunteers.Add(new CollectionVolunteerMapping() { CollectionEvent = _CollectionEvent1, CollectionVolunteer = volunteer3 });

            var _CollectionEvent2 = new CollectionEvent();
            _CollectionEvent2.Owner = office2;
            _CollectionEvent2.Title = "Another Collection Event";
            //_CollectionEvent2.Locations = new List<CollectionLocationMapping>();
            //_CollectionEvent2.Locations.Add(new CollectionLocationMapping() { CollectionEvent = _CollectionEvent2, CollectionLocation = location1 });
            //_CollectionEvent2.Locations.Add(new CollectionLocationMapping() { CollectionEvent = _CollectionEvent2, CollectionLocation = location2 });
            //_CollectionEvent2.Volunteers = new List<CollectionVolunteerMapping>();
            //_CollectionEvent2.Volunteers.Add(new CollectionVolunteerMapping() { CollectionEvent = _CollectionEvent2, CollectionVolunteer = volunteer1 });
            //_CollectionEvent2.Volunteers.Add(new CollectionVolunteerMapping() { CollectionEvent = _CollectionEvent2, CollectionVolunteer = volunteer2 });

            var _CollectionEvent3 = new CollectionEvent();
            _CollectionEvent3.Owner = office3;
            _CollectionEvent3.Title = "A Collection Event";
            //_CollectionEvent3.Locations = new List<CollectionLocationMapping>();
            //_CollectionEvent3.Locations.Add(new CollectionLocationMapping() { CollectionEvent = _CollectionEvent3, CollectionLocation = location1 });
            //_CollectionEvent3.Locations.Add(new CollectionLocationMapping() { CollectionEvent = _CollectionEvent3, CollectionLocation = location2 });
            //_CollectionEvent3.Volunteers = new List<CollectionVolunteerMapping>();
            //_CollectionEvent3.Volunteers.Add(new CollectionVolunteerMapping() { CollectionEvent = _CollectionEvent3, CollectionVolunteer = volunteer1 });






            //var registrant1 = (DistributionEventRegistrant)context.Persons.Add(new DistributionEventRegistrant() { RegistrationStatusId = new Guid("79A30E23-F389-4236-AABA-73FCAF1B3191"), FirstName = "John", Lastname = "Doe", TimeStamp = new TimeStamp() });
            //var registrant2 = (DistributionEventRegistrant)context.Persons.Add(new DistributionEventRegistrant() { RegistrationStatusId = new Guid("471D88EA-BCBA-4445-AAB9-55DDF1FE9778"), FirstName = "Jane", Lastname = "Doe", TimeStamp = new TimeStamp() });

            var location3temp = new DistributionLocation();
            location3temp.PriorityLevelId = new Guid("37910897-E2BE-4E2F-8A26-91EA8C25F51C");
            location3temp.AddressId = new Guid("5F576B9F-EFEF-4483-BFEF-2539A4EE815F");
            location3temp.Registrants = new List<DistributionEventRegistrant>();
            //location3temp.Registrants.Add(registrant1);

            var location3 = (DistributionLocation)context.MeetingLocations.Add(location3temp);

            var location4temp = new DistributionLocation();
            location4temp.PriorityLevelId = new Guid("37910897-E2BE-4E2F-8A26-91EA8C25F51C");
            location4temp.AddressId = new Guid("9E7049CB-77F3-4CE8-8D97-5DF2AC5B7D97");
            location4temp.Registrants = new List<DistributionEventRegistrant>();
           // location4temp.Registrants.Add(registrant2);

            var location4 = (DistributionLocation)context.MeetingLocations.Add(location4temp);

            var _DistributionEvent1 = new DistributionEvent();
            _DistributionEvent1.Owner = office4;
            _DistributionEvent1.Participants = new List<Office>();
            _DistributionEvent1.Participants.Add(office5);
            _DistributionEvent1.Participants.Add(office6);
            _DistributionEvent1.Locations = new List<DistributionLocation>();
            _DistributionEvent1.Locations.Add(location3);
            _DistributionEvent1.Locations.Add(location4);

           


            context.Households.AddOrUpdate(h => h.Name, new Household() { Name = "Adams Family" }, new Household() { Name = "Urquieta Family" }, new Household() { Name = "Serna " });

            context.Reports.AddOrUpdate(new Report() { ReportId = "mafaNeedsMetLog", ReportName = "Mafa Needs Met Log", SelectLimit = 0, StoredProcedureName = "sp_Report_MAFA_Needs_Met_Case_Log_V2" });
            context.Reports.AddOrUpdate(new Report() { ReportId = "mafaNeedsMet", ReportName = "Mafa Needs Met", SelectLimit = 0, StoredProcedureName = "sp_Report_MAFA_Needs_Met_V2" });
        }
    }


}

