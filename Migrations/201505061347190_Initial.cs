namespace OH.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Address1 = c.String(maxLength: 250),
                        Address2 = c.String(maxLength: 250),
                        City = c.String(maxLength: 250),
                        State = c.String(maxLength: 250),
                        Zip = c.String(maxLength: 250),
                        Location = c.Geography(),
                        Verified = c.String(maxLength: 250),
                        County = c.String(maxLength: 250),
                        Country = c.String(maxLength: 250),
                        IsActive = c.Boolean(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.TimeStamps",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        RefId = c.Guid(nullable: false),
                        CreatedBy = c.String(maxLength: 250),
                        UtcCreatedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModifiedBy = c.String(maxLength: 250),
                        UtcLastModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ApplicationClasses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        ApplicationId = c.Guid(nullable: false),
                        EntityClassGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Applications", t => t.ApplicationId, cascadeDelete: true)
                .Index(t => t.ApplicationId);
            
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Description = c.String(maxLength: 250),
                        NameAbbreviation = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        ParentCategoryId = c.Guid(),
                        Name = c.String(maxLength: 250),
                        ChangeLog_Id = c.Guid(),
                        Owner_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChangeLogs", t => t.ChangeLog_Id)
                .ForeignKey("dbo.Persons", t => t.Owner_Id)
                .ForeignKey("dbo.Categories", t => t.ParentCategoryId)
                .Index(t => t.ParentCategoryId)
                .Index(t => t.ChangeLog_Id)
                .Index(t => t.Owner_Id);
            
            CreateTable(
                "dbo.CategoryAttachmentMapping",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        CategoryId = c.Guid(nullable: false),
                        AttachmentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attachments", t => t.AttachmentId, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.AttachmentId);
            
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        OriginalFileName = c.String(maxLength: 250),
                        FileName = c.String(maxLength: 250),
                        OwnerId = c.Guid(nullable: false),
                        RefId = c.Guid(nullable: false),
                        FileExtension = c.String(maxLength: 250),
                        FileStream = c.Binary(),
                        ChangeLog_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChangeLogs", t => t.ChangeLog_Id)
                .Index(t => t.ChangeLog_Id);
            
            CreateTable(
                "dbo.ChangeLogs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        EntityGuid = c.Guid(nullable: false),
                        Comment = c.String(maxLength: 2500),
                        TimeStamp_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id, cascadeDelete: true)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 250),
                        Lastname = c.String(maxLength: 250),
                        MiddleName = c.String(maxLength: 250),
                        DateOfBirth = c.DateTime(precision: 7, storeType: "datetime2"),
                        Title = c.String(maxLength: 250),
                        Suffix = c.String(maxLength: 250),
                        Gender = c.String(maxLength: 250),
                        DriverLicenceNumber = c.String(maxLength: 250),
                        DriverLicenseState = c.String(maxLength: 250),
                        DriverLicenseExpirationDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        Last4Ssn = c.String(maxLength: 250),
                        SocialSecurityNumber = c.String(maxLength: 250),
                        PrimaryEmailId = c.Guid(),
                        PrimaryAddressId = c.Guid(),
                        PrimaryPhoneId = c.Guid(),
                        ShirtSizeId = c.Guid(),
                        MeetingLocation_Id = c.Guid(),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeetingLocations", t => t.MeetingLocation_Id)
                .ForeignKey("dbo.Phones", t => t.PrimaryPhoneId)
                .ForeignKey("dbo.Addresses", t => t.PrimaryAddressId)
                .ForeignKey("dbo.EmailAddresses", t => t.PrimaryEmailId)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.PrimaryEmailId)
                .Index(t => t.PrimaryAddressId)
                .Index(t => t.PrimaryPhoneId)
                .Index(t => t.MeetingLocation_Id)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.PersonAddressMapping",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        AddressId = c.Guid(nullable: false),
                        PersonId = c.Guid(nullable: false),
                        AddressType = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.AddressId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.PersonEmailAddresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        EmailAddressId = c.Guid(nullable: false),
                        PersonId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EmailAddresses", t => t.EmailAddressId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.EmailAddressId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.EmailAddresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 250),
                        DoNotSend = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MeetingLocations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        AddressId = c.Guid(nullable: false),
                        Name = c.String(maxLength: 250),
                        Description = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        PhoneNumber = c.String(maxLength: 250),
                        Extension = c.String(maxLength: 250),
                        PhoneType = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MeetingRooms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Description = c.String(maxLength: 250),
                        Number = c.String(maxLength: 250),
                        Capacity = c.Int(nullable: false),
                        MeetingLocationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeetingLocations", t => t.MeetingLocationId, cascadeDelete: true)
                .Index(t => t.MeetingLocationId);
            
            CreateTable(
                "dbo.DistributionEventRegistrants",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        RegistrationEmailSent = c.DateTime(precision: 7, storeType: "datetime2"),
                        ConfirmationEmailSent = c.DateTime(precision: 7, storeType: "datetime2"),
                        ReminderEmailSent = c.DateTime(precision: 7, storeType: "datetime2"),
                        TimeSlot = c.DateTime(precision: 7, storeType: "datetime2"),
                        RegistrationStatusId = c.Guid(nullable: false),
                        RegistrantFirstName = c.String(maxLength: 250),
                        RegistrantLastname = c.String(maxLength: 250),
                        RegistrantEmail = c.String(maxLength: 250),
                        PersonId = c.Guid(nullable: false),
                        DistributionLocation_Id = c.Guid(),
                        DistributionEvent_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.DistributionLocations", t => t.DistributionLocation_Id)
                .ForeignKey("dbo.DistributionEvents", t => t.DistributionEvent_Id)
                .Index(t => t.PersonId)
                .Index(t => t.DistributionLocation_Id)
                .Index(t => t.DistributionEvent_Id);
            
            CreateTable(
                "dbo.DistributionEventAttendees",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Shirtsize = c.Guid(),
                        Attendee_Id = c.Guid(),
                        DistributionEventRegistrant_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.Attendee_Id)
                .ForeignKey("dbo.DistributionEventRegistrants", t => t.DistributionEventRegistrant_Id)
                .Index(t => t.Attendee_Id)
                .Index(t => t.DistributionEventRegistrant_Id);
            
            CreateTable(
                "dbo.PersonPhoneMapping",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        PhoneId = c.Guid(nullable: false),
                        PersonId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.Phones", t => t.PhoneId, cascadeDelete: true)
                .Index(t => t.PhoneId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.CategoryItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Text = c.String(maxLength: 250),
                        SpriteCssClass = c.String(maxLength: 250),
                        ImageUrl = c.String(maxLength: 250),
                        Expanded = c.Boolean(nullable: false),
                        HasChildren = c.Boolean(nullable: false),
                        ParentCategoryItem_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryItems", t => t.ParentCategoryItem_Id)
                .Index(t => t.ParentCategoryItem_Id);
            
            CreateTable(
                "dbo.CollectionEvents",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RegistrationRadius = c.Int(nullable: false),
                        MaxStoresPerVolunteer = c.Int(nullable: false),
                        CollectionBeginsUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        CollectionEndsUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Partner = c.String(maxLength: 250),
                        EventTypeId = c.Guid(nullable: false),
                        PublishToEventCalendar = c.Boolean(),
                        FormNbr = c.String(maxLength: 250),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Title = c.String(maxLength: 250),
                        Description = c.String(maxLength: 250),
                        PublishToWebUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        UnpublishFromWebUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        RegistrationOpensUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        RegistrationClosesUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EventClosed = c.Boolean(nullable: false),
                        RemoveFromWeb = c.Boolean(),
                        ContactListName = c.String(maxLength: 250),
                        OwnerId = c.Guid(nullable: false),
                        ParentEventKey = c.String(maxLength: 250),
                        Meeting_Id = c.Guid(),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Meetings", t => t.Meeting_Id)
                .ForeignKey("dbo.Offices", t => t.OwnerId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.OwnerId)
                .Index(t => t.Meeting_Id)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.CollectionLocations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        AddressId = c.Guid(nullable: false),
                        Name = c.String(maxLength: 250),
                        Description = c.String(maxLength: 250),
                        PointOfInterest = c.String(maxLength: 250),
                        PointOfContact = c.String(maxLength: 250),
                        StoreNumber = c.Int(nullable: false),
                        HoursOfOperation = c.Double(nullable: false),
                        AddressValid = c.Boolean(nullable: false),
                        NotAvailable = c.Boolean(nullable: false),
                        OpeningOnUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        TimeStamp_Id = c.Guid(),
                        CollectionEvent_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .ForeignKey("dbo.CollectionEvents", t => t.CollectionEvent_Id)
                .Index(t => t.AddressId)
                .Index(t => t.TimeStamp_Id)
                .Index(t => t.CollectionEvent_Id);
            
            CreateTable(
                "dbo.CollectionVolunteers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        PickupRadiusLimit = c.Int(nullable: false),
                        MaxLocationsAllowed = c.Int(nullable: false),
                        AffiliatedWith = c.String(maxLength: 250),
                        DistributingTo = c.String(maxLength: 250),
                        MilitaryBase = c.String(maxLength: 250),
                        Unit = c.String(maxLength: 250),
                        PledgeConfirmedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PersonId = c.Guid(nullable: false),
                        CollectionLocation_Id = c.Guid(),
                        CollectionEvent_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.CollectionLocations", t => t.CollectionLocation_Id)
                .ForeignKey("dbo.CollectionEvents", t => t.CollectionEvent_Id)
                .Index(t => t.PersonId)
                .Index(t => t.CollectionLocation_Id)
                .Index(t => t.CollectionEvent_Id);
            
            CreateTable(
                "dbo.Meetings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Title = c.String(maxLength: 250),
                        Description = c.String(maxLength: 250),
                        MeetingLocationId = c.Guid(),
                        MeetingRoomId = c.Guid(),
                        IsAllDay = c.Boolean(nullable: false),
                        RecurrenceRule = c.String(maxLength: 250),
                        RecurrenceId = c.Int(),
                        RecurrenceException = c.String(maxLength: 250),
                        StartTimezone = c.String(maxLength: 250),
                        EndTimezone = c.String(maxLength: 250),
                        ParentMeeting_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeetingLocations", t => t.MeetingLocationId)
                .ForeignKey("dbo.Meetings", t => t.ParentMeeting_Id)
                .Index(t => t.MeetingLocationId)
                .Index(t => t.ParentMeeting_Id);
            
            CreateTable(
                "dbo.MeetingAttendees",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        MeetingId = c.Guid(nullable: false),
                        PersonId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Meetings", t => t.MeetingId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.MeetingId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Offices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        InternalName = c.String(maxLength: 250),
                        CollectionEvent_Id = c.Guid(),
                        DistributionEvent_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionEvents", t => t.CollectionEvent_Id)
                .ForeignKey("dbo.DistributionEvents", t => t.DistributionEvent_Id)
                .Index(t => t.CollectionEvent_Id)
                .Index(t => t.DistributionEvent_Id);
            
            CreateTable(
                "dbo.CollectionVolunteerMapping",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        CollectionEventId = c.Guid(nullable: false),
                        CollectionVolunteerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CollectionEvents", t => t.CollectionEventId, cascadeDelete: true)
                .ForeignKey("dbo.CollectionVolunteers", t => t.CollectionVolunteerId, cascadeDelete: true)
                .Index(t => t.CollectionEventId)
                .Index(t => t.CollectionVolunteerId);
            
            CreateTable(
                "dbo.DistributionEvents",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AutoConfirm = c.Boolean(),
                        WaitList = c.Boolean(),
                        WaitListMax = c.Int(nullable: false),
                        CloseOnFull = c.Boolean(),
                        EtapCampaign = c.String(maxLength: 250),
                        EtapApproach = c.String(maxLength: 250),
                        EtapGiftOwner = c.String(maxLength: 250),
                        EtapFund = c.String(maxLength: 250),
                        RegAckEmailText = c.String(maxLength: 250),
                        RegWaitListEmailText = c.String(maxLength: 250),
                        RegConfirmEmailText = c.String(maxLength: 250),
                        RegReminderEmailText = c.String(maxLength: 250),
                        Reminder1Date = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder2Date = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder3Date = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder1SentDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder2SentDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder3SentDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        CountScheme = c.Int(nullable: false),
                        ServiceMember = c.Boolean(),
                        Mother = c.Boolean(),
                        Father = c.Boolean(),
                        Brother = c.Boolean(),
                        Sister = c.Boolean(),
                        Son = c.Boolean(),
                        Daughter = c.Boolean(),
                        Spouse = c.Boolean(),
                        Friend = c.Boolean(),
                        StepSon = c.Boolean(),
                        StepDaughter = c.Boolean(),
                        Other = c.Boolean(),
                        PrivateEvent = c.Boolean(),
                        AgeMin = c.Int(nullable: false),
                        AgeMax = c.Int(nullable: false),
                        GradeMinId = c.Guid(),
                        GradeMaxId = c.Guid(),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Partner = c.String(maxLength: 250),
                        EventTypeId = c.Guid(nullable: false),
                        PublishToEventCalendar = c.Boolean(),
                        FormNbr = c.String(maxLength: 250),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Title = c.String(maxLength: 250),
                        Description = c.String(maxLength: 250),
                        PublishToWebUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        UnpublishFromWebUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        RegistrationOpensUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        RegistrationClosesUtc = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EventClosed = c.Boolean(nullable: false),
                        RemoveFromWeb = c.Boolean(),
                        ContactListName = c.String(maxLength: 250),
                        OwnerId = c.Guid(nullable: false),
                        ParentEventKey = c.String(maxLength: 250),
                        Meeting_Id = c.Guid(),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Meetings", t => t.Meeting_Id)
                .ForeignKey("dbo.Offices", t => t.OwnerId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.OwnerId)
                .Index(t => t.Meeting_Id)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.HouseholdMembers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Grade = c.String(maxLength: 250),
                        HouseholdId = c.Guid(nullable: false),
                        HouseholdMemberType = c.Guid(nullable: false),
                        IsDependent = c.Boolean(nullable: false),
                        IsPrimary = c.Boolean(nullable: false),
                        IsServiceMember = c.Boolean(nullable: false),
                        LivesWith = c.String(maxLength: 250),
                        PersonId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Households", t => t.HouseholdId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.HouseholdId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Households",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ListTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        SortOrder = c.Int(),
                        ShowInCombo = c.Boolean(),
                        EnumValue = c.Int(),
                        CategoryListTypeId = c.Guid(),
                        OptionalValue = c.String(maxLength: 250),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormControl",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        SequencerId = c.Int(nullable: false),
                        FormDataType = c.Int(nullable: false),
                        Required = c.Boolean(nullable: false),
                        Label = c.String(maxLength: 250),
                        DataName = c.String(maxLength: 250),
                        FullWidthProperties = c.Boolean(nullable: false),
                        Fields = c.String(maxLength: 250),
                        Validate = c.Boolean(nullable: false),
                        CheckBoxProperties_Id = c.Guid(),
                        DateTimeProperties_Id = c.Guid(),
                        DescriptionProperties_Id = c.Guid(),
                        EmbeddedHtmlProperties_Id = c.Guid(),
                        FileUploadProperties_Id = c.Guid(),
                        Form_Id = c.Guid(),
                        NumberProperties_Id = c.Guid(),
                        RadioButtonsProperties_Id = c.Guid(),
                        Section_Id = c.Guid(),
                        SelectListProperties_Id = c.Guid(),
                        ServiceMemberProperties_Id = c.Guid(),
                        TimeStamp_Id = c.Guid(),
                        UmbracoProperties_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OHFormCheckBoxProperties", t => t.CheckBoxProperties_Id)
                .ForeignKey("dbo.OHFormDateTimeProperties", t => t.DateTimeProperties_Id)
                .ForeignKey("dbo.OHFormDescriptionProperties", t => t.DescriptionProperties_Id)
                .ForeignKey("dbo.OHFormEmbeddedHtmlProperties", t => t.EmbeddedHtmlProperties_Id)
                .ForeignKey("dbo.OHFormFileUploadProperties", t => t.FileUploadProperties_Id)
                .ForeignKey("dbo.OHForm", t => t.Form_Id)
                .ForeignKey("dbo.OHFormNumberProperties", t => t.NumberProperties_Id)
                .ForeignKey("dbo.OHFormRadioButtonsProperties", t => t.RadioButtonsProperties_Id)
                .ForeignKey("dbo.OHFormSection", t => t.Section_Id)
                .ForeignKey("dbo.OHFormSelectListProperties", t => t.SelectListProperties_Id)
                .ForeignKey("dbo.OHFormServiceMemberProperties", t => t.ServiceMemberProperties_Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .ForeignKey("dbo.OHFormUmbracoProperties", t => t.UmbracoProperties_Id)
                .Index(t => t.CheckBoxProperties_Id)
                .Index(t => t.DateTimeProperties_Id)
                .Index(t => t.DescriptionProperties_Id)
                .Index(t => t.EmbeddedHtmlProperties_Id)
                .Index(t => t.FileUploadProperties_Id)
                .Index(t => t.Form_Id)
                .Index(t => t.NumberProperties_Id)
                .Index(t => t.RadioButtonsProperties_Id)
                .Index(t => t.Section_Id)
                .Index(t => t.SelectListProperties_Id)
                .Index(t => t.ServiceMemberProperties_Id)
                .Index(t => t.TimeStamp_Id)
                .Index(t => t.UmbracoProperties_Id);
            
            CreateTable(
                "dbo.OHFormCheckBoxProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Text = c.String(maxLength: 250),
                        Checked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormDateTimeProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        DateType = c.String(maxLength: 250),
                        DefaultDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormDescriptionProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormEmbeddedHtmlProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        RawHtml = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormFileUploadProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        AllowedExtensions = c.String(maxLength: 250),
                        UploadButtonText = c.String(maxLength: 250),
                        MaxKbFileSize = c.Int(nullable: false),
                        UploaderHtml = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHForm",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        SubmitButtonText = c.String(maxLength: 250),
                        CancelButtonText = c.String(maxLength: 250),
                        Theme_Id = c.Guid(),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OHFormTheme", t => t.Theme_Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.Theme_Id)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.OHFormTheme",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        ShowBorder = c.Boolean(nullable: false),
                        TopOfFormImage = c.String(maxLength: 250),
                        CornerRadius = c.Int(nullable: false),
                        FontName = c.String(maxLength: 250),
                        FontSize = c.String(maxLength: 250),
                        ShowSectionBorder = c.Boolean(nullable: false),
                        SectionFontName = c.String(maxLength: 250),
                        SectionFontSize = c.String(maxLength: 250),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.OHFormNumberProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        MinValue = c.Int(nullable: false),
                        MaxValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormRadioButtonsProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Orientation = c.Int(nullable: false),
                        RadioName = c.String(maxLength: 250),
                        RadioValues = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormSection",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        SectionTitle = c.String(maxLength: 250),
                        SectionDescription = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormSelectListProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        SelectListOptions = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormServiceMemberProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        EmailAddressDisplay = c.Boolean(nullable: false),
                        EmailAddressValidate = c.Boolean(nullable: false),
                        EmailAddressPrefill = c.Boolean(nullable: false),
                        SMFirstNameDisplay = c.Boolean(nullable: false),
                        SMFirstNameValidate = c.Boolean(nullable: false),
                        SMFirstNamePrefill = c.Boolean(nullable: false),
                        SMLastNameDisplay = c.Boolean(nullable: false),
                        SMLastNameValidate = c.Boolean(nullable: false),
                        SMLastNamePrefill = c.Boolean(nullable: false),
                        SMLast4SSNDisplay = c.Boolean(nullable: false),
                        SMLast4SSNValidate = c.Boolean(nullable: false),
                        SMLast4SSNPrefill = c.Boolean(nullable: false),
                        SecQuestionDisplay = c.Boolean(nullable: false),
                        SecQuestionValidate = c.Boolean(nullable: false),
                        SecQuestionPrefill = c.Boolean(nullable: false),
                        SecAnswerDisplay = c.Boolean(nullable: false),
                        SecAnswerValidate = c.Boolean(nullable: false),
                        SecAnswerPrefill = c.Boolean(nullable: false),
                        EmailAddressConfirmedDisplay = c.Boolean(nullable: false),
                        EmailAddressConfirmedValidate = c.Boolean(nullable: false),
                        EmailAddressConfirmedPrefill = c.Boolean(nullable: false),
                        FirstNameDisplay = c.Boolean(nullable: false),
                        FirstNameValidate = c.Boolean(nullable: false),
                        FirstNamePrefill = c.Boolean(nullable: false),
                        LastNameDisplay = c.Boolean(nullable: false),
                        LastNameValidate = c.Boolean(nullable: false),
                        LastNamePrefill = c.Boolean(nullable: false),
                        MailingAddressDisplay = c.Boolean(nullable: false),
                        MailingAddressValidate = c.Boolean(nullable: false),
                        MailingAddressPrefill = c.Boolean(nullable: false),
                        PhysicalAddressDisplay = c.Boolean(nullable: false),
                        PhysicalAddressValidate = c.Boolean(nullable: false),
                        PhysicalAddressPrefill = c.Boolean(nullable: false),
                        DateOfBirthDisplay = c.Boolean(nullable: false),
                        DateOfBirthValidate = c.Boolean(nullable: false),
                        DateOfBirthPrefill = c.Boolean(nullable: false),
                        SendNewsletterDisplay = c.Boolean(nullable: false),
                        SendNewsletterValidate = c.Boolean(nullable: false),
                        SendNewsletterPrefill = c.Boolean(nullable: false),
                        SendTextMsgNotificationsDisplay = c.Boolean(nullable: false),
                        SendTextMsgNotificationsVaildate = c.Boolean(nullable: false),
                        SendTextMsgNotificationsPrefill = c.Boolean(nullable: false),
                        TimeStampDisplay = c.Boolean(nullable: false),
                        TimeStampValidate = c.Boolean(nullable: false),
                        TimeStampPrefill = c.Boolean(nullable: false),
                        GoldStarFamilyDisplay = c.Boolean(nullable: false),
                        GoldStarFamilyValidate = c.Boolean(nullable: false),
                        GoldStarFamilyPrefill = c.Boolean(nullable: false),
                        ShirtSizeDisplay = c.Boolean(nullable: false),
                        ShirtSizeVaildate = c.Boolean(nullable: false),
                        ShirtSizePrefill = c.Boolean(nullable: false),
                        PhoneDisplay = c.Boolean(nullable: false),
                        PhoneValidate = c.Boolean(nullable: false),
                        PhonePrefill = c.Boolean(nullable: false),
                        MobilePhoneDisplay = c.Boolean(nullable: false),
                        MobilePhoneValidate = c.Boolean(nullable: false),
                        MobilePhonePrefill = c.Boolean(nullable: false),
                        DocumentsDisplay = c.Boolean(nullable: false),
                        DocumentsValidate = c.Boolean(nullable: false),
                        DocumentsPrefill = c.Boolean(nullable: false),
                        DependentsDisplay = c.Boolean(nullable: false),
                        DependentsValidate = c.Boolean(nullable: false),
                        DependentsPrefill = c.Boolean(nullable: false),
                        ActiveDutyDisplay = c.Boolean(nullable: false),
                        ActiveDutyValidate = c.Boolean(nullable: false),
                        ActiveDutyPrefill = c.Boolean(nullable: false),
                        CommandDisplay = c.Boolean(nullable: false),
                        CommandValidate = c.Boolean(nullable: false),
                        CommandPrefill = c.Boolean(nullable: false),
                        ReservesDisplay = c.Boolean(nullable: false),
                        ReservesValidate = c.Boolean(nullable: false),
                        ReservesPrefill = c.Boolean(nullable: false),
                        WoundedDisplay = c.Boolean(nullable: false),
                        WoundedValidate = c.Boolean(nullable: false),
                        WoundedPrefill = c.Boolean(nullable: false),
                        YrsServiceDisplay = c.Boolean(nullable: false),
                        YrsServiceValidate = c.Boolean(nullable: false),
                        YrsServicePrefill = c.Boolean(nullable: false),
                        MilitaryBaseDisplay = c.Boolean(nullable: false),
                        MilitaryBaseValidate = c.Boolean(nullable: false),
                        MilitaryBasePrefill = c.Boolean(nullable: false),
                        BranchDisplay = c.Boolean(nullable: false),
                        BranchValidate = c.Boolean(nullable: false),
                        BranchPrefill = c.Boolean(nullable: false),
                        RankDisplay = c.Boolean(nullable: false),
                        RankValidate = c.Boolean(nullable: false),
                        RankPrefill = c.Boolean(nullable: false),
                        DeployedStatusDisplay = c.Boolean(nullable: false),
                        DeployedStatusValidate = c.Boolean(nullable: false),
                        DeployedStatusPrefill = c.Boolean(nullable: false),
                        MilitaryStatusDisplay = c.Boolean(nullable: false),
                        MilitaryStatusValidate = c.Boolean(nullable: false),
                        MilitaryStatusPrefill = c.Boolean(nullable: false),
                        VaStatusDisplay = c.Boolean(nullable: false),
                        VaStatusValidate = c.Boolean(nullable: false),
                        VaStatusPrefill = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormUmbracoProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        UmbracoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OHFormValue",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        DataValue = c.String(maxLength: 250),
                        DataName = c.String(maxLength: 250),
                        Control_Id = c.Guid(),
                        Form_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OHFormControl", t => t.Control_Id)
                .ForeignKey("dbo.OHForm", t => t.Form_Id)
                .Index(t => t.Control_Id)
                .Index(t => t.Form_Id);
            
            CreateTable(
                "dbo.PersonCaregiver",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        AgeRange = c.String(maxLength: 250),
                        DurationOfTimeProvidingCare = c.Guid(),
                        GreatestChallengeAsCaregiver = c.String(maxLength: 250),
                        HasChildren = c.Boolean(nullable: false),
                        HobbiesOrInterests = c.String(maxLength: 250),
                        HoursProvidingCareWeekly = c.Guid(),
                        HowManyChildren = c.Int(nullable: false),
                        NeedsUpdataing = c.Boolean(nullable: false),
                        NotifyIfOpportunity = c.Boolean(nullable: false),
                        Occupation = c.String(maxLength: 250),
                        ProfileActive = c.Boolean(nullable: false),
                        RegisteredCaregiverWithVa = c.Boolean(nullable: false),
                        RelationToVeteran = c.Guid(),
                        SendNewsletter = c.Boolean(nullable: false),
                        TellAboutWork = c.String(maxLength: 250),
                        WorkOutsideHome = c.Boolean(nullable: false),
                        PersonId = c.Guid(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.PersonId)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.PersonMilitary",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        BranchOfServiceId = c.Guid(),
                        CommandPost = c.String(maxLength: 250),
                        DeploymentStatusId = c.Guid(nullable: false),
                        MilitaryRankId = c.Guid(),
                        NeedsUpdataing = c.Boolean(nullable: false),
                        ServiceStatusId = c.Guid(),
                        VeteranStatusId = c.Guid(),
                        Wounded = c.Boolean(nullable: false),
                        YearsServed = c.Int(nullable: false),
                        PersonId = c.Guid(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.PersonId)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.PersonMilitaryInjuryInflictions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        BriefDescription = c.String(maxLength: 250),
                        DetailDescription = c.String(maxLength: 250),
                        TypeOfInjury = c.Guid(),
                        Cause = c.String(maxLength: 250),
                        LocationId = c.Guid(),
                        InjuryInflictionCategoryId = c.Guid(nullable: false),
                        DateOfOccurrence = c.DateTime(),
                        Disposition = c.Guid(),
                        ProfileMilitaryId = c.Guid(nullable: false),
                        ListInjuryInflictionCategory_Id = c.Guid(),
                        TimeStamp_Id = c.Guid(),
                        PersonMilitary_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ListTypes", t => t.ListInjuryInflictionCategory_Id)
                .ForeignKey("dbo.ListTypes", t => t.LocationId)
                .ForeignKey("dbo.ProfileMilitary", t => t.ProfileMilitaryId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .ForeignKey("dbo.PersonMilitary", t => t.PersonMilitary_Id)
                .Index(t => t.LocationId)
                .Index(t => t.ProfileMilitaryId)
                .Index(t => t.ListInjuryInflictionCategory_Id)
                .Index(t => t.TimeStamp_Id)
                .Index(t => t.PersonMilitary_Id);
            
            CreateTable(
                "dbo.ProfileMilitary",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        MilitaryProfileId = c.Guid(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonMilitary", t => t.MilitaryProfileId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.MilitaryProfileId)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.WorkflowItem",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        End = c.DateTime(nullable: false),
                        Expanded = c.Boolean(nullable: false),
                        OrderId = c.Int(nullable: false),
                        ParentWorkFlowItemId = c.Guid(),
                        PercentComplete = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Start = c.DateTime(nullable: false),
                        Summary = c.Boolean(nullable: false),
                        Title = c.String(maxLength: 250),
                        WorkFlowId = c.Guid(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .ForeignKey("dbo.Workflow", t => t.WorkFlowId, cascadeDelete: true)
                .Index(t => t.WorkFlowId)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.ProfileCaregiver",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        ProfileId = c.Guid(),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonCaregiver", t => t.ProfileId)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.ProfileId)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.Workflow",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Owner = c.Guid(),
                        Summary = c.String(maxLength: 250),
                        Title = c.String(maxLength: 250),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.ProfileCaregiverAttachments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        AttachmentId = c.Guid(nullable: false),
                        ProfileCaregiverId = c.Guid(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attachments", t => t.AttachmentId, cascadeDelete: true)
                .ForeignKey("dbo.ProfileCaregiver", t => t.ProfileCaregiverId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.AttachmentId)
                .Index(t => t.ProfileCaregiverId)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.ProfileMilitaryAttachments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        ProfileMilitaryId = c.Guid(nullable: false),
                        AttachmentId = c.Guid(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attachments", t => t.AttachmentId, cascadeDelete: true)
                .ForeignKey("dbo.ProfileMilitary", t => t.ProfileMilitaryId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.ProfileMilitaryId)
                .Index(t => t.AttachmentId)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.ProfileMilitaryWorkflowItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        WorkflowItemId = c.Guid(nullable: false),
                        ProfileMilitaryId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProfileMilitary", t => t.ProfileMilitaryId, cascadeDelete: true)
                .ForeignKey("dbo.WorkflowItem", t => t.WorkflowItemId, cascadeDelete: true)
                .Index(t => t.WorkflowItemId)
                .Index(t => t.ProfileMilitaryId);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        ReportId = c.String(maxLength: 250),
                        ReportName = c.String(maxLength: 250),
                        SelectLimit = c.Int(nullable: false),
                        StoredProcedureName = c.String(maxLength: 250),
                        ApplicationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Title = c.String(maxLength: 250),
                        Description = c.String(maxLength: 250),
                        OwnerId = c.Int(),
                        IsAllDay = c.Boolean(nullable: false),
                        RecurrenceRule = c.String(maxLength: 250),
                        RecurrenceId = c.Int(),
                        RecurrenceException = c.String(maxLength: 250),
                        StartTimezone = c.String(maxLength: 250),
                        EndTimezone = c.String(maxLength: 250),
                        ParentTask_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tasks", t => t.ParentTask_Id)
                .Index(t => t.ParentTask_Id);
            
            CreateTable(
                "dbo.WorkflowItemAssignment",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        ResourceId = c.Guid(nullable: false),
                        Units = c.Decimal(nullable: false, precision: 18, scale: 2),
                        WorkFlowItemId = c.Guid(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.ResourceId, cascadeDelete: true)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .ForeignKey("dbo.WorkflowItem", t => t.WorkFlowItemId, cascadeDelete: true)
                .Index(t => t.ResourceId)
                .Index(t => t.WorkFlowItemId)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.WorkflowItemDependency",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClusterId = c.Int(nullable: false, identity: true),
                        DependencyId = c.Guid(nullable: false),
                        PredecessorId = c.Guid(nullable: false),
                        SuccessorId = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        TimeStamp_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .Index(t => t.TimeStamp_Id);
            
            CreateTable(
                "dbo.CategoryApplications",
                c => new
                    {
                        Category_Id = c.Guid(nullable: false),
                        Application_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Category_Id, t.Application_Id })
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .ForeignKey("dbo.Applications", t => t.Application_Id, cascadeDelete: true)
                .Index(t => t.Category_Id)
                .Index(t => t.Application_Id);
            
            CreateTable(
                "dbo.PhoneMeetingLocations",
                c => new
                    {
                        Phone_Id = c.Guid(nullable: false),
                        MeetingLocation_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Phone_Id, t.MeetingLocation_Id })
                .ForeignKey("dbo.Phones", t => t.Phone_Id, cascadeDelete: true)
                .ForeignKey("dbo.MeetingLocations", t => t.MeetingLocation_Id, cascadeDelete: true)
                .Index(t => t.Phone_Id)
                .Index(t => t.MeetingLocation_Id);
            
            CreateTable(
                "dbo.EmailAddressMeetingLocations",
                c => new
                    {
                        EmailAddressId = c.Guid(nullable: false),
                        MeetingLocationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.EmailAddressId, t.MeetingLocationId })
                .ForeignKey("dbo.EmailAddresses", t => t.EmailAddressId, cascadeDelete: true)
                .ForeignKey("dbo.MeetingLocations", t => t.MeetingLocationId, cascadeDelete: true)
                .Index(t => t.EmailAddressId)
                .Index(t => t.MeetingLocationId);
            
            CreateTable(
                "dbo.ProfileCaregiverWorkflowItems",
                c => new
                    {
                        WorkflowItemId = c.Guid(nullable: false),
                        ProfileCaregiverId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkflowItemId, t.ProfileCaregiverId })
                .ForeignKey("dbo.WorkflowItem", t => t.WorkflowItemId, cascadeDelete: true)
                .ForeignKey("dbo.ProfileCaregiver", t => t.ProfileCaregiverId, cascadeDelete: true)
                .Index(t => t.WorkflowItemId)
                .Index(t => t.ProfileCaregiverId);
            
            CreateTable(
                "dbo.WorkflowItemProfileMilitaries",
                c => new
                    {
                        WorkflowItem_Id = c.Guid(nullable: false),
                        ProfileMilitary_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkflowItem_Id, t.ProfileMilitary_Id })
                .ForeignKey("dbo.WorkflowItem", t => t.WorkflowItem_Id, cascadeDelete: true)
                .ForeignKey("dbo.ProfileMilitary", t => t.ProfileMilitary_Id, cascadeDelete: true)
                .Index(t => t.WorkflowItem_Id)
                .Index(t => t.ProfileMilitary_Id);
            
            CreateTable(
                "dbo.DistributionLocations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TimeStamp_Id = c.Guid(),
                        DistributionEvent_Id = c.Guid(),
                        Place = c.String(maxLength: 250),
                        EventDetails = c.String(maxLength: 250),
                        LocationDetails = c.String(maxLength: 250),
                        StartingInventory = c.Int(nullable: false),
                        RegistrationRadius = c.Int(nullable: false),
                        StartDateTimeUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        EndDateTimeUtc = c.DateTime(precision: 7, storeType: "datetime2"),
                        RemoveFromWeb = c.Boolean(),
                        AutoConfirm = c.Boolean(),
                        WaitList = c.Boolean(),
                        WaitListMax = c.Int(nullable: false),
                        CloseOnFull = c.Boolean(),
                        LocationClosed = c.Boolean(),
                        EnableTimeSlots = c.Boolean(),
                        TimeSlotMinutes = c.Int(nullable: false),
                        PriorityLevelId = c.Guid(nullable: false),
                        RegAckEmailText = c.String(maxLength: 250),
                        RegWaitListEmailText = c.String(maxLength: 250),
                        RegConfirmEmailText = c.String(maxLength: 250),
                        RegReminderEmailText = c.String(maxLength: 250),
                        Reminder1Date = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder2Date = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder3Date = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder1Sent = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder2Sent = c.DateTime(precision: 7, storeType: "datetime2"),
                        Reminder3Sent = c.DateTime(precision: 7, storeType: "datetime2"),
                        RegistrationOpensUtc = c.DateTime(),
                        RegistrationClosesUtc = c.DateTime(),
                        UmbracoContentId = c.String(maxLength: 250),
                        LocationContact = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeetingLocations", t => t.Id)
                .ForeignKey("dbo.TimeStamps", t => t.TimeStamp_Id)
                .ForeignKey("dbo.DistributionEvents", t => t.DistributionEvent_Id)
                .Index(t => t.Id)
                .Index(t => t.TimeStamp_Id)
                .Index(t => t.DistributionEvent_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DistributionLocations", "DistributionEvent_Id", "dbo.DistributionEvents");
            DropForeignKey("dbo.DistributionLocations", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.DistributionLocations", "Id", "dbo.MeetingLocations");
            DropForeignKey("dbo.WorkflowItemDependency", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.WorkflowItemAssignment", "WorkFlowItemId", "dbo.WorkflowItem");
            DropForeignKey("dbo.WorkflowItemAssignment", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.WorkflowItemAssignment", "ResourceId", "dbo.Persons");
            DropForeignKey("dbo.Tasks", "ParentTask_Id", "dbo.Tasks");
            DropForeignKey("dbo.ProfileMilitaryWorkflowItems", "WorkflowItemId", "dbo.WorkflowItem");
            DropForeignKey("dbo.ProfileMilitaryWorkflowItems", "ProfileMilitaryId", "dbo.ProfileMilitary");
            DropForeignKey("dbo.ProfileMilitaryAttachments", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.ProfileMilitaryAttachments", "ProfileMilitaryId", "dbo.ProfileMilitary");
            DropForeignKey("dbo.ProfileMilitaryAttachments", "AttachmentId", "dbo.Attachments");
            DropForeignKey("dbo.ProfileCaregiverAttachments", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.ProfileCaregiverAttachments", "ProfileCaregiverId", "dbo.ProfileCaregiver");
            DropForeignKey("dbo.ProfileCaregiverAttachments", "AttachmentId", "dbo.Attachments");
            DropForeignKey("dbo.PersonMilitary", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.PersonMilitary", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.PersonMilitaryInjuryInflictions", "PersonMilitary_Id", "dbo.PersonMilitary");
            DropForeignKey("dbo.PersonMilitaryInjuryInflictions", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.PersonMilitaryInjuryInflictions", "ProfileMilitaryId", "dbo.ProfileMilitary");
            DropForeignKey("dbo.WorkflowItem", "WorkFlowId", "dbo.Workflow");
            DropForeignKey("dbo.Workflow", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.WorkflowItem", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.WorkflowItemProfileMilitaries", "ProfileMilitary_Id", "dbo.ProfileMilitary");
            DropForeignKey("dbo.WorkflowItemProfileMilitaries", "WorkflowItem_Id", "dbo.WorkflowItem");
            DropForeignKey("dbo.ProfileCaregiverWorkflowItems", "ProfileCaregiverId", "dbo.ProfileCaregiver");
            DropForeignKey("dbo.ProfileCaregiverWorkflowItems", "WorkflowItemId", "dbo.WorkflowItem");
            DropForeignKey("dbo.ProfileCaregiver", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.ProfileCaregiver", "ProfileId", "dbo.PersonCaregiver");
            DropForeignKey("dbo.ProfileMilitary", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.ProfileMilitary", "MilitaryProfileId", "dbo.PersonMilitary");
            DropForeignKey("dbo.PersonMilitaryInjuryInflictions", "LocationId", "dbo.ListTypes");
            DropForeignKey("dbo.PersonMilitaryInjuryInflictions", "ListInjuryInflictionCategory_Id", "dbo.ListTypes");
            DropForeignKey("dbo.PersonCaregiver", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.PersonCaregiver", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.OHFormValue", "Form_Id", "dbo.OHForm");
            DropForeignKey("dbo.OHFormValue", "Control_Id", "dbo.OHFormControl");
            DropForeignKey("dbo.OHFormControl", "UmbracoProperties_Id", "dbo.OHFormUmbracoProperties");
            DropForeignKey("dbo.OHFormControl", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.OHFormControl", "ServiceMemberProperties_Id", "dbo.OHFormServiceMemberProperties");
            DropForeignKey("dbo.OHFormControl", "SelectListProperties_Id", "dbo.OHFormSelectListProperties");
            DropForeignKey("dbo.OHFormControl", "Section_Id", "dbo.OHFormSection");
            DropForeignKey("dbo.OHFormControl", "RadioButtonsProperties_Id", "dbo.OHFormRadioButtonsProperties");
            DropForeignKey("dbo.OHFormControl", "NumberProperties_Id", "dbo.OHFormNumberProperties");
            DropForeignKey("dbo.OHForm", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.OHForm", "Theme_Id", "dbo.OHFormTheme");
            DropForeignKey("dbo.OHFormTheme", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.OHFormControl", "Form_Id", "dbo.OHForm");
            DropForeignKey("dbo.OHFormControl", "FileUploadProperties_Id", "dbo.OHFormFileUploadProperties");
            DropForeignKey("dbo.OHFormControl", "EmbeddedHtmlProperties_Id", "dbo.OHFormEmbeddedHtmlProperties");
            DropForeignKey("dbo.OHFormControl", "DescriptionProperties_Id", "dbo.OHFormDescriptionProperties");
            DropForeignKey("dbo.OHFormControl", "DateTimeProperties_Id", "dbo.OHFormDateTimeProperties");
            DropForeignKey("dbo.OHFormControl", "CheckBoxProperties_Id", "dbo.OHFormCheckBoxProperties");
            DropForeignKey("dbo.HouseholdMembers", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.HouseholdMembers", "HouseholdId", "dbo.Households");
            DropForeignKey("dbo.DistributionEvents", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.DistributionEventRegistrants", "DistributionEvent_Id", "dbo.DistributionEvents");
            DropForeignKey("dbo.Offices", "DistributionEvent_Id", "dbo.DistributionEvents");
            DropForeignKey("dbo.DistributionEvents", "OwnerId", "dbo.Offices");
            DropForeignKey("dbo.DistributionEvents", "Meeting_Id", "dbo.Meetings");
            DropForeignKey("dbo.CollectionVolunteerMapping", "CollectionVolunteerId", "dbo.CollectionVolunteers");
            DropForeignKey("dbo.CollectionVolunteerMapping", "CollectionEventId", "dbo.CollectionEvents");
            DropForeignKey("dbo.CollectionVolunteers", "CollectionEvent_Id", "dbo.CollectionEvents");
            DropForeignKey("dbo.CollectionEvents", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.Offices", "CollectionEvent_Id", "dbo.CollectionEvents");
            DropForeignKey("dbo.CollectionEvents", "OwnerId", "dbo.Offices");
            DropForeignKey("dbo.CollectionEvents", "Meeting_Id", "dbo.Meetings");
            DropForeignKey("dbo.Meetings", "ParentMeeting_Id", "dbo.Meetings");
            DropForeignKey("dbo.Meetings", "MeetingLocationId", "dbo.MeetingLocations");
            DropForeignKey("dbo.MeetingAttendees", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.MeetingAttendees", "MeetingId", "dbo.Meetings");
            DropForeignKey("dbo.CollectionLocations", "CollectionEvent_Id", "dbo.CollectionEvents");
            DropForeignKey("dbo.CollectionVolunteers", "CollectionLocation_Id", "dbo.CollectionLocations");
            DropForeignKey("dbo.CollectionVolunteers", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.CollectionLocations", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.CollectionLocations", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.CategoryItems", "ParentCategoryItem_Id", "dbo.CategoryItems");
            DropForeignKey("dbo.ApplicationClasses", "ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.Categories", "ParentCategoryId", "dbo.Categories");
            DropForeignKey("dbo.Categories", "Owner_Id", "dbo.Persons");
            DropForeignKey("dbo.Persons", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.Persons", "PrimaryEmailId", "dbo.EmailAddresses");
            DropForeignKey("dbo.Persons", "PrimaryAddressId", "dbo.Addresses");
            DropForeignKey("dbo.PersonPhoneMapping", "PhoneId", "dbo.Phones");
            DropForeignKey("dbo.PersonPhoneMapping", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.PersonEmailAddresses", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.PersonEmailAddresses", "EmailAddressId", "dbo.EmailAddresses");
            DropForeignKey("dbo.EmailAddressMeetingLocations", "MeetingLocationId", "dbo.MeetingLocations");
            DropForeignKey("dbo.EmailAddressMeetingLocations", "EmailAddressId", "dbo.EmailAddresses");
            DropForeignKey("dbo.DistributionEventRegistrants", "DistributionLocation_Id", "dbo.DistributionLocations");
            DropForeignKey("dbo.DistributionEventRegistrants", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.DistributionEventAttendees", "DistributionEventRegistrant_Id", "dbo.DistributionEventRegistrants");
            DropForeignKey("dbo.DistributionEventAttendees", "Attendee_Id", "dbo.Persons");
            DropForeignKey("dbo.MeetingRooms", "MeetingLocationId", "dbo.MeetingLocations");
            DropForeignKey("dbo.Persons", "PrimaryPhoneId", "dbo.Phones");
            DropForeignKey("dbo.PhoneMeetingLocations", "MeetingLocation_Id", "dbo.MeetingLocations");
            DropForeignKey("dbo.PhoneMeetingLocations", "Phone_Id", "dbo.Phones");
            DropForeignKey("dbo.Persons", "MeetingLocation_Id", "dbo.MeetingLocations");
            DropForeignKey("dbo.MeetingLocations", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.PersonAddressMapping", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.PersonAddressMapping", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.CategoryAttachmentMapping", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.CategoryAttachmentMapping", "AttachmentId", "dbo.Attachments");
            DropForeignKey("dbo.ChangeLogs", "TimeStamp_Id", "dbo.TimeStamps");
            DropForeignKey("dbo.Categories", "ChangeLog_Id", "dbo.ChangeLogs");
            DropForeignKey("dbo.Attachments", "ChangeLog_Id", "dbo.ChangeLogs");
            DropForeignKey("dbo.CategoryApplications", "Application_Id", "dbo.Applications");
            DropForeignKey("dbo.CategoryApplications", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.Addresses", "TimeStamp_Id", "dbo.TimeStamps");
            DropIndex("dbo.DistributionLocations", new[] { "DistributionEvent_Id" });
            DropIndex("dbo.DistributionLocations", new[] { "TimeStamp_Id" });
            DropIndex("dbo.DistributionLocations", new[] { "Id" });
            DropIndex("dbo.WorkflowItemProfileMilitaries", new[] { "ProfileMilitary_Id" });
            DropIndex("dbo.WorkflowItemProfileMilitaries", new[] { "WorkflowItem_Id" });
            DropIndex("dbo.ProfileCaregiverWorkflowItems", new[] { "ProfileCaregiverId" });
            DropIndex("dbo.ProfileCaregiverWorkflowItems", new[] { "WorkflowItemId" });
            DropIndex("dbo.EmailAddressMeetingLocations", new[] { "MeetingLocationId" });
            DropIndex("dbo.EmailAddressMeetingLocations", new[] { "EmailAddressId" });
            DropIndex("dbo.PhoneMeetingLocations", new[] { "MeetingLocation_Id" });
            DropIndex("dbo.PhoneMeetingLocations", new[] { "Phone_Id" });
            DropIndex("dbo.CategoryApplications", new[] { "Application_Id" });
            DropIndex("dbo.CategoryApplications", new[] { "Category_Id" });
            DropIndex("dbo.WorkflowItemDependency", new[] { "TimeStamp_Id" });
            DropIndex("dbo.WorkflowItemAssignment", new[] { "TimeStamp_Id" });
            DropIndex("dbo.WorkflowItemAssignment", new[] { "WorkFlowItemId" });
            DropIndex("dbo.WorkflowItemAssignment", new[] { "ResourceId" });
            DropIndex("dbo.Tasks", new[] { "ParentTask_Id" });
            DropIndex("dbo.ProfileMilitaryWorkflowItems", new[] { "ProfileMilitaryId" });
            DropIndex("dbo.ProfileMilitaryWorkflowItems", new[] { "WorkflowItemId" });
            DropIndex("dbo.ProfileMilitaryAttachments", new[] { "TimeStamp_Id" });
            DropIndex("dbo.ProfileMilitaryAttachments", new[] { "AttachmentId" });
            DropIndex("dbo.ProfileMilitaryAttachments", new[] { "ProfileMilitaryId" });
            DropIndex("dbo.ProfileCaregiverAttachments", new[] { "TimeStamp_Id" });
            DropIndex("dbo.ProfileCaregiverAttachments", new[] { "ProfileCaregiverId" });
            DropIndex("dbo.ProfileCaregiverAttachments", new[] { "AttachmentId" });
            DropIndex("dbo.Workflow", new[] { "TimeStamp_Id" });
            DropIndex("dbo.ProfileCaregiver", new[] { "TimeStamp_Id" });
            DropIndex("dbo.ProfileCaregiver", new[] { "ProfileId" });
            DropIndex("dbo.WorkflowItem", new[] { "TimeStamp_Id" });
            DropIndex("dbo.WorkflowItem", new[] { "WorkFlowId" });
            DropIndex("dbo.ProfileMilitary", new[] { "TimeStamp_Id" });
            DropIndex("dbo.ProfileMilitary", new[] { "MilitaryProfileId" });
            DropIndex("dbo.PersonMilitaryInjuryInflictions", new[] { "PersonMilitary_Id" });
            DropIndex("dbo.PersonMilitaryInjuryInflictions", new[] { "TimeStamp_Id" });
            DropIndex("dbo.PersonMilitaryInjuryInflictions", new[] { "ListInjuryInflictionCategory_Id" });
            DropIndex("dbo.PersonMilitaryInjuryInflictions", new[] { "ProfileMilitaryId" });
            DropIndex("dbo.PersonMilitaryInjuryInflictions", new[] { "LocationId" });
            DropIndex("dbo.PersonMilitary", new[] { "TimeStamp_Id" });
            DropIndex("dbo.PersonMilitary", new[] { "PersonId" });
            DropIndex("dbo.PersonCaregiver", new[] { "TimeStamp_Id" });
            DropIndex("dbo.PersonCaregiver", new[] { "PersonId" });
            DropIndex("dbo.OHFormValue", new[] { "Form_Id" });
            DropIndex("dbo.OHFormValue", new[] { "Control_Id" });
            DropIndex("dbo.OHFormTheme", new[] { "TimeStamp_Id" });
            DropIndex("dbo.OHForm", new[] { "TimeStamp_Id" });
            DropIndex("dbo.OHForm", new[] { "Theme_Id" });
            DropIndex("dbo.OHFormControl", new[] { "UmbracoProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "TimeStamp_Id" });
            DropIndex("dbo.OHFormControl", new[] { "ServiceMemberProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "SelectListProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "Section_Id" });
            DropIndex("dbo.OHFormControl", new[] { "RadioButtonsProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "NumberProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "Form_Id" });
            DropIndex("dbo.OHFormControl", new[] { "FileUploadProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "EmbeddedHtmlProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "DescriptionProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "DateTimeProperties_Id" });
            DropIndex("dbo.OHFormControl", new[] { "CheckBoxProperties_Id" });
            DropIndex("dbo.HouseholdMembers", new[] { "PersonId" });
            DropIndex("dbo.HouseholdMembers", new[] { "HouseholdId" });
            DropIndex("dbo.DistributionEvents", new[] { "TimeStamp_Id" });
            DropIndex("dbo.DistributionEvents", new[] { "Meeting_Id" });
            DropIndex("dbo.DistributionEvents", new[] { "OwnerId" });
            DropIndex("dbo.CollectionVolunteerMapping", new[] { "CollectionVolunteerId" });
            DropIndex("dbo.CollectionVolunteerMapping", new[] { "CollectionEventId" });
            DropIndex("dbo.Offices", new[] { "DistributionEvent_Id" });
            DropIndex("dbo.Offices", new[] { "CollectionEvent_Id" });
            DropIndex("dbo.MeetingAttendees", new[] { "PersonId" });
            DropIndex("dbo.MeetingAttendees", new[] { "MeetingId" });
            DropIndex("dbo.Meetings", new[] { "ParentMeeting_Id" });
            DropIndex("dbo.Meetings", new[] { "MeetingLocationId" });
            DropIndex("dbo.CollectionVolunteers", new[] { "CollectionEvent_Id" });
            DropIndex("dbo.CollectionVolunteers", new[] { "CollectionLocation_Id" });
            DropIndex("dbo.CollectionVolunteers", new[] { "PersonId" });
            DropIndex("dbo.CollectionLocations", new[] { "CollectionEvent_Id" });
            DropIndex("dbo.CollectionLocations", new[] { "TimeStamp_Id" });
            DropIndex("dbo.CollectionLocations", new[] { "AddressId" });
            DropIndex("dbo.CollectionEvents", new[] { "TimeStamp_Id" });
            DropIndex("dbo.CollectionEvents", new[] { "Meeting_Id" });
            DropIndex("dbo.CollectionEvents", new[] { "OwnerId" });
            DropIndex("dbo.CategoryItems", new[] { "ParentCategoryItem_Id" });
            DropIndex("dbo.PersonPhoneMapping", new[] { "PersonId" });
            DropIndex("dbo.PersonPhoneMapping", new[] { "PhoneId" });
            DropIndex("dbo.DistributionEventAttendees", new[] { "DistributionEventRegistrant_Id" });
            DropIndex("dbo.DistributionEventAttendees", new[] { "Attendee_Id" });
            DropIndex("dbo.DistributionEventRegistrants", new[] { "DistributionEvent_Id" });
            DropIndex("dbo.DistributionEventRegistrants", new[] { "DistributionLocation_Id" });
            DropIndex("dbo.DistributionEventRegistrants", new[] { "PersonId" });
            DropIndex("dbo.MeetingRooms", new[] { "MeetingLocationId" });
            DropIndex("dbo.MeetingLocations", new[] { "AddressId" });
            DropIndex("dbo.PersonEmailAddresses", new[] { "PersonId" });
            DropIndex("dbo.PersonEmailAddresses", new[] { "EmailAddressId" });
            DropIndex("dbo.PersonAddressMapping", new[] { "PersonId" });
            DropIndex("dbo.PersonAddressMapping", new[] { "AddressId" });
            DropIndex("dbo.Persons", new[] { "TimeStamp_Id" });
            DropIndex("dbo.Persons", new[] { "MeetingLocation_Id" });
            DropIndex("dbo.Persons", new[] { "PrimaryPhoneId" });
            DropIndex("dbo.Persons", new[] { "PrimaryAddressId" });
            DropIndex("dbo.Persons", new[] { "PrimaryEmailId" });
            DropIndex("dbo.ChangeLogs", new[] { "TimeStamp_Id" });
            DropIndex("dbo.Attachments", new[] { "ChangeLog_Id" });
            DropIndex("dbo.CategoryAttachmentMapping", new[] { "AttachmentId" });
            DropIndex("dbo.CategoryAttachmentMapping", new[] { "CategoryId" });
            DropIndex("dbo.Categories", new[] { "Owner_Id" });
            DropIndex("dbo.Categories", new[] { "ChangeLog_Id" });
            DropIndex("dbo.Categories", new[] { "ParentCategoryId" });
            DropIndex("dbo.ApplicationClasses", new[] { "ApplicationId" });
            DropIndex("dbo.Addresses", new[] { "TimeStamp_Id" });
            DropTable("dbo.DistributionLocations");
            DropTable("dbo.WorkflowItemProfileMilitaries");
            DropTable("dbo.ProfileCaregiverWorkflowItems");
            DropTable("dbo.EmailAddressMeetingLocations");
            DropTable("dbo.PhoneMeetingLocations");
            DropTable("dbo.CategoryApplications");
            DropTable("dbo.WorkflowItemDependency");
            DropTable("dbo.WorkflowItemAssignment");
            DropTable("dbo.Tasks");
            DropTable("dbo.Reports");
            DropTable("dbo.ProfileMilitaryWorkflowItems");
            DropTable("dbo.ProfileMilitaryAttachments");
            DropTable("dbo.ProfileCaregiverAttachments");
            DropTable("dbo.Workflow");
            DropTable("dbo.ProfileCaregiver");
            DropTable("dbo.WorkflowItem");
            DropTable("dbo.ProfileMilitary");
            DropTable("dbo.PersonMilitaryInjuryInflictions");
            DropTable("dbo.PersonMilitary");
            DropTable("dbo.PersonCaregiver");
            DropTable("dbo.OHFormValue");
            DropTable("dbo.OHFormUmbracoProperties");
            DropTable("dbo.OHFormServiceMemberProperties");
            DropTable("dbo.OHFormSelectListProperties");
            DropTable("dbo.OHFormSection");
            DropTable("dbo.OHFormRadioButtonsProperties");
            DropTable("dbo.OHFormNumberProperties");
            DropTable("dbo.OHFormTheme");
            DropTable("dbo.OHForm");
            DropTable("dbo.OHFormFileUploadProperties");
            DropTable("dbo.OHFormEmbeddedHtmlProperties");
            DropTable("dbo.OHFormDescriptionProperties");
            DropTable("dbo.OHFormDateTimeProperties");
            DropTable("dbo.OHFormCheckBoxProperties");
            DropTable("dbo.OHFormControl");
            DropTable("dbo.ListTypes");
            DropTable("dbo.Households");
            DropTable("dbo.HouseholdMembers");
            DropTable("dbo.DistributionEvents");
            DropTable("dbo.CollectionVolunteerMapping");
            DropTable("dbo.Offices");
            DropTable("dbo.MeetingAttendees");
            DropTable("dbo.Meetings");
            DropTable("dbo.CollectionVolunteers");
            DropTable("dbo.CollectionLocations");
            DropTable("dbo.CollectionEvents");
            DropTable("dbo.CategoryItems");
            DropTable("dbo.PersonPhoneMapping");
            DropTable("dbo.DistributionEventAttendees");
            DropTable("dbo.DistributionEventRegistrants");
            DropTable("dbo.MeetingRooms");
            DropTable("dbo.Phones");
            DropTable("dbo.MeetingLocations");
            DropTable("dbo.EmailAddresses");
            DropTable("dbo.PersonEmailAddresses");
            DropTable("dbo.PersonAddressMapping");
            DropTable("dbo.Persons");
            DropTable("dbo.ChangeLogs");
            DropTable("dbo.Attachments");
            DropTable("dbo.CategoryAttachmentMapping");
            DropTable("dbo.Categories");
            DropTable("dbo.Applications");
            DropTable("dbo.ApplicationClasses");
            DropTable("dbo.TimeStamps");
            DropTable("dbo.Addresses");
        }
    }
}
