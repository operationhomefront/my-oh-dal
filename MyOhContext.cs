﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Applications;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.Authorization;
using OH.DAL.Domain.Categories;
using OH.DAL.Domain.ChangeLogs;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Events;
using OH.DAL.Domain.Forms;
using OH.DAL.Domain.Households;
using OH.DAL.Domain.ListTypes;
using OH.DAL.Domain.ListTypes.Injury;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.Meetings;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.Phones;
using OH.DAL.Domain.ProgramProfile;
using OH.DAL.Domain.ProgramProfile.Caregiver;
using OH.DAL.Domain.ProgramProfile.Injury;
using OH.DAL.Domain.ProgramProfile.Military;
using OH.DAL.Domain.Reports;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Domain.Tasks;
using OH.DAL.Domain.WorkflowTemplates;

namespace OH.DAL
{
    public class MyOhContext : DbContext
    {
        public MyOhContext() : base("MyOHContext")
        {
            Database.SetInitializer(new MyOhDevDatabaseInitializer());
            //Database.Initialize(true);            

            //Add all dbsets here to access via ClassGuid
            //AllEntities = new List<object>();
            //AllEntities.Add(Attachments.ToList());
            //AllEntities.Add(Categories.ToList());
        }


        //public IList<object> AllEntities { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<ApplicationClass> ApplicationClasses { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryItem> CategoryItems { get; set; }
        public DbSet<CollectionLocation> CollectionLocations { get; set; }
        public DbSet<CollectionEvent> CollectionEvents { get; set; }
        public DbSet<CollectionVolunteer> CollectionVolunteers { get; set; }
        public DbSet<CollectionVolunteerMapping> CollectionVolunteerMapping { get; set; }

        //JAKE -> ADDING DISTRIBUTION
        public DbSet<DistributionEvent> DistributionEvents { get; set; }
        public DbSet<DistributionLocation> DistributionLocations { get; set; }
        public DbSet<DistributionEventRegistrant> DistributionEventRegistrants { get; set; }
        public DbSet<DistributionEventAttendee> DistributionEventAttendees { get; set; }
        //JAKE <- ADDING DISTRIBUTION

        public DbSet<CategoryAttachment> CategoryAttachments { get; set; }
        //public DbSet<CategoryListType> CategoryListTypes { get; set; }
        public DbSet<ChangeLog> ChangeLogs { get; set; }
        public DbSet<EmailAddress> EmailAddresses { get; set; }
        public DbSet<Household> Households { get; set; }
        public DbSet<HouseholdMember> HouseholdMembers { get; set; }
        public DbSet<ListType> ListTypes { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<MeetingAttendee> MeetingAttendees { get; set; }
        public DbSet<MeetingLocation> MeetingLocations { get; set; }
        public DbSet<MeetingRoom> MeetingRooms { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PersonAddress> PersonAddresses { get; set; }
        public DbSet<PersonCaregiver> PersonCaregiver { get; set; }
        public DbSet<PersonEmailAddress> PersonEmailAddresses { get; set; }
        public DbSet<PersonMilitary> PersonMilitary { get; set; }
        public DbSet<PersonPhone> PersonPhones { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TimeStamp> TimeStamp { get; set; }

        //OHForm
        public DbSet<OHForm> OHForms { get; set; }
        public DbSet<OHFormControl> OHFormControls { get; set; }
        public DbSet<OHFormEmbeddedHtmlProperties> OHFormEmbeddedHtmlProperties { get; set; }
        public DbSet<OHFormFileUploadProperties> OHFormFileUploadProperties { get; set; }
        public DbSet<OHFormTheme> OHFormThemes { get; set; }
        public DbSet<OHFormNumberProperties> OHFormNumberProperties { get; set; }
        public DbSet<OHFormRadioButtonsProperties> OHFormRadioButtonsProperties { get; set; }
        public DbSet<OHFormSection> OHFormSections { get; set; }
        public DbSet<OHFormValue> OHFormValues { get; set; }


        //ProgramProfiles
        public DbSet<InjuryInfliction> ProfileInjuryInflictions { get; set; }
        public DbSet<ProfileCaregiver> ProfileCaregiver { get; set; }
        
        public DbSet<ProfileCaregiverAttachment> ProfileCaregiverAttachments { get; set; }
        public DbSet<ProfileMilitary> ProfileMilitary { get; set; }
        public DbSet<ProfileMilitaryWorkflowItems> ProfileMilitaryWorkflowItems { get; set; }

        public DbSet<ProfileMilitaryAttachment> ProfileMilitaryAttachments { get; set; }


        //Security 
        //public DbSet<UserAccount> OHUsers { get; set; }
        //public DbSet<UserRole> OHRoles { get; set; }
        //public DbSet<UserGroup> OHGroups { get; set; }
        //public DbSet<UserMembership> UserMembership { get; set; }
        //public DbSet<UserOAuthMembership> UserOAuthMembership { get; set; }
        
        //Workflow
        public DbSet<WorkflowItemDependency> WorkflowItemDependency { get; set; }
        public DbSet<WorkflowItemAssignment> WorkflowItemAssignment { get; set; }
        public DbSet<WorkflowItem> WorkflowItem { get; set; }
        public DbSet<Workflow> Workflow { get; set; }

        public DbSet<Report> Reports { get; set; }
        
        //MyOHAuthorization
        //public DbSet<MyOhUser> MyOHUsers { get; set; }
        //public DbSet<MyOhRole> MyOHRoles { get; set; }
        //public DbSet<MyOhoAuthMembership> MyOhoAuthMemberships { get; set; }        
        //public DbSet<MyOhUserMembership> MyOHUserMemberships { get; set; }

        //public DbSet<MyOhSecurityGroup> MyOHSecurityGroups { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Set ClusterId as Primary Key for all tables
            modelBuilder.Properties().Where(p => p.Name == "Id").Configure(p => p.IsKey());
            modelBuilder.Entity<PersonCaregiver>().Property(e => e.ClusterId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           
            modelBuilder.Entity<EmailAddress>().HasMany(c => c.MeetingLocations).WithMany(p => p.LocationEmailAddresses).Map(
               m =>
               {
                   m.MapLeftKey("EmailAddressId");
                   m.MapRightKey("MeetingLocationId");
                  
               });

            modelBuilder.Entity<WorkflowItem>().HasMany(c => c.ProfilesCaregiver).WithMany(p => p.WorkFlowItems).Map(
               m =>
               {
                   m.MapLeftKey("WorkflowItemId");
                   m.MapRightKey("ProfileCaregiverId");
                   m.ToTable("ProfileCaregiverWorkflowItems");
               });
            
            
            //modelBuilder.Entity<ProfileMilitary>().HasMany(c => c.WorkFlowItems).WithMany(p => p.ProfilesMilitary).Map(
            //  m =>
            //  {
            //      m.MapLeftKey("WorkflowItemId");
            //      m.MapRightKey("ProfileMilitaryId");
            //  });

            var profileMilitary = modelBuilder.Entity<PersonMilitary>().Property(t => t.PersonId).IsRequired();

            var caregiver = modelBuilder.Entity<PersonCaregiver>().Property(t => t.PersonId).IsRequired();

            var category = modelBuilder.Entity<Category>();
            category.HasOptional(x => x.ParentCategory);
            
            //var user = modelBuilder.Entity<UserAccount>();
            //user.Property(u => u.Id).HasColumnName("UserId");

            //var passwordLogin = modelBuilder.Entity<UserMembership>()
            //    .HasKey(l => l.Id);
                

            //passwordLogin.HasRequired(l => l.User).WithOptional(u => u.PasswordLogin);
            //user.HasMany(u => u.Roles).WithMany(r => r.Users).Map((config) =>
            //{
            //    config
            //        .ToTable("UserInRoles")
            //        .MapLeftKey("UserId")
            //        .MapRightKey("RoleId");
            //});

            //var externalLogin = modelBuilder.Entity<UserOAuthMembership>()
            //    .HasKey(l => new { l.LoginProvider, l.ProviderKey });
            //externalLogin.Property(l => l.LoginProvider).HasColumnName("Provider");
            //externalLogin.Property(l => l.ProviderKey).HasColumnName("ProviderUserId");
            //externalLogin.HasRequired(l => l.User).WithMany(u => u.ExternalLogins);


            //var role = modelBuilder.Entity<UserRole>();
            //role.Property(r => r.Id).HasColumnName("RoleId");
            //role.Property(r => r.Name).HasColumnName("RoleName");

            //role.HasRequired(r => r.Group);
            //var roleGroup = modelBuilder.Entity<UserGroup>();
            //roleGroup.Property(g => g.Id).HasColumnName("GroupId");
            //roleGroup.Property(g => g.Name).HasColumnName("GroupName");


            //Set Default String Size
            modelBuilder.Properties<String>().Configure(p => p.HasMaxLength(250));
            //For Indexing on an integer
            //TODO: Fix this it will with indexing, see if there is a way to set the ClusterId as the Cluster Index Key
            //modelBuilder.Properties().Where(p => p.Name == "ClusterId").Configure(a=>a.HasColumnAnnotation(
            //        IndexAnnotation.AnnotationName,
            //        new IndexAnnotation(
            //            new IndexAttribute(String.Format("IX_{0}_ClusterId",a.ClrPropertyInfo.Name), 1) { IsUnique = true })));

            //TODO: Review with Roy, This can be used if we want to have unique category names, I dont think this is ideal at the moment
            /*//Make Category Names Unique
            modelBuilder.Entity<Category>()
                .Property(t => t.Name)
                .IsRequired()
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Category_Name", 1) {IsUnique = true}));
*/
            //modelBuilder.Entity<Attachment>().HasRequired<ChangeLog>(p => p.ChangeLog).WithMany().WillCascadeOnDelete(false);
            //modelBuilder.Entity<Attachment>().HasRequired<Category>(p => p.Category).WithMany().WillCascadeOnDelete(false);
            //modelBuilder.Entity<Category>().HasRequired<ChangeLog>(p => p.ChangeLog).WithMany().WillCascadeOnDelete(false);
            base.OnModelCreating(modelBuilder);
        }

        public class MyOhDevDatabaseInitializer : DropCreateDatabaseIfModelChanges<MyOhContext>
        {
            public MyOhDevDatabaseInitializer() : base()
            {
            }

            protected override void Seed(MyOhContext context)
            {
                base.Seed(context);
            }


            public override void InitializeDatabase(MyOhContext context)
            {
                //TODO: this did not work, find out why
                //This will not prevent the script from running when there are open connections
                //context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                //    "ALTER DATABASE [" + context.Database.Connection.Database +
                //    "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
                base.InitializeDatabase(context);
                //if (context.Database.Exists())
                //{  
                //    //This will prevent the script from running when there are open connections
                //    context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "ALTER DATABASE [" + context.Database.Connection.Database + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");

                //    context.Database.Delete();
                //}

                //context.Database.Create();

                //TODO: Automate this in the future

                //context.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Categories] ADD  CONSTRAINT [UK_Categories_Id] UNIQUE NONCLUSTERED ( 	[Id] ASC ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]");


                //context.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Attachments] ADD  CONSTRAINT [UK_Attachments_Id] UNIQUE NONCLUSTERED ( [Id] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]");
                //context.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Attachments]  WITH CHECK ADD  CONSTRAINT [FK_Attachments_Categories] FOREIGN KEY([CategoryId]) REFERENCES [dbo].[Categories] ([Id]); ALTER TABLE [dbo].[Attachments] CHECK CONSTRAINT [FK_Attachments_Categories]");
            }
        }

        //public IEnumerable<object> GetAllEntitiesByGuid(object guid)
        //{
        //    //Type type = typeof(MyOHContext);

        //    //var propertyInfoArray = type.GetProperties().Where(t => t.PropertyType.Name.Contains("DbSet"));

        //    //foreach (var propertyInfo in propertyInfoArray)
        //    //{
        //    //    var name = propertyInfo.Name;
        //    //    name = name;
        //    //}

        //    this.Attachments.Where(t=>t.)


        //    return null;
        //}
    }
}