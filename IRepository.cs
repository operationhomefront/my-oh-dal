﻿using System;
using System.Collections.Generic;

namespace OH.DAL
{
    public interface IRepository<T> : IDisposable where T : class
    {
        T GetById(Guid id);
        IEnumerable<T> GetAll();
        //IEnumerable<object> GetAllEntitiesByGuid(object guid);
        Guid AddOrUpdate(T entity);
        void Delete(Guid id);
        void Save();
        
        Guid GetLastInsertedId();

    }
}
