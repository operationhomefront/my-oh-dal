﻿using OH.DAL.Domain.Addresses;

namespace OH.DAL.Repositories.Addresses
{
    public interface IAddressRepository : IRepository<Address>
    {
        void UpdateAddress(Address address);
    }
}
