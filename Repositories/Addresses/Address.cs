﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using OH.DAL.Domain.Addresses;

namespace OH.DAL.Repositories.Addresses
{
    public class AddressRepository : Repository<Address>, IAddressRepository
    {
        public AddressRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public void UpdateAddress(Address address)
        {
            TextInfo properCase = new CultureInfo("en-US", false).TextInfo;
            using (MyOhContext db = new MyOhContext())
            {
                bool saveFailed;
                do
                {
                    saveFailed = false;
                    try
                    {
                        //Address old = new Address {Id = address.Id};
                        var old = db.Addresses.Find(address.Id);

                        if (address.City != null)
                            address.City = properCase.ToTitleCase(address.City);
                        if (address.Address1 != null)
                            address.Address1 = properCase.ToTitleCase(address.Address1);
                        if (address.Address2 != null)
                            address.Address2 = properCase.ToTitleCase(address.Address2);

                        db.Entry(old).CurrentValues.SetValues(address);
                        db.Entry(old).Property(u => u.ClusterId).CurrentValue =
                            db.Entry(old).Property(u => u.ClusterId).OriginalValue;

                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update the values of the entity that failed to save from the store 
                        ex.Entries.Single().Reload();
                    }

                    catch (Exception er)
                    {
                        var s = er.Message;
                        throw;
                    }

                } while (saveFailed);

            }
        }
    }
}