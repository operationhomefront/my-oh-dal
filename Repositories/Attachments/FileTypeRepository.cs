﻿using System.Linq;
using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Attachments
{
    public class FileTypeRepository : Repository<FileType>, IFileTypeRepository
    {
        public FileTypeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {

        }
        
        public string GetFileTypeCssByExtension(string extension)
        {
            var fileType = DbSet.FirstOrDefault(ext => ext.Extension.Trim().ToLower().Equals(extension.Trim().ToLower()));

            return fileType != null ? fileType.CssClass : "";
        }
    }
}
