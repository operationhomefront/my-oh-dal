﻿using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Attachments
{
    public interface IFileTypeRepository : IRepository<FileType>
    {
        string GetFileTypeCssByExtension(string extension);

    }
}
