﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using OH.DAL.Domain.Attachments;

namespace OH.DAL.Repositories.Attachments
{
    public class AttachmentRepository: Repository<Attachment>, IAttachmentRepository
    {
        public AttachmentRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {            
        }


        public Attachment GetByOwnerAndFile(Guid ownerId, string fileName)
        {
            return DbSet.SingleOrDefault(m => m.OwnerId == ownerId && m.FileName == fileName);
        }


        public Collection<Guid> GetByOwner(Guid ownerId)
        {
            var val = DbSet.Where(m => m.OwnerId == ownerId).Select(r => new { r.Id });
            Collection<Guid> t= new Collection<Guid>();
            foreach (var v in val)
            {
                t.Add(v.Id);
            }
            
            return t;
        }
    }
}
