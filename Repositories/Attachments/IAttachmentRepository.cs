﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OH.DAL.Domain.Attachments;

namespace OH.DAL.Repositories.Attachments
{
    public interface IAttachmentRepository: IRepository<Attachment>
    {
        Attachment GetByOwnerAndFile(Guid ownerId, string fileName);
        Collection<Guid> GetByOwner(Guid ownerId);
    }
}
