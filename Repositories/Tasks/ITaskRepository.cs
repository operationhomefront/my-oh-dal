﻿using OH.DAL.Domain.Tasks;

namespace OH.DAL.Repositories.Tasks
{
    public interface ITaskRepository : IRepository<Task>
    {

    }
}
