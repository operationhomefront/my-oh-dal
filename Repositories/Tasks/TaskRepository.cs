﻿using OH.DAL.Domain.Tasks;

namespace OH.DAL.Repositories.Tasks
{
    public class TaskRepository : Repository<Task>, ITaskRepository
    {
        public TaskRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}