﻿using System;
using System.Collections.Generic;
using System.Linq;
using OH.DAL.Domain.Households;

namespace OH.DAL.Repositories.Households
{
    public class HouseholdRepository : Repository<Household>, IHouseholdRepository
    {
        public HouseholdRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public IEnumerable<Household> GetByMemberId(System.Guid id)
        {
            //set to enumerable in case there is more than one
            return DbSet.Where(x => x.HouseholdMembers.Any(m => m.Person.Id == id));
        }

        public void UpdateHousehold(Household model)
        {
            using (var db = new MyOhContext())
            {
                try
                {
                    model.HouseholdMembers = null;
                    var old = new Household { Id = model.Id };
                    db.Households.Attach(old);
                    db.Entry(old).CurrentValues.SetValues(model);
                    db.Entry(old).Property(u => u.ClusterId).CurrentValue = db.Entry(old).Property(u => u.ClusterId).OriginalValue;
                    db.SaveChanges();
                }
                catch (Exception er)
                {
                    var s = er.Message;
                    throw;
                }
            }
        }
        public void AddHousehold(Household model)
        {
            model.HouseholdMembers = null;
            DbSet.Add(model);
            Save();
        }

        
    }
}