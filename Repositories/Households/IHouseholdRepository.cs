﻿using System;
using System.Collections.Generic;
using OH.DAL.Domain.Households;

namespace OH.DAL.Repositories.Households
{
    public interface IHouseholdRepository : IRepository<Household>
    {
        IEnumerable<Household> GetByMemberId(Guid id);
        void UpdateHousehold(Household model);
        void AddHousehold(Household model);
    }
}
