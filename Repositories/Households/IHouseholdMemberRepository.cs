﻿using System;
using OH.DAL.Domain.Households;

namespace OH.DAL.Repositories.Households
{
    public interface IHouseholdMemberRepository : IRepository<HouseholdMember>
    {
        void RemoveMember(Guid id);
        void AddMember(HouseholdMember member);
        void UpdateMember(HouseholdMember member);
        bool HouseholdMemberExists(Guid id,Guid householdId);
    }
}
