﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using OH.DAL.Domain.Households;

namespace OH.DAL.Repositories.Households
{
    public class HouseholdMemberRepository : Repository<HouseholdMember>, IHouseholdMemberRepository
    {
        public HouseholdMemberRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public void RemoveMember(System.Guid id)
        {
            var old = DbSet.Find(id);
            if (old != null)
                DbSet.Remove(old);
        }

        public void AddMember(HouseholdMember member)
        {
            //double check exists
            member.Person = null;
            DbSet.AddOrUpdate(member);
        }

        public void UpdateMember(HouseholdMember member)
        {

            using (var db = new MyOhContext())
            {
                try
                {

                    member.Person = null;
                    var p = new HouseholdMember() {Id = member.Id};
                    db.HouseholdMembers.Attach(p);
                    db.Entry(p).CurrentValues.SetValues(p);
                    db.Entry(p).Property(u => u.ClusterId).CurrentValue =
                        db.Entry(p).Property(u => u.ClusterId).OriginalValue;

                    db.SaveChanges();
                }
                catch (Exception er)
                {
                    var s = er.Message;
                    throw;
                }
            }
        }


        public bool HouseholdMemberExists(Guid id, Guid householdId)
        {

            var member = DbSet.FirstOrDefault(x => x.PersonId == id && x.HouseholdId == householdId);
            if (member != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}