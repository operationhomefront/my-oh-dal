﻿using System;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public interface IPersonEmailAddressRepository : IRepository<PersonEmailAddress>
    {

        PersonEmailAddress GetByIdInclude(Guid id);
        PersonEmailAddress GetByPersonId(Guid personId);
    }
}
