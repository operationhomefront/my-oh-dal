﻿using System;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public class PersonEmailAddressRepository : Repository<PersonEmailAddress>, IPersonEmailAddressRepository
    {
        public PersonEmailAddressRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public PersonEmailAddress GetByIdInclude(Guid id)
        {
            return DbSet.FirstOrDefault(i => i.Id == id);
                //.Include(p => p.Person).Include(e => e.EmailAddress)
                
        }
        public PersonEmailAddress GetByPersonId(Guid personId)
        {
            return DbSet.Include(e => e.EmailAddress).FirstOrDefault(i => i.PersonId == personId);
        }
    }
}