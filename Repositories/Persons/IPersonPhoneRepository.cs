﻿using System;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public interface IPersonPhoneRepository : IRepository<PersonPhone>
    {
        PersonPhone GetByIdInclude(System.Guid id);
        PersonPhone GetByPersonId(Guid personId);
    }
}
