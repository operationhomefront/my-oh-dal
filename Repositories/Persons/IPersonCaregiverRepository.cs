﻿using System;
using System.Data.Entity.Migrations.Model;
using OH.DAL.Domain;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public interface IPersonCaregiverRepository : IRepository<PersonCaregiver>
    {
        void UpdateCaregiver(PersonCaregiver careGiver);
        void AddUpdateCaregiver(PersonCaregiver careGiver);
        PersonCaregiver GetMy(Guid id);
        PersonCaregiver GetNoTracking(Guid id);
        PersonCaregiver GetNoTrackingByPerson(Guid id);
    }
}
