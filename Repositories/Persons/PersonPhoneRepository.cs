﻿using System;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public class PersonPhoneRepository : Repository<PersonPhone>, IPersonPhoneRepository
    {
        public PersonPhoneRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public PersonPhone GetByIdInclude(Guid id)
        {
            return DbSet.Where(i => i.Id.Equals(id)).Include(p => p.Person).Include(p => p.Phone).FirstOrDefault();
        }


        public PersonPhone GetByPersonId(Guid personId)
        {
            return DbSet.Where(i => i.PersonId == personId).Include(p => p.Phone).FirstOrDefault();
        }
    }
}