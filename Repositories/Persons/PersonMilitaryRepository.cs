﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public class PersonMilitaryRepository : Repository<PersonMilitary>, IPersonMilitaryRepository
    {
        public PersonMilitaryRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public void UpdatePersonMilitary(PersonMilitary personMilitary)
        {
            using (MyOhContext db = new MyOhContext())
            {
                personMilitary.MilitaryPerson = null;
                PersonMilitary p = new PersonMilitary() { Id = personMilitary.Id };
                db.PersonMilitary.Attach(p);

                bool saveFailed;
                do
                {
                    saveFailed = false;
                    try
                    {
                        
                        db.Entry(p).CurrentValues.SetValues(personMilitary);
                        db.Entry(p).Property(u => u.ClusterId).CurrentValue = db.Entry(p).Property(u => u.ClusterId).OriginalValue;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update original values from the database 
                        var entry = ex.Entries.Single();
                        entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                    }

                } while (saveFailed); 
            }
        }

        public PersonMilitary GetMy(Guid id)
        {
            using (MyOhContext db = new MyOhContext())
            {
                var old = db.PersonMilitary.FirstOrDefault(x => x.Id == id);
                return old;
            }
        }


        public PersonMilitary GetNoTracking(Guid id)
        {
            return DbSet.AsNoTracking().FirstOrDefault(x => x.Id == id);
        }

       


        public PersonMilitary GetByPersonId(Guid id)
        {
            var person = DbSet.AsNoTracking().Include("MilitaryPerson").FirstOrDefault(x => x.MilitaryPerson.Id == id);
            //var person = DbSet;
            //var person1 = person.AsNoTracking();
            //var person2 = person1.Include("Person");
            //var person3 = person2.FirstOrDefault(x => x.MilitaryPerson.Id == id);

            

            return person;
        }
    }
}