﻿using System;
using System.Data.Entity.Migrations.Model;
using OH.DAL.Domain;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{

    public interface IPersonMilitaryRepository : IRepository<PersonMilitary>
    {
        void UpdatePersonMilitary(PersonMilitary personMilitary);
        PersonMilitary GetMy(Guid id);
        PersonMilitary GetNoTracking(Guid id);
        PersonMilitary GetByPersonId(Guid id);
        
    }
}
