﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public class PersonAddressRepository : Repository<PersonAddress>, IPersonAddressRepository
    {
        public PersonAddressRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public PersonAddress GetByIdInclude(Guid id)
        {
            return DbSet.AsNoTracking().Where(i => i.Id.Equals(id)).Include(p => p.Person).Include(a => a.Address).FirstOrDefault();
        }

        public PersonAddress GetNoTracking(Guid id)
        {
            return DbSet.AsNoTracking().Include(a => a.Address).FirstOrDefault(i => i.Id == id);
        }
        public PersonAddress GetByPersonIdAndType(Guid personId, Guid type)
        {
            return DbSet.AsNoTracking().Where(i => i.PersonId == personId && i.AddressType == type).Include(a => a.Address).FirstOrDefault();
        }


        public IEnumerable<PersonAddress> GetByPersonId(Guid personId)
        {
            return DbSet.AsNoTracking().Where(i => i.PersonId == personId).Include(a => a.Address);
        }
    }
}