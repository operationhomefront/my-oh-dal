﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using OH.DAL.Domain;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.ProgramProfile.Caregiver;

namespace OH.DAL.Repositories.Persons
{
    public class PersonCaregiverRepository : Repository<PersonCaregiver>, IPersonCaregiverRepository
    {
        public PersonCaregiverRepository(MyOhContext myOhContext) : base(myOhContext)
        {
        }

        public void UpdateCaregiver(PersonCaregiver careGiver)
        {
            using (MyOhContext db = new MyOhContext())
            {
                careGiver.Person = null;   
                PersonCaregiver p = new PersonCaregiver() { Id = careGiver.Id };
                db.PersonCaregiver.Attach(p);
                
                db.Entry(p).CurrentValues.SetValues(careGiver);
                db.Entry(p).Property(u => u.ClusterId).CurrentValue = db.Entry(p).Property(u => u.ClusterId).OriginalValue;
                db.SaveChanges();
            }
        }

        public PersonCaregiver GetMy(Guid id)
        {
            using (MyOhContext db = new MyOhContext())
            {
                var old = db.PersonCaregiver.Include("Person").FirstOrDefault(x => x.Id == id);
                return old;
            }
        }


        public PersonCaregiver GetNoTracking(Guid id)
        {
            return DbSet.AsNoTracking().Include("Person").FirstOrDefault(x => x.Id == id);
        }


        public void AddUpdateCaregiver(PersonCaregiver careGiver)
        {
            using (MyOhContext db = new MyOhContext())
            {

                var old = db.Persons.AsNoTracking().FirstOrDefault(x => x.Id == careGiver.Id);
                if (old != null)
                {
                    Person p = new Person() { Id = careGiver.Id };
                    db.Persons.Attach(p);
                    //write security trimming
                    db.Entry(p).CurrentValues.SetValues(careGiver);
                    db.Entry(p).Property(u => u.ClusterId).CurrentValue = db.Entry(p).Property(u => u.ClusterId).OriginalValue;
                
                    db.SaveChanges();

                    DbSet.Add(careGiver);
                }
                else
                {
                    DbSet.Add(careGiver);
                }
                
            }
        }

        public PersonCaregiver GetNoTrackingByPerson(Guid id)
        {
            return DbSet.AsNoTracking().Include("Person").FirstOrDefault(x => x.PersonId == id);
        }
    }
}