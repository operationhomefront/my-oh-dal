﻿using System;
using System.Collections.Generic;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public interface IPersonAddressRepository : IRepository<PersonAddress>
    {

        PersonAddress GetByIdInclude(Guid id);
        PersonAddress GetByPersonIdAndType(Guid personId,Guid type);
        PersonAddress GetNoTracking(Guid id);
        IEnumerable<PersonAddress> GetByPersonId(Guid personId);

    }
}
