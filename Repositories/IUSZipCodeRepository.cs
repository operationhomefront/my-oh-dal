﻿using OH.DAL.Domain;

namespace OH.DAL.Repositories
{
    public interface IUSZipCodeRepository : ILegacyOHv4Repository<USZipCode>
    {
        USZipCode GetSingleUsZipCode(string zip);
    }
}
