﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class EventRepository : Repository<Event>, IEventRepository
    {
        public EventRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}