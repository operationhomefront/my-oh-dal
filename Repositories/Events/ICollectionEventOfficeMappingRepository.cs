﻿using OH.DAL.Domain.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OH.DAL.Repositories.Events
{
   public interface ICollectionEventOfficeMappingRepository : IRepository<CollectionEventOfficeMapping>
    {
        CollectionEventOfficeMapping GetById(Guid Id);
        IEnumerable<CollectionEventOfficeMapping> GetMappingsByEventId(Guid Id);
    }
}
