﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class RegistrantRepository : Repository<Registrant>, IRegistrantRepository
    {
        public RegistrantRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}