﻿using System.Collections.Generic;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class DistributionLocationRepository : Repository<DistributionLocation>, IDistributionLocationRepository
    {
        public DistributionLocationRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public IEnumerable<DistributionLocation> GetLocationsByEventId(System.Guid Id)
        {
            return null;
            
            //return from l in DbSet
            //    where l.EventDetails. == Id
            //    select l;
        }
    }
}