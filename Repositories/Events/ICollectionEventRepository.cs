﻿using System;
using System.Collections.Generic;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public interface ICollectionEventRepository : IRepository<CollectionEvent>
    {
        IEnumerable<CollectionEvent> GetAllActiveCollectionEvents();
        IEnumerable<CollectionEvent> GetClosedCollectionEvents();
        IEnumerable<CollectionEvent> GetUpcomingCollectionEvents();
        IEnumerable<CollectionEvent> GetAll(string officeName);
        IEnumerable<CollectionEvent> GetAllActiveCollectionEvents(string officeName);
        IEnumerable<CollectionEvent> GetClosedCollectionEvents(string officeName);
        IEnumerable<CollectionEvent> GetUpcomingCollectionEvents(string officeName);
    }
}
