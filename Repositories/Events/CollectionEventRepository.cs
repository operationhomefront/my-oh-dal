﻿using System;
using System.Collections.Generic;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class CollectionEventRepository : Repository<CollectionEvent>, ICollectionEventRepository
    {
        public CollectionEventRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public new CollectionEvent GetById(Guid id)
        {
            return DbSet.FirstOrDefault(a => a.Id == id);
        }

        public new IEnumerable<CollectionEvent>  GetAll()
        {
           
            return DbSet.ToList();
        }
        public IEnumerable<CollectionEvent> GetAll(string officeName)
        {
            return DbSet.Where(a =>
                a.Owner.InternalName == officeName ).ToList();
        }


        public IEnumerable<CollectionEvent> GetAllActiveCollectionEvents()
        {
            return DbSet.Where(a =>
                a.EventClosed == false &&
                a.RegistrationOpensUtc <= DateTime.UtcNow &&
                a.RegistrationClosesUtc >= DateTime.UtcNow ).ToList();
        }

        public IEnumerable<CollectionEvent> GetClosedCollectionEvents()
        {
            return DbSet.Where(a =>
                (a.EventClosed == true || a.RegistrationClosesUtc <= DateTime.UtcNow) ).ToList();
        }

        public IEnumerable<CollectionEvent> GetUpcomingCollectionEvents()
        {
            return DbSet.Where(a =>
                a.EventClosed == false &&
                a.RegistrationOpensUtc > DateTime.UtcNow ).ToList();
        }

        public IEnumerable<CollectionEvent> GetAllActiveCollectionEvents(string officeName)
        {
            return DbSet.Where(a =>
                a.EventClosed == false &&
                a.Owner.InternalName==officeName &&
                a.RegistrationOpensUtc <= DateTime.UtcNow &&
                a.RegistrationClosesUtc >= DateTime.UtcNow ).ToList();
        }

        public IEnumerable<CollectionEvent> GetClosedCollectionEvents(string officeName)
        {
            return DbSet.Where(a =>
                (a.EventClosed == true || a.RegistrationClosesUtc <= DateTime.UtcNow) &&
                a.Owner.InternalName == officeName ).ToList();
        }

        public IEnumerable<CollectionEvent> GetUpcomingCollectionEvents(string officeName)
        {
            return DbSet.Where(a =>
                a.EventClosed == false &&
                a.Owner.InternalName == officeName &&
                a.RegistrationOpensUtc > DateTime.UtcNow ).ToList();
        }


    }
}