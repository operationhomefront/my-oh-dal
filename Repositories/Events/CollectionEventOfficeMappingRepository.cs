﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;
using System;
using System.Linq;
using System.Collections.Generic;

namespace OH.DAL.Repositories.Events
{
    public class CollectionEventOfficeMappingRepository : Repository<CollectionEventOfficeMapping>, ICollectionEventOfficeMappingRepository
    {
        public CollectionEventOfficeMappingRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public CollectionEventOfficeMapping GetById(Guid Id)
        {
            return DbSet.FirstOrDefault(c => c.Id == Id);
        }

        public IEnumerable<CollectionEventOfficeMapping> GetMappingsByEventId(Guid Id) {
            return DbSet.Where(c => c.CollectionEventId == Id);
        }
    }
}