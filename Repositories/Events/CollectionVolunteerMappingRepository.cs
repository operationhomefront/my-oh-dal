﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using OH.DAL.Domain;
using System.Data.Entity;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class CollectionVolunteerMappingRepository : Repository<CollectionVolunteerMapping>,
        ICollectionVolunteerMappingRepository
    {
        public CollectionVolunteerMappingRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {

        }

        public CollectionVolunteerMapping GetByIdInclude(Guid id)
        {
            return
                DbSet.Where(i => i.Id.Equals(id))
                    .Include(p => p.CollectionVolunteer)
                    .Include(a => a.CollectionVolunteer.Addresses)
                    .FirstOrDefault();

        }

        public List<CollectionVolunteer> GetByVolunteersByEventIdRadius(Guid id,
            System.Data.Entity.Spatial.DbGeography eventLocation, double meterRadius)
        {
            return DbSet.AsNoTracking()
                .Where(i => i.CollectionEventId == id)
                .Select(i => i.CollectionVolunteer)
                .Where(
                    vol =>
                        vol.Addresses != null &&
                        vol.Addresses.Any(
                            i =>
                                i.AddressType == new Guid("249EF9CC-8EC1-432D-BDDE-763F2F4DAFA2") &&
                                i.Address.Location.Distance(eventLocation) <= meterRadius)

                ).ToList();
        }
    }
}