﻿using System.Collections.Generic;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public interface IDistributionEventRepository : IRepository<DistributionEvent>
    {
        IEnumerable<DistributionEvent> GetActiveDistributionEvents();
        IEnumerable<DistributionEvent> GetClosingActiveDistributionEvents();
        IEnumerable<DistributionEvent> GetClosedDistributionEvents();
        IEnumerable<DistributionEvent> GetUpcomingDistributionEvents();

    }
}
