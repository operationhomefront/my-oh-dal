﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public interface ICollectionLocationRepository : IRepository<CollectionLocation>
    {

    }
}
