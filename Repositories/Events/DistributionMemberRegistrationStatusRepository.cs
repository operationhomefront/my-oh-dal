﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class DistributionMemberRegistrationStatusRepository : Repository<DistributionMemberRegistrationStatus>, IDistributionMemberRegistrationStatusRepository
    {
        public DistributionMemberRegistrationStatusRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}