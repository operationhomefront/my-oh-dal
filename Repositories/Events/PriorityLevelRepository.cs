﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class PriorityLevelRepository : Repository<PriorityLevel>, IPriorityLevelRepository
    {
        public PriorityLevelRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}