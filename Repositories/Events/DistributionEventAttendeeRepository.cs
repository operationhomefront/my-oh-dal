﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class DistributionEventAttendeeRepository : Repository<DistributionEventAttendee>, IDistributionEventAttendeeRepository
    {
        public DistributionEventAttendeeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}