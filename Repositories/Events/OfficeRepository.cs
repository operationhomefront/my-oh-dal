﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class OfficeRepository : Repository<Office>, IOfficeRepository
    {
        public OfficeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}