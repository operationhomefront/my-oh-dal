﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class CollectionVolunteerRepository : Repository<CollectionVolunteer>, ICollectionVolunteerRepository
    {
        public CollectionVolunteerRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}