﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class DistributionEventRegistrantRepository : Repository<DistributionEventRegistrant>, IDistributionEventRegistrantRepository
    {
        public DistributionEventRegistrantRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}