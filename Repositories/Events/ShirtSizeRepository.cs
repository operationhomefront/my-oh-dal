﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class ShirtSizeRepository : Repository<ShirtSize>, IShirtSizeRepository
    {
        public ShirtSizeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}