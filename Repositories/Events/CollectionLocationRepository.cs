﻿using System;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class CollectionLocationRepository : Repository<CollectionLocation>, ICollectionLocationRepository
    {
        public CollectionLocationRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
        public new CollectionLocation GetById(Guid id)
        {
            return DbSet.FirstOrDefault(a => a.Id == id);
        }

    }
}