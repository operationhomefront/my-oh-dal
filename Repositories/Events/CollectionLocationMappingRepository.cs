﻿using System;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class CollectionLocationMappingRepository : Repository<CollectionLocationMapping>, ICollectionLocationMappingRepository
    {
        public CollectionLocationMappingRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public new CollectionLocationMapping GetById(Guid id)
        {
            return DbSet.FirstOrDefault(a => a.Id == id);
        }
    }
}