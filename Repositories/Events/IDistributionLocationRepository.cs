﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public interface IDistributionLocationRepository : IRepository<DistributionLocation>
    {
        IEnumerable<DistributionLocation> GetLocationsByEventId(Guid Id);
    }
}
