﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class GradeRepository : Repository<Grade>, IGradeRepository
    {
        public GradeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}