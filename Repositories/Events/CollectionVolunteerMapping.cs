﻿using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class CollectionVolunteerMappingRepository : Repository<CollectionVolunteerMapping>, ICollectionVolunteerMappingRepository
    {
        public CollectionVolunteerMappingRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}