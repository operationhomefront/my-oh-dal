﻿using System;
using System.Collections.Generic;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public interface ICollectionVolunteerMappingRepository : IRepository<CollectionVolunteerMapping>
    {
        CollectionVolunteerMapping GetByIdInclude(Guid id);

        List<CollectionVolunteer> GetByVolunteersByEventIdRadius(Guid id,
            System.Data.Entity.Spatial.DbGeography eventLocation, double meterRadius);
    }
}
