﻿using System;
using System.Collections.Generic;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.Events;

namespace OH.DAL.Repositories.Events
{
    public class DistributionEventRepository : Repository<DistributionEvent>, IDistributionEventRepository
    {
        public DistributionEventRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public IEnumerable<DistributionEvent> GetActiveDistributionEvents()
        {
            return DbSet.Where(a =>
                   a.EventClosed == false &&
                   a.RegistrationOpensUtc <= DateTime.UtcNow &&
                   a.RegistrationClosesUtc >= DateTime.UtcNow ).ToList();
        }

        public IEnumerable<DistributionEvent> GetClosingActiveDistributionEvents()
        {
            return DbSet.Where(a =>
                (a.EventClosed == false && a.RegistrationClosesUtc <= DateTime.UtcNow )
                ||
                (a.Locations.Any(l => l.RegistrationClosesUtc <= DateTime.UtcNow && l.LocationClosed == false))).ToList();
        }

        public IEnumerable<DistributionEvent> GetClosedDistributionEvents()
        {
            return DbSet.Where(a =>
                (a.EventClosed == true || a.RegistrationClosesUtc <= DateTime.UtcNow) ).ToList();
        }

        public IEnumerable<DistributionEvent> GetUpcomingDistributionEvents()
        {
            return DbSet.Where(a =>
                a.EventClosed == false &&
                a.RegistrationOpensUtc > DateTime.UtcNow).ToList();
        }
    }
}