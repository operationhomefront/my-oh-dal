﻿using System;
using OH.DAL.Domain.Phones;

namespace OH.DAL.Repositories.Phones
{
    public interface IPhoneRepository : IRepository<Phone>
    {
        void UpdatePhone(Phone phone);
        Phone GetNoTracking(Guid id);
    }

}
