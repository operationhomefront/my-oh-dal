﻿using System;
using System.Linq;
using OH.DAL.Domain.Phones;

namespace OH.DAL.Repositories.Phones
{
    public class PhoneRepository : Repository<Phone>, IPhoneRepository
    {
        public PhoneRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public void UpdatePhone(Phone phone)
        {
            using (MyOhContext db = new MyOhContext())
            {
                try
                {
                    Phone old = new Phone { Id = phone.Id };
                    db.Phones.Attach(old);
                    db.Entry(old).CurrentValues.SetValues(phone);
                    db.SaveChanges();
                }
                catch (Exception er)
                {
                    var s = er.Message;
                    throw;
                }

            }
        }

        public Phone GetNoTracking(Guid id)
        {
            return DbSet.AsNoTracking().FirstOrDefault(m => m.Id==id);
        }
    }
}