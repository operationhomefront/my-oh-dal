﻿using OH.DAL.Domain;
using OH.DAL.Domain.ProgramProfile;
using OH.DAL.Domain.ProgramProfile.Military;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public class ProfileMilitaryAttachmentRepository : Repository<ProfileMilitaryAttachment>, IProfileMilitaryAttachmentRepository
    {
        public ProfileMilitaryAttachmentRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}