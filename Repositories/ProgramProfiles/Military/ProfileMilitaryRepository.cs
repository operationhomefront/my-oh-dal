﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using OH.DAL.Domain.ProgramProfile.Military;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public class ProfileMilitaryRepository : Repository<ProfileMilitary>, IProfileMilitaryRepository
    {
        public ProfileMilitaryRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public void UpdateProfileMilitary(ProfileMilitary profileMilitary)
        {
            using (MyOhContext db = new MyOhContext())
            {
                profileMilitary.Attachments = null;
                profileMilitary.MilitaryProfile = null;
                profileMilitary.WorkFlowItems = null;
                var old = db.ProfileMilitary.FirstOrDefault(x => x.Id == profileMilitary.Id);
                db.Entry(old).CurrentValues.SetValues(profileMilitary);

                db.SaveChanges();
            }
        }

        public ProfileMilitary Add(ProfileMilitary model)
        {
            return DbSet.Add(model);
        }


        public ProfileMilitary GetByPersonId(Guid personId)
        {
           
            return DbSet.Include("MilitaryProfile").FirstOrDefault(x => x.MilitaryProfile.MilitaryPerson.Id == personId);
        }
    }
}