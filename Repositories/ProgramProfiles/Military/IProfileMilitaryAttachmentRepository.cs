﻿using OH.DAL.Domain.ProgramProfile;
using OH.DAL.Domain.ProgramProfile.Military;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public interface IProfileMilitaryAttachmentRepository : IRepository<ProfileMilitaryAttachment>
    {

    }
}
