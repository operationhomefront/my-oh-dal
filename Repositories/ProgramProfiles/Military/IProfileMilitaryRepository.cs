﻿using System;
using OH.DAL.Domain.ProgramProfile;
using OH.DAL.Domain.ProgramProfile.Military;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public interface IProfileMilitaryRepository : IRepository<ProfileMilitary>
    {
        ProfileMilitary Add(ProfileMilitary model);
        ProfileMilitary GetByPersonId(Guid personId);
        void UpdateProfileMilitary(ProfileMilitary model);
    }
}