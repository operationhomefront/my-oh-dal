﻿using System;
using OH.DAL.Domain;
using OH.DAL.Domain.ProgramProfile.Caregiver;

namespace OH.DAL.Repositories.ProgramProfiles.CareGiver
{
    public interface IProfileCaregiverRepository : IRepository<ProfileCaregiver>
    {
        void UpdateProfileCaregiver(ProfileCaregiver profileCaregiver);
        ProfileCaregiver GetByIdNoTracking(Guid id);
        ProfileCaregiver GetByPersonIdNoTracking(Guid id);
    }
}
