﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using OH.DAL.Domain;
using OH.DAL.Domain.ProgramProfile.Caregiver;

namespace OH.DAL.Repositories.ProgramProfiles.CareGiver
{
    public class ProfileCaregiverRepository : Repository<ProfileCaregiver>, IProfileCaregiverRepository
    {
        public ProfileCaregiverRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {

        }

        public new ProfileCaregiver GetById(Guid id)
        {
           return DbSet.FirstOrDefault(m=>m.Id == id);
        }
        public ProfileCaregiver GetByIdNoTracking(Guid id)
        {
            return DbSet.AsNoTracking().FirstOrDefault(m => m.Id == id);
        }
        public ProfileCaregiver GetByPersonIdNoTracking(Guid id)
        {
            return DbSet.AsNoTracking().Include("Profile").FirstOrDefault(m => m.Profile.PersonId == id);
        }
        public void UpdateProfileCaregiver(ProfileCaregiver profileCaregiver)
        {
            using (MyOhContext db = new MyOhContext())
            {

                var old = db.ProfileCaregiver.FirstOrDefault(x => x.Id == profileCaregiver.Id);

                db.Entry(old).CurrentValues.SetValues(profileCaregiver);
                
                db.SaveChanges();
            }
        }
    }
}