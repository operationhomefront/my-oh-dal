﻿using OH.DAL.Domain;
using OH.DAL.Domain.ProgramProfile.Caregiver;

namespace OH.DAL.Repositories.ProgramProfiles.CareGiver
{
    public class ProfileCaregiverAttachmentRepository : Repository<ProfileCaregiverAttachment>, IProfileCaregiverAttachmentRepository
    {
        public ProfileCaregiverAttachmentRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}