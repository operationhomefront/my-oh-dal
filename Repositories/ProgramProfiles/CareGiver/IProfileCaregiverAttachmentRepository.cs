﻿using OH.DAL.Domain;
using OH.DAL.Domain.ProgramProfile.Caregiver;

namespace OH.DAL.Repositories.ProgramProfiles.CareGiver
{
    public interface IProfileCaregiverAttachmentRepository : IRepository<ProfileCaregiverAttachment>
    {

    }
}
