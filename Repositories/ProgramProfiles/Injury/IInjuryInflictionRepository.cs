﻿using System;
using System.Security.Cryptography.X509Certificates;
using OH.DAL.Domain.ProgramProfile.Injury;

namespace OH.DAL.Repositories.ProgramProfiles.Injury
{
    public interface IInjuryInflictionRepository : IRepository<InjuryInfliction>
    {
        void Add(InjuryInfliction injury);
        InjuryInfliction GetLatest();
    }
}
