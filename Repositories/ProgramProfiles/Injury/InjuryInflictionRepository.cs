﻿using System.Linq;
using OH.DAL.Domain.ProgramProfile.Injury;

namespace OH.DAL.Repositories.ProgramProfiles.Injury
{
    public class InjuryInflictionRepository : Repository<InjuryInfliction>, IInjuryInflictionRepository
    {
        public InjuryInflictionRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public void Add(InjuryInfliction injury)
        {
            DbSet.Add(injury);
        }

        public InjuryInfliction GetLatest()
        {
            var val =DbSet.Max(x => x.ClusterId);
            return DbSet.FirstOrDefault(x=>x.ClusterId == val);
        }
    }
}