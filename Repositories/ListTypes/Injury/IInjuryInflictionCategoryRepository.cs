﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OH.DAL.Domain.ListTypes.Injury;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Injury
{
    public interface IInjuryInflictionCategoryRepository : IRepository<ListInjuryInflictionCategory>
    {
    }
}
