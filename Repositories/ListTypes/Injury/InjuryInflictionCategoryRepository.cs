﻿using OH.DAL.Domain.ListTypes.Injury;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Injury
{
    public class InjuryInflictionCategoryRepository : Repository<ListInjuryInflictionCategory>, IInjuryInflictionCategoryRepository
    {
        public InjuryInflictionCategoryRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}