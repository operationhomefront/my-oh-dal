﻿using OH.DAL.Domain;
using OH.DAL.Domain.ListTypes.Person;

namespace OH.DAL.Repositories.ListTypes.Person
{
    public interface IRelationshipRepository : IRepository<Relationship>
    {

    }
}
