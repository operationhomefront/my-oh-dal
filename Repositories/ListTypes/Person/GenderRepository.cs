﻿using System;
using System.Collections.Generic;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.ListTypes.Person;

namespace OH.DAL.Repositories.ListTypes.Person
{
    public class GenderRepository : Repository<Gender>, IListTypeRepository,IGenderRepository
    {
        public GenderRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public System.Collections.ObjectModel.Collection<Domain.ListTypes.ListType> GetByCallingObject(object obj)
        {
            throw new System.NotImplementedException();
        }

        public new Domain.ListTypes.ListType GetById(System.Guid id)
        {
            throw new System.NotImplementedException();
        }

        public new IEnumerable<Domain.ListTypes.ListType> GetAll()
        {
            var list = DbSet.Where(x => x.ObjectSource.Equals(typeof(Gender)));
            return list;
        }

        public System.Guid AddOrUpdate(Domain.ListTypes.ListType entity)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Domain.ListTypes.ListType> GetByType(Type type)
        {
            throw new System.NotImplementedException();
        }


        public IEnumerable<Domain.ListTypes.ListType> GetByTypeTest<T>() where T : class
        {
            throw new NotImplementedException();
        }
    }
}