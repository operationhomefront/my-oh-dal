﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OH.DAL.Domain.ListTypes;

namespace OH.DAL.Repositories.ListTypes
{
    public interface IListTypeRepository : IRepository<ListType>
    {
        IEnumerable<ListType> GetByType(Type type);
        IEnumerable<ListType> GetByTypeTest<T>()where T : class;
    }
}
