﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using OH.DAL.Domain.ListTypes;
using OH.DAL.Domain.ListTypes.Person;
using OH.DAL.Repositories.ListTypes.Person;

namespace OH.DAL.Repositories.ListTypes
{
    public class ListTypeRepository : Repository<ListType>, IListTypeRepository
    {
        public ListTypeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {

        }
        public IEnumerable<ListType> GetByType(Type type)
        {
            bool sortable = false;
            var list = new List<ListType>();
            foreach (var listType in DbSet.ToList())
            {
                var currentListType = listType;
                if (listType.SortOrder != null)
                    sortable = true;

                if (type.Name.Equals(currentListType.GetType().Name))
                {
                    list.Add(currentListType);
                }
            }

            if (sortable)
            {
                return list.OrderBy(m => m.SortOrder);
            }
            else
            {
                return list;
            }
        }


        public IEnumerable<ListType> GetByTypeTest<T>() where T : class
        {
            T item = (T)Activator.CreateInstance(typeof(T));
            return DbSet.Where(x => x.ObjectSource.Equals(item));
        }
    }
}