﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.ListTypes;
using OH.DAL.Domain.ListTypes.General;

namespace OH.DAL.Repositories.ListTypes.General
{
    public class StatesRepository : Repository<States>,IListTypeRepository, IStatesRepository
    {
        public StatesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public System.Collections.ObjectModel.Collection<Domain.ListTypes.ListType> GetByCallingObject(object obj)
        {
            throw new System.NotImplementedException();
        }

        public new Domain.ListTypes.ListType GetById(System.Guid id)
        {
            throw new System.NotImplementedException();
        }

        public new System.Collections.Generic.IEnumerable<Domain.ListTypes.ListType> GetAll()
        {
            var list = DbSet.Where(x => x.ObjectSource.Equals(typeof(States)));
            return list;
        }

        public System.Guid AddOrUpdate(Domain.ListTypes.ListType entity)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<ListType> GetByType(Type type)
        {
            throw new System.NotImplementedException();
        }


        public System.Collections.Generic.IEnumerable<ListType> GetByTypeTest<T>() where T : class
        {
            throw new NotImplementedException();
        }
    }
}