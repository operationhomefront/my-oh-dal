﻿using OH.DAL.Domain;
using OH.DAL.Domain.ListTypes.General;

namespace OH.DAL.Repositories.ListTypes.General
{
    public interface IStatesRepository : IRepository<States>
    {

    }
}
