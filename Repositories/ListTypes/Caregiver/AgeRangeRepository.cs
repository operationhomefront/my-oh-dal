﻿using System;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.ListTypes.CareGiver;
using OH.DAL.Domain.ListTypes.General;

namespace OH.DAL.Repositories.ListTypes.Caregiver
{
    public class AgeRangeRepository : Repository<AgeRange>,IListTypeRepository, IAgeRangeRepository
    {
        public AgeRangeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    
public System.Collections.ObjectModel.Collection<Domain.ListTypes.ListType> GetByCallingObject(object obj)
{
 	throw new System.NotImplementedException();
}

public new Domain.ListTypes.ListType GetById(System.Guid id)
{
 	throw new System.NotImplementedException();
}

public new System.Collections.Generic.IEnumerable<Domain.ListTypes.ListType> GetAll()
{
 	 var list = DbSet.Where(x => x.ObjectSource.Equals(typeof(AgeRange)));
            return list;
}

public System.Guid AddOrUpdate(Domain.ListTypes.ListType entity)
{
 	throw new System.NotImplementedException();
}

public System.Collections.Generic.IEnumerable<Domain.ListTypes.ListType> GetByType(Type type)
{
    throw new System.NotImplementedException();
}


public System.Collections.Generic.IEnumerable<Domain.ListTypes.ListType> GetByTypeTest<T>() where T : class
{
    throw new NotImplementedException();
}
    }
}