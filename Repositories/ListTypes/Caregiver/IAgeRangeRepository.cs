﻿using OH.DAL.Domain;
using OH.DAL.Domain.ListTypes.CareGiver;

namespace OH.DAL.Repositories.ListTypes.Caregiver
{
    public interface IAgeRangeRepository : IRepository<AgeRange>
    {

    }
}
