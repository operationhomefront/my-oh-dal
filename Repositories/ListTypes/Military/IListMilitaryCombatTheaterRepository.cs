﻿using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public interface IListMilitaryCombatTheaterRepository : IRepository<ListMilitaryCombatTheater>
    {

    }
}