﻿using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public class ListMilitaryCombatTheaterRepository : Repository<ListMilitaryCombatTheater>, IListMilitaryCombatTheaterRepository
    {
        public ListMilitaryCombatTheaterRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

    }
}