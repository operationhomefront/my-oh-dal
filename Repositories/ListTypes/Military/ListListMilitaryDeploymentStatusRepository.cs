﻿using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public class ListListMilitaryDeploymentStatusRepository : Repository<ListMilitaryDeploymentStatus>, IListMilitaryDeploymentStatusRepository
    {
        public ListListMilitaryDeploymentStatusRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }


    }
}