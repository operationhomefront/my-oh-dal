﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public interface IListMilitaryBranchRepository : IRepository<ListMilitaryBranch>
    {
        ICollection<ListMilitaryBranch> GetActiveDutyMilitaryBranches(bool isActiveDuty);
    }
}
