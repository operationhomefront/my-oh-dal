﻿using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public class ListListMilitaryServiceStatusRepository : Repository<ListMilitaryServiceStatus>, IListMilitaryServiceStatusRepository
    {
        public ListListMilitaryServiceStatusRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }


    }
}