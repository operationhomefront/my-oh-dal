﻿using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public class ListListMilitaryRankRepository : Repository<ListMilitaryRank>, IListMilitaryRankRepository
    {
        public ListListMilitaryRankRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }


    }
}