﻿using System.Collections.Generic;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.ListTypes.Military;
using OH.DAL.Domain.ProgramProfile;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public class ListMilitaryBranchRepository : Repository<ListMilitaryBranch>, IListMilitaryBranchRepository
    {
        public ListMilitaryBranchRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public ICollection<ListMilitaryBranch> GetActiveDutyMilitaryBranches(bool isActiveDuty)
        {
            return DbSet.Where(x => x.OptionalValue == isActiveDuty.ToString()).ToList();
        }
    }
}