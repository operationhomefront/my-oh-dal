﻿using OH.DAL.Domain.ListTypes.Military;

namespace OH.DAL.Repositories.ProgramProfiles.Military
{
    public class ListMilitaryVeteranStatusRepository : Repository<ListMilitaryVeteranStatus>, IListMilitaryVeteranStatusRepository
    {
        public ListMilitaryVeteranStatusRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }


    }
}