﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormDescriptionPropertiesRepository : Repository<OHFormDescriptionProperties>, IOHFormDescriptionPropertiesRepository
    {
        public OHFormDescriptionPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}