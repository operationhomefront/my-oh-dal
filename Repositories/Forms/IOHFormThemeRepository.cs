﻿using OH.DAL.Domain;
using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public interface IOHFormThemeRepository : IRepository<OHFormTheme>
    {

    }
}
