﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public interface IOHFormSectionRepository : IRepository<OHFormSection>
    {

    }
}