﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormDateTimePropertiesRepository : Repository<OHFormDateTimeProperties>, IOHFormDateTimePropertiesRepository
    {
        public OHFormDateTimePropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}