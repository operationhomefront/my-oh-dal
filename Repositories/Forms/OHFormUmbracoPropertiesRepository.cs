﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormUmbracoPropertiesRepository : Repository<OHFormUmbracoProperties>, IOHFormUmbracoPropertiesRepository
    {
        public OHFormUmbracoPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}