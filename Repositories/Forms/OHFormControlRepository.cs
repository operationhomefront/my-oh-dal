﻿using OH.DAL.Domain;
using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormControlRepository : Repository<OHFormControl>, IOHFormControlRepository
    {
        public OHFormControlRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}