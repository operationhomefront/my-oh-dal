﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormServiceMemberPropertiesRepository : Repository<OHFormServiceMemberProperties>, IOHFormServiceMemberPropertiesRepository
    {
        public OHFormServiceMemberPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}