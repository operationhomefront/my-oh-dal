﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public interface IOHFormValueRepository : IRepository<OHFormValue>
    {

    }
}