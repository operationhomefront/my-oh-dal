﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormFileUploadPropertiesRepository : Repository<OHFormFileUploadProperties>, IOHFormFileUploadPropertiesRepository
    {
        public OHFormFileUploadPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}