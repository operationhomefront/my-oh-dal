﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public interface IOHFormControlRepository : IRepository<OHFormControl>
    {

    }
}
