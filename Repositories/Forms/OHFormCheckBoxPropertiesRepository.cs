﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormCheckBoxPropertiesRepository : Repository<OHFormCheckBoxProperties>, IOHFormCheckBoxPropertiesRepository
    {
        public OHFormCheckBoxPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}