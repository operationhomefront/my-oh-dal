﻿using OH.DAL.Domain;
using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public interface IOHFormRepository : IRepository<OHForm>
    {

    }
}
