﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormValueRepository : Repository<OHFormValue>, IOHFormValueRepository
    {
        public OHFormValueRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}