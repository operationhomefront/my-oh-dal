﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormRadioButtonsPropertiesRepository : Repository<OHFormRadioButtonsProperties>, IOHFormRadioButtonsPropertiesRepository
    {
        public OHFormRadioButtonsPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}