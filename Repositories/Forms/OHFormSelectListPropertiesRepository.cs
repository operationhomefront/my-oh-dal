﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormSelectListPropertiesRepository : Repository<OHFormSelectListProperties>, IOHFormSelectListPropertiesRepository
    {
        public OHFormSelectListPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}