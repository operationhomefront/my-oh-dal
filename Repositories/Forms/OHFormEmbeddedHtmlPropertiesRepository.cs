﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormEmbeddedHtmlPropertiesRepository : Repository<OHFormEmbeddedHtmlProperties>, IOHFormEmbeddedHtmlPropertiesRepository
    {
        public OHFormEmbeddedHtmlPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}