﻿using OH.DAL.Domain;
using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormRepository : Repository<OHForm>, IOHFormRepository
    {
        public OHFormRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}