using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormSectionRepository : Repository<OHFormSection>, IOHFormSectionRepository
    {
        public OHFormSectionRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}