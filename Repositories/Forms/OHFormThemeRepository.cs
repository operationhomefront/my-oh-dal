﻿using OH.DAL.Domain;
using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormThemeRepository : Repository<OHFormTheme>, IOHFormThemeRepository
    {
        public OHFormThemeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}