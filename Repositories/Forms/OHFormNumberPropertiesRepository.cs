﻿using OH.DAL.Domain.Forms;

namespace OH.DAL.Repositories.Forms
{
    public class OHFormNumberPropertiesRepository : Repository<OHFormNumberProperties>, IOHFormNumberPropertiesRepository
    {
        public OHFormNumberPropertiesRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}