﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.LegacyOHv4;

namespace OH.DAL.Repositories
{
    public class USZipCodeRepository : LegacyOHv4Repository<USZipCode>, IUSZipCodeRepository
    {
        public USZipCodeRepository(LegacyOHv4Context LegacyOHv4Context)
            : base(LegacyOHv4Context)
        {
        }

        public USZipCode GetSingleUsZipCode(string zip)
        {
            return DbSet.Single(x => x.ZipCode == zip);
        }
    }
}