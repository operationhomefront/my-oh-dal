﻿using OH.DAL.Domain.Authorization;

namespace OH.DAL.Repositories.Authorization
{
    public interface IMyOhSecurityGroupRepository : IRepository<MyOhSecurityGroup>
    {

    }
}
