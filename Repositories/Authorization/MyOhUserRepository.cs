﻿using System.Collections.Generic;
using System.Linq;
using OH.DAL.Domain.Authorization;

namespace OH.DAL.Repositories.Authorization
{
    public class MyOhUserRepository : Repository<MyOhUser>, IMyOhUserRepository
    {
        public MyOhUserRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {

        }

        public IEnumerable<MyOhUser> GetAllUsers()
        {
            return DbSet.ToList();
        }
    }
}