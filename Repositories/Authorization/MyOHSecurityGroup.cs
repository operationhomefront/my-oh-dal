﻿using OH.DAL.Domain.Authorization;

namespace OH.DAL.Repositories.Authorization
{
    public class MyOhSecurityGroupRepository : Repository<MyOhSecurityGroup>, IMyOhSecurityGroupRepository
    {
        public MyOhSecurityGroupRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}