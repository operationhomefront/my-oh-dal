﻿using OH.DAL.Domain;
using OH.DAL.Domain.Authorization;

namespace OH.DAL.Repositories.Authorization
{
    public class MyOhRoleRepository : Repository<MyOhRole>, IMyOhRoleRepository
    {
        public MyOhRoleRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}