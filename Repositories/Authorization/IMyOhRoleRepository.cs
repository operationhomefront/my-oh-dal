﻿using System.Collections.Generic;
using OH.DAL.Domain.Authorization;

namespace OH.DAL.Repositories.Authorization
{
    public interface IMyOhRoleRepository : IRepository<MyOhRole>
    {
        
    }
}
