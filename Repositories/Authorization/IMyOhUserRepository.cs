﻿using System.Collections.Generic;
using OH.DAL.Domain;
using OH.DAL.Domain.Authorization;

namespace OH.DAL.Repositories.Authorization
{
    public interface IMyOhUserRepository : IRepository<MyOhUser>
    {
        IEnumerable<MyOhUser> GetAllUsers();
    }

}
