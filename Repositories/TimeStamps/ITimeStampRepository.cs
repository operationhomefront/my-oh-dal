﻿using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Repositories.TimeStamps
{
    public interface ITimeStampRepository : IRepository<TimeStamp>
    {

    }
}
