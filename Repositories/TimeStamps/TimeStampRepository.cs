﻿using OH.DAL.Domain.TimeStamps;

namespace OH.DAL.Repositories.TimeStamps
{
    public class TimeStampRepository : Repository<TimeStamp>, ITimeStampRepository
    {
        public TimeStampRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}