﻿using OH.DAL.Domain;
using OH.DAL.Domain.Security;

namespace OH.DAL.Repositories.Security
{
    public class UserMembershipRepository : Repository<UserMembership>, IUserMembershipRepository
    {
        public UserMembershipRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}