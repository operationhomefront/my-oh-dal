﻿using OH.DAL.Domain;
using OH.DAL.Domain.Security;

namespace OH.DAL.Repositories.Security
{
    public class UserGroupRepository : Repository<UserGroup>, IUserGroupRepository
    {
        public UserGroupRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}