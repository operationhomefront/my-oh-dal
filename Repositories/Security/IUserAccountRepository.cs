﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using OH.DAL.Domain;
using OH.DAL.Domain.Security;

namespace OH.DAL.Repositories.Security
{
    public interface IUserAccountRepository : IRepository<UserAccount>
    {
        Task<UserAccount> GetByIdAsync(Guid userGuid);

        Task<UserAccount> GetByUsernameAsync(string username);

        Task UpdateChangesAsync(UserAccount user);
        Task InsertAsync(UserAccount user);
        Task DeleteAsync(Guid userGuid);
        
    }
}
