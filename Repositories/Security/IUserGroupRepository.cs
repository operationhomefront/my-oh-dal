﻿using OH.DAL.Domain;
using OH.DAL.Domain.Security;

namespace OH.DAL.Repositories.Security
{
    public interface IUserGroupRepository : IRepository<UserGroup>
    {

    }
}
