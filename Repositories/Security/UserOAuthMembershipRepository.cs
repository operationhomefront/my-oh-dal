﻿using OH.DAL.Domain;
using OH.DAL.Domain.Security;

namespace OH.DAL.Repositories.Security
{
    public class UserOAuthMembershipRepository : Repository<UserOAuthMembership>, IUserOAuthMembershipRepository
    {
        public UserOAuthMembershipRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}