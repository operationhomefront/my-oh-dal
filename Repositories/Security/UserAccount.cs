﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using OH.DAL.Domain;
using OH.DAL.Domain.Security;
using System.Threading.Tasks;

namespace OH.DAL.Repositories.Security
{
    public class UserAccountRepository : Repository<UserAccount>, IUserAccountRepository
    {
        public UserAccountRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public async Task<UserAccount> GetByIdAsync(System.Guid userId)
        {
            return await DbSet.FirstOrDefaultAsync(u => u.Id == userId);
        }


        public async Task<UserAccount> GetByUsernameAsync(string username)
        {

            return await DbSet.FirstOrDefaultAsync(u => u.UserName == username);
            //return DbSet.FirstOrDefaultAsync(u => u.UserName == username);
            //return await DbSet.Where(account => account.Email == "email").ToListAsync();
        }

        public async Task UpdateChangesAsync(UserAccount user)
        {
            using (var db = new MyOhContext())
            {
                db.OHUsers.Attach(user);
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }

        public async Task InsertAsync(UserAccount user)
        {
            using (var db = new MyOhContext())
            {
                db.OHUsers.Add(user);
                await db.SaveChangesAsync();
            }   
        }

        public async Task DeleteAsync(Guid userGuid)
        {
            using (var db = new MyOhContext())
            {
                var user = new UserAccount{Id = userGuid};

                db.OHUsers.Attach(user);
                db.Entry(user).State = EntityState.Deleted;
                await db.SaveChangesAsync();
            }
        }
    }
}