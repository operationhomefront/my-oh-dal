﻿using System;
using OH.DAL.Domain.EmailAddresses;

namespace OH.DAL.Repositories.EmailAddresses
{
    public interface IEmailAddressRepository : IRepository<EmailAddress>
    {
        void UpdateEmailAddres(EmailAddress emailAddress);
        EmailAddress GetNoTracking(Guid id);
    }
}
