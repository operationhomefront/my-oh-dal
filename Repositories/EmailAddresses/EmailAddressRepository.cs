﻿using System;
using System.Linq;
using OH.DAL.Domain.EmailAddresses;

namespace OH.DAL.Repositories.EmailAddresses
{
    public class EmailAddressRepository : Repository<EmailAddress>, IEmailAddressRepository
    {
        public EmailAddressRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }



        public void UpdateEmailAddres(EmailAddress emailAddress)
        {
            using (MyOhContext db = new MyOhContext())
            {
                try
                {
                    
                    EmailAddress old = new EmailAddress { Id = emailAddress.Id };
                    db.EmailAddresses.Attach(old);
                    db.Entry(old).CurrentValues.SetValues(emailAddress);
                    db.Entry(old).Property(u => u.ClusterId).CurrentValue = db.Entry(old).Property(u => u.ClusterId).OriginalValue;
                
                    db.SaveChanges();
                }
                catch (Exception er)
                {
                    var s = er.Message;
                    throw;
                }

            }
        }


        public EmailAddress GetNoTracking(Guid id)
        {
            return DbSet.AsNoTracking().FirstOrDefault(m => m.Id == id);
        }
    }
}