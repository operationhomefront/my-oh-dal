﻿using System;
using System.Collections;
using System.Collections.Generic;
using OH.DAL.Domain;
using OH.DAL.Domain.Meetings;

namespace OH.DAL.Repositories.Meetings
{
    public interface IMeetingRoomRepository : IRepository<MeetingRoom>
    {
        IEnumerable<MeetingRoom> GetAllByLocationId(Guid locationId);
    }
}
