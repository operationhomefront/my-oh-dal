﻿using System;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Meetings;

namespace OH.DAL.Repositories.Meetings
{
    public class MeetingAttendeeRepository : Repository<MeetingAttendee>, IMeetingAttendeeRepository
    {
        public MeetingAttendeeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {

        }

        public MeetingAttendee GetByIdInclude(Guid id)
        {
            return DbSet.Where(c => c.Id.Equals(id)).Include(m => m.Meeting).FirstOrDefault();
        }
    }
}