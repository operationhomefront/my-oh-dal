﻿using System;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Meetings;

namespace OH.DAL.Repositories.Meetings
{
    public class MeetingRepository : Repository<Meeting>, IMeetingRepository
    {
        public MeetingRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        //public new Meeting GetById(Guid id)
        //{
        //    return DbSet.FirstOrDefault(a => a.Id == id);
        //}

        public Meeting GetByIdIncludeMeetingAttendees(Guid id)
        {
            return DbSet.Where(i => i.Id.Equals(id)).Include(m => m.MeetingAttendees).FirstOrDefault();
        }
    }
}