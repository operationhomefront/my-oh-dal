﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Meetings;

namespace OH.DAL.Repositories.Meetings
{
    public class MeetingRoomRepository : Repository<MeetingRoom>, IMeetingRoomRepository
    {
        public MeetingRoomRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public IEnumerable<MeetingRoom> GetAllByLocationId(Guid locationId)
        {
            return DbSet.Where(l=>l.MeetingLocationId.Equals(locationId)).Include(l=>l.MeetingLocation).AsEnumerable();
        }
    }
}