﻿using System;
using OH.DAL.Domain;
using OH.DAL.Domain.Meetings;

namespace OH.DAL.Repositories.Meetings
{
    public interface IMeetingRepository : IRepository<Meeting>
    {
        Meeting GetByIdIncludeMeetingAttendees(Guid id);
    }
}
