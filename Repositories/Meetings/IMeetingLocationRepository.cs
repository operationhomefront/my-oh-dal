﻿using OH.DAL.Domain.Meetings;

namespace OH.DAL.Repositories.Meetings
{
    public interface IMeetingLocationRepository : IRepository<MeetingLocation>
    {

    }
}
