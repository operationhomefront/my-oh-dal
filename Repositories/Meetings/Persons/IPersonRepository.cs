﻿using System;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public interface IPersonRepository : IRepository<Person>
    {
        Person GetNoTracking(Guid Id);
        Person GetLatest();

        bool Update(Person p);
    }
}
