﻿using OH.DAL.Domain;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public class CaregiverRepository : Repository<Caregiver>, ICaregiverRepository
    {
        public CaregiverRepository(MyOhContext myOhContext) : base(myOhContext)
        {
        }
    }
}