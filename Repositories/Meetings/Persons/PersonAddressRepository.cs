﻿using System;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public class PersonAddressRepository : Repository<PersonAddress>, IPersonAddressRepository
    {
        public PersonAddressRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public PersonAddress GetByIdInclude(Guid id)
        {
            return DbSet.Where(i => i.Id.Equals(id)).Include(p => p.Person).Include(a => a.Address).FirstOrDefault();
        }
    }
}