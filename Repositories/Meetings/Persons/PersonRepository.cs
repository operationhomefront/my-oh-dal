﻿using System.Data.Entity.Migrations;
using System.Linq;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        private MyOhContext _myOhContext;
        
        public PersonRepository(MyOhContext myOhContext) : base(myOhContext)
        {
            _myOhContext = myOhContext;
        }


        public Person GetNoTracking(System.Guid id)
        {
            return DbSet.AsNoTracking().FirstOrDefault(x => x.Id == id);

            
        }

        public Person GetLatest()
        {
            var val = DbSet.Max(x => x.ClusterId);
            return DbSet.FirstOrDefault(x => x.ClusterId == val);
        }

        public bool Update(Person p)
        {
               
            var val = _myOhContext.Persons.FirstOrDefault(x => x.Id == p.Id);

            if (val != null)
            {

                if (p.FirstName != null && val.FirstName != p.FirstName)
                    val.FirstName = p.FirstName;
                if (p.Last4Ssn != null && val.Last4Ssn != p.Last4Ssn)
                    val.Last4Ssn = p.Last4Ssn;
                if (p.Lastname != null && val.Lastname != p.Lastname)
                    val.Lastname = p.Lastname;
                if (p.MiddleName != null && val.MiddleName != p.MiddleName)
                    val.MiddleName = p.MiddleName;
                if (p.SocialSecurityNumber != null && val.SocialSecurityNumber != p.SocialSecurityNumber)
                    val.SocialSecurityNumber = p.SocialSecurityNumber;
                if (p.DateOfBirth != null && val.DateOfBirth != p.DateOfBirth)
                    val.DateOfBirth = p.DateOfBirth;
                if (p.Gender != null && val.Gender != p.Gender)
                    val.Gender = p.Gender;
                if (p.Suffix != null && val.Suffix != p.Suffix)
                    val.Suffix = p.Suffix;
                if (p.Title != null && val.Title != p.Title)
                    val.Title = p.Title;
                if (p.DriverLicenceNumber != null && val.DriverLicenceNumber != p.DriverLicenceNumber)
                    val.DriverLicenceNumber = p.DriverLicenceNumber;
                if (p.DriverLicenseState != null && val.DriverLicenseState != p.DriverLicenseState)
                    val.DriverLicenseState = p.DriverLicenseState;
                if (p.DriverLicenseExpirationDate != null &&
                    val.DriverLicenseExpirationDate != p.DriverLicenseExpirationDate)
                    val.DriverLicenseExpirationDate = p.DriverLicenseExpirationDate;

                _myOhContext.Persons.AddOrUpdate(val);
                _myOhContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }   
}