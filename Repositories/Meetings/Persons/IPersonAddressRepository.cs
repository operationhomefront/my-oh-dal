﻿using System;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public interface IPersonAddressRepository : IRepository<PersonAddress>
    {

        PersonAddress GetByIdInclude(Guid id);
    }
}
