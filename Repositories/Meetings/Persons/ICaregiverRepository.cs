﻿using OH.DAL.Domain;
using OH.DAL.Domain.Persons;

namespace OH.DAL.Repositories.Persons
{
    public interface ICaregiverRepository : IRepository<Caregiver>
    {

    }
}
