﻿using System;
using OH.DAL.Domain.Meetings;

namespace OH.DAL.Repositories.Meetings
{
    public interface IMeetingAttendeeRepository : IRepository<MeetingAttendee>
    {

        MeetingAttendee GetByIdInclude(Guid id);
    }
}
