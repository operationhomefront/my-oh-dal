﻿using OH.DAL.Domain.Meetings;

namespace OH.DAL.Repositories.Meetings
{
    public class MeetingLocationRepository : Repository<MeetingLocation>, IMeetingLocationRepository
    {
        public MeetingLocationRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}