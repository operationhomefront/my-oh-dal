using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Events;
using OH.DAL.Domain.Persons;
using OH.DAL;
using OH.DAL.Domain.Authorization;
using System.Data;
using System.Data.SqlClient;
using OH.DAL.Domain.Reports;


namespace OH.DAL.Repositories
{
    public class ReportRepository : Repository<Report>, IReportRepository
    {
        private readonly MyOhContext _myOhContext;
        //private DbContext _applicationDbContext;
        private bool _disposed;


        //public ReportRepository(DbContext applicatioDbContext)
        //{            
        //    _applicationDbContext = applicatioDbContext;
        //}


        public ReportRepository( MyOhContext myOhContext)
            : base(myOhContext)
        {
            _myOhContext = myOhContext;

        }

        public IEnumerable<Report> GetAllAvailableReports()
        {
            return DbSet.ToList();
        }
        public IEnumerable<Report> GetAllAvailableReportsById(Guid id)
        {
            return DbSet.Where(r => r.ApplicationId == id).ToList();
        }
        public IEnumerable<Person> GetAllPersonsByFirstName(string firstName)
        {
            //return _myOhContext.Persons.Where(f => f.FirstName.Equals(firstName)).ToList();
            var query = from p in _myOhContext.Persons
                join m in _myOhContext.MeetingAttendees on p.Id equals m.Person.Id
                where p.FirstName == firstName
                select p;

            return query.ToList();

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _myOhContext.Dispose();
                }
            }
            _disposed = true;
        }


        public IEnumerable<Person> GetAllPersons()
        {
            return _myOhContext.Persons.ToList();
        }

        public DataTable GetMafaReport(DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce)
        {
            string connectionString = "Data Source=172.24.16.6;Initial Catalog=OHv2;Persist Security Info=True;User ID=oh.db.user;Password=hrZ6!9@52@Wz"; //ConfigurationManager.AppSettings["LegacyOHv2ConnectionString"];
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand com = new SqlCommand("sp_Report_MAFA_Needs_Met_Case_Log_V2", con))
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("bgnDate", bgnDateTime);
                    com.Parameters.AddWithValue("endDate", endDateTime);
                    com.Parameters.AddWithValue("includeAirForce", ConvertBoolToInt(includeAirForce));
                    com.Parameters.AddWithValue("includeArmy", ConvertBoolToInt(includeArmy));
                    com.Parameters.AddWithValue("includeMarines", ConvertBoolToInt(includeMarines));
                    com.Parameters.AddWithValue("includeNavy", ConvertBoolToInt(includeNavy));
                    try
                    {
                        con.Open();
                        var dataReader = com.ExecuteReader(CommandBehavior.CloseConnection);
                        dt.Load(dataReader);




                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }

            return dt;
        }


        private int ConvertBoolToInt(bool boolToConvert)
        {
            int result = 0;

            if (boolToConvert)
            {
                result = 1;
            }

            return result;
        }

        public void GetUserRoles()
        {
            //_applicationDbContext = _applicationDbContext;

            
            //var p = _applicationDbContext.Set<MyOhUser>().ToList();

            //var dbSet = _applicationDbContext.Set<MyOhUser>();

            //dbSet = dbSet;
            //var p = dbSet.ToList();


        }


        public DataTable BuildGridReport(DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce, string includeReport)
        {
            string connectionString = "Data Source=172.24.16.6;Initial Catalog=OHv2;Persist Security Info=True;User ID=oh.db.user;Password=hrZ6!9@52@Wz"; //ConfigurationManager.AppSettings["LegacyOHv2ConnectionString"];
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand com = new SqlCommand(includeReport, con))
                {
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddWithValue("bgnDate", bgnDateTime);
                    com.Parameters.AddWithValue("endDate", endDateTime);
                    com.Parameters.AddWithValue("includeAirForce", ConvertBoolToInt(includeAirForce));
                    com.Parameters.AddWithValue("includeArmy", ConvertBoolToInt(includeArmy));
                    com.Parameters.AddWithValue("includeMarines", ConvertBoolToInt(includeMarines));
                    com.Parameters.AddWithValue("includeNavy", ConvertBoolToInt(includeNavy));
                    try
                    {
                        con.Open();
                        var dataReader = com.ExecuteReader(CommandBehavior.CloseConnection);
                        dt.Load(dataReader);




                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }

            return dt;
        }
    }
}