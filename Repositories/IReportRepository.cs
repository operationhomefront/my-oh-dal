﻿using System.Collections.Generic;
using OH.DAL.Domain.Persons;
using System.Data;
using System;
using OH.DAL.Domain.Reports;

namespace OH.DAL.Repositories
{
    public interface IReportRepository : IRepository<Report>
    {
        IEnumerable<Person> GetAllPersonsByFirstName(string firstName);

        IEnumerable<Person> GetAllPersons();

        void GetUserRoles();
        DataTable GetMafaReport(DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce);

        DataTable BuildGridReport(DateTime bgnDateTime, DateTime endDateTime, bool includeArmy, bool includeNavy, bool includeMarines, bool includeAirForce, string includeReport);

        IEnumerable<Report> GetAllAvailableReports();
        IEnumerable<Report> GetAllAvailableReportsById(Guid id);
    }
}
