﻿using OH.DAL.Domain;

namespace OH.DAL.Repositories.WorkFlow
{
    public class WorkFlowRepository : Repository<Domain.WorkflowTemplates.Workflow>, IWorkFlowRepository
    {
        public WorkFlowRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}