﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using OH.DAL.Domain;
using OH.DAL.Domain.ProgramProfile.Caregiver;
using OH.DAL.Domain.WorkflowTemplates;
using OH.DAL.Repositories.ProgramProfiles.CareGiver;

namespace OH.DAL.Repositories.WorkFlow
{
    public class WorkFlowItemRepository : Repository<WorkflowItem>, IWorkFlowItemRepository
    {
        public WorkFlowItemRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public void saveWorkflowItem_ProfileCaregiver(Guid profileId)
        {
            using (MyOhContext db = new MyOhContext())
            {
                ProfileCaregiverRepository pc = new ProfileCaregiverRepository(new MyOhContext());
                var cc = pc.GetByIdNoTracking(profileId); 
                ICollection<ProfileCaregiver> pcCol = new Collection<ProfileCaregiver>();

                db.Entry(cc).State = EntityState.Modified;
                pcCol.Add(cc);

                var workFlow = new Workflow
                {
                    Summary = "Initial Caregiver Inquiry",
                    Title = "Initial Caregiver Inquiry",
                    WorkFlowItems = new Collection<WorkflowItem>()
                };

                workFlow.WorkFlowItems.Add(new WorkflowItem()
                {
                    Title = "Initial Caregiver Inquiry",
                    Start = DateTime.Now,
                    End=DateTime.Now,
                    OrderId=0,
                    ParentWorkFlowItemId = null,
                    ProfilesCaregiver = pcCol,
                    Expanded = true,
                    PercentComplete = Convert.ToDecimal(50.00)
                });

                
                
                db.Workflow.Add(workFlow);
                db.SaveChanges();
            }


        }
    }
}