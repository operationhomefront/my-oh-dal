﻿using OH.DAL.Domain;
using OH.DAL.Domain.WorkflowTemplates;

namespace OH.DAL.Repositories.WorkFlow
{
    public interface IWorkFlowItemAssignmentRepository : IRepository<WorkflowItemAssignment>
    {

    }
}
