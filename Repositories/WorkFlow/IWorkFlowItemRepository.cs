﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using OH.DAL.Domain;
using OH.DAL.Domain.WorkflowTemplates;

namespace OH.DAL.Repositories.WorkFlow
{
    public interface IWorkFlowItemRepository : IRepository<WorkflowItem>
    {
        void saveWorkflowItem_ProfileCaregiver(Guid profileId);
    }
}
