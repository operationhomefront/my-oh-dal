﻿using OH.DAL.Domain;
using OH.DAL.Domain.WorkflowTemplates;

namespace OH.DAL.Repositories.WorkFlow
{
    public class WorkFlowItemAssignmentRepository : Repository<WorkflowItemAssignment>, IWorkFlowItemAssignmentRepository
    {
        public WorkFlowItemAssignmentRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}