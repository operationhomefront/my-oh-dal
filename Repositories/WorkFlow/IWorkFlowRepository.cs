﻿using OH.DAL.Domain;

namespace OH.DAL.Repositories.WorkFlow
{
    public interface IWorkFlowRepository : IRepository<DAL.Domain.WorkflowTemplates.Workflow>
    {

    }
}
