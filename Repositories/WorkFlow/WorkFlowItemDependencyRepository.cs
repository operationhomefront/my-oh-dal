﻿using OH.DAL.Domain;
using OH.DAL.Domain.WorkflowTemplates;

namespace OH.DAL.Repositories.WorkFlow
{
    public class WorkFlowItemDependencyRepository : Repository<WorkflowItemDependency>, IWorkFlowItemDependencyRepository
    {
        public WorkFlowItemDependencyRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}