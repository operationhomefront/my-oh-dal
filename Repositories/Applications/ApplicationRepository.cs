﻿using OH.DAL.Domain.Applications;

namespace OH.DAL.Repositories.Applications
{
    public class ApplicationRepository : Repository<Application>, IApplicationRepository
    {
        public ApplicationRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}