﻿using System;
using OH.DAL.Domain.Applications;

namespace OH.DAL.Repositories.Applications
{
    public interface IApplicationClassRepository : IRepository<ApplicationClass>
    {
        ApplicationClass GetByIdInclude(Guid id);
    }
}
