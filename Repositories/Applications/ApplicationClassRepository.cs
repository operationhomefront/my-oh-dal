﻿using System;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Applications;

namespace OH.DAL.Repositories.Applications
{
    public class ApplicationClassRepository : Repository<ApplicationClass>, IApplicationClassRepository
    {
        public ApplicationClassRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public ApplicationClass GetByIdInclude(Guid id)
        {
            return DbSet.Where(ac => ac.Id.Equals(id)).Include(a => a.Application).FirstOrDefault();            
        }
    }
}