﻿using OH.DAL.Domain.Applications;

namespace OH.DAL.Repositories.Applications
{
    public interface IApplicationRepository : IRepository<Application>
    {

    }
}
