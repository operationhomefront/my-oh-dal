﻿using OH.DAL.Domain.ChangeLogs;

namespace OH.DAL.Repositories.ChangeLogs
{
    public class ChangeLogRepository : Repository<ChangeLog>, IChangeLogRepository
    {
        public ChangeLogRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {

        }


    }
}
