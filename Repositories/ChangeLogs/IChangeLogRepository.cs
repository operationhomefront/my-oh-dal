﻿using OH.DAL.Domain.ChangeLogs;

namespace OH.DAL.Repositories.ChangeLogs
{
    public interface IChangeLogRepository : IRepository<ChangeLog>
    {

    }
}
