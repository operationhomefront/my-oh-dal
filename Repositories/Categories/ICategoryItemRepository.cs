﻿using OH.DAL.Domain;
using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Categories
{
    public interface ICategoryItemRepository : IRepository<CategoryItem>
    {

    }
}
