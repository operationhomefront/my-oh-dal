﻿using System;
using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Categories
{
    public interface ICategoryListTypeRepository : IRepository<CategoryListType>
    {
        CategoryListType GetByIdInclude(Guid id);
    }
}
