﻿using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Categories
{
    public class CategoryItemRepository : Repository<CategoryItem>, ICategoryItemRepository
    {
        public CategoryItemRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }
    }
}