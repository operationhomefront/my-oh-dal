﻿using System;
using System.Collections;
using System.Collections.Generic;
using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Categories
{
    public interface ICategoryAttachmentRepository:  IRepository<CategoryAttachment>
    {
        CategoryAttachment GetByIdInclude(Guid id);
        bool RemoveAttachmentByOwnerFileAndCategory(Guid ownerId, string fileName, Guid categoryId);

        IEnumerable<CategoryAttachment> GetByOwnerAndCategory(Guid ownerId, Guid categoryId);
    }
}
