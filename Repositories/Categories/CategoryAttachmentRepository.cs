﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Runtime.CompilerServices;
using OH.DAL.Domain.Categories;
using OH.DAL.Repositories.Attachments;

namespace OH.DAL.Repositories.Categories
{
    public class CategoryAttachmentRepository : Repository<CategoryAttachment>, ICategoryAttachmentRepository
    {
        private readonly IAttachmentRepository _attachmentRepository;

        public CategoryAttachmentRepository(MyOhContext myOhContext, IAttachmentRepository attachmentRepository)
            : base(myOhContext)
        {
            _attachmentRepository = attachmentRepository;
        }

        public CategoryAttachment GetByIdInclude(Guid id)
        {
            return DbSet.Where(i => i.Id.Equals(id)).Include(c => c.Category).Include(a => a.Attachment).FirstOrDefault();
        }

        public bool RemoveAttachmentByOwnerFileAndCategory(Guid ownerId, string fileName, Guid categoryId)
        {
            //first find the file
            var file = _attachmentRepository.GetByOwnerAndFile(ownerId, fileName);
            if (file != null)
            {
                var ca = DbSet.SingleOrDefault(m => m.AttachmentId == file.Id && m.CategoryId == categoryId);
                if (ca != null)
                {
                    DbSet.Remove(ca);
                    Save();
                    //next remove the file
                    _attachmentRepository.Delete(file.Id);
                    _attachmentRepository.Save();
                }
            }
            return true;
        }


        public IEnumerable<CategoryAttachment> GetByOwnerAndCategory(Guid ownerId, Guid categoryId)
        {
            //var listFilter = _attachmentRepository.
            var vals = _attachmentRepository.GetByOwner(ownerId);
            var rtn = DbSet.Include(i => i.Attachment).Where(w =>w.CategoryId==categoryId &&  vals.Contains(w.AttachmentId));
            return rtn;
           
        }
    }
}