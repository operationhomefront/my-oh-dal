﻿using System;
using System.Collections;
using System.Collections.Generic;
using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Categories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Guid AddOrUpdateSubCategoryParentCategory(Category subCategory, Category parentCategory);
        Category GetCategoryByName(string categoryName);

        //IEnumerable<Category> GetSubCategoriesByParentCategoryId(Guid parentCategoryId);

        Category GetCategoryByIdInclude(Guid categoryId);

        Category GetCategoryByOwnerId(Guid ownerId);

        IEnumerable<Category> GetAllByOwnerIdAndCategoryId(Guid ownerId, Guid categoryId);        
        
    }

}
