﻿using System;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Categories
{
    public class CategoryListTypeRepository : Repository<CategoryListType>, ICategoryListTypeRepository
    {
        public CategoryListTypeRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public CategoryListType GetByIdInclude(Guid id)
        {
            return DbSet.Where(i => i.Id.Equals(id)).Include(c=>c.Category).Include(l=>l.ListType).FirstOrDefault();
        }    
    }
}