﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OH.DAL.Domain.Categories;

namespace OH.DAL.Repositories.Categories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(MyOhContext myOhContext)
            : base(myOhContext)
        {
        }

        public Guid AddOrUpdateSubCategoryParentCategory(Category subCategory, Category parentCategory)
        {
            //TODO: Review with Roy, This can be used if we want to have unique category names, I dont think this is ideal at the moment
            /* if (parentCategory.Id.Equals(Guid.Empty))
            {
                //Check if category with the same name exists, and get it if it does
                var parentCategoryFromDatabase = GetCategoryByName(parentCategory.Name);

                if (parentCategoryFromDatabase == null)
                {
                    parentCategory.Id = (Guid)AddOrUpdate(parentCategory);
                    Save();
                }

                if (subCategory.Id.Equals(Guid.Empty))
                {
                    //Check if subCategory with the same name exists, and get it if it does
                    var subCategoryFromDatabase = GetCategoryByName(subCategory.Name);

                    if (subCategoryFromDatabase == null)
                    {
                        subCategory.Id = (Guid)AddOrUpdate(subCategory);
                        Save();
                    }
                }


            }*/

            //If Parent Category Id is empty, save the parent category
            //if (parentCategory.Id.Equals(Guid.Empty))
            //{
            //    parentCategory.Id = (Guid) AddOrUpdate(parentCategory);
            //    Save();
            //}

            subCategory.ParentCategory = parentCategory;

            return AddOrUpdate(subCategory);
        }

        public Category GetCategoryByName(string categoryName)
        {
            return DbSet.FirstOrDefault(c => c.Name.Trim().Equals(categoryName.Trim()));
        }

        //public IEnumerable<Category> GetSubCategoriesByParentCategoryId(Guid parentCategoryId)
        //{
        //    var parentCategory =
        //        DbSet.Where(c => c.Id.Equals(parentCategoryId)).Include(s => s.SubCategories).FirstOrDefault();

        //    return parentCategory == null ? null : parentCategory.SubCategories.ToList();
        //}

        public Category GetCategoryByIdInclude(Guid categoryId)
        {
            //return DbSet.Where(c => c.Id == (categoryId.HasValue ? categoryId : new Guid("31f61a13-2d6a-457d-8e20-262b1813b621"))).ToList();
            //return DbSet.Include(a=>a.CategoryAttachments.Select(c=>c.Attachment)).FirstOrDefault(c => c.Id == categoryId);
            //var category = DbSet.Include("CategoryAttachments.Attachment").Where(c => c.Id == categoryId).ToList();
            return DbSet.Where(i => i.Id.Equals(categoryId)).Include(s=>s.SubCategories).Include("CategoryAttachments.Attachment").FirstOrDefault();

            //return
            //    DbSet.Where(c => c.Id.Equals(categoryId))
            //        .Select(
            //            c =>
            //                new
            //                {
            //                    c.Id,
            //                    c.Name,
            //                    SubCategories =
            //                        c.SubCategories.Select(cc => new {cc.Id, cc.Name, cc.SubCategories})
            //                            .Union<object>(
            //                                c.Attachments.Select(
            //                                    ca =>
            //                                        new {ca.Id, Name = ca.FileName, SubCategories = new List<object>()}))
            //                })
            //        .ToList();
        }

        public Category GetCategoryByOwnerId(Guid ownerId)
        {
            var result = DbSet.Where(i => i.Owner.Id.Equals(ownerId)).Include(s => s.SubCategories).Include("CategoryAttachments.Attachment").FirstOrDefault();

            return result;
        }

        public IEnumerable<Category> GetAllByOwnerIdAndCategoryId(Guid ownerId, Guid categoryId)
        {
            return DbSet.Where(x => x.Id.Equals(categoryId) && x.Owner.Id.Equals(ownerId)).AsEnumerable();
        }
    }
}