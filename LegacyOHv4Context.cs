﻿using System;
using System.Data.Entity;
using OH.DAL.Domain;

namespace OH.DAL
{
    public class LegacyOHv4Context : DbContext
    {
        public LegacyOHv4Context() : base("LegacyOHv4Context")
        {
            Database.SetInitializer(new LegacyOHv4DatabaseInitializer());
            //Database.Initialize(true);            

            //Add all dbsets here to access via ClassGuid
            //AllEntities = new List<object>();
            //AllEntities.Add(Attachments.ToList());
            //AllEntities.Add(Categories.ToList());
            
        }

        public DbSet<USZipCode> USZipCodes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties().Where(p => p.Name == "ZipCode").Configure(p => p.IsKey());
            modelBuilder.Properties<String>().Configure(p => p.HasMaxLength(250));

            }

        public class LegacyOHv4DatabaseInitializer : DropCreateDatabaseIfModelChanges<LegacyOHv4Context>
        {
            public LegacyOHv4DatabaseInitializer(): base()
            {
            }

            protected override void Seed(LegacyOHv4Context context)
            {
                base.Seed(context);
            }


            public override void InitializeDatabase(LegacyOHv4Context context)
            {
                //TODO: this did not work, find out why
                //This will not prevent the script from running when there are open connections
                //context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                //    "ALTER DATABASE [" + context.Database.Connection.Database +
                //    "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
                base.InitializeDatabase(context);
                //if (context.Database.Exists())
                //{  
                //    //This will prevent the script from running when there are open connections
                //    context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "ALTER DATABASE [" + context.Database.Connection.Database + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");

                //    context.Database.Delete();
                //}

                //context.Database.Create();

                //TODO: Automate this in the future

                //context.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Categories] ADD  CONSTRAINT [UK_Categories_Id] UNIQUE NONCLUSTERED ( 	[Id] ASC ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]");


                //context.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Attachments] ADD  CONSTRAINT [UK_Attachments_Id] UNIQUE NONCLUSTERED ( [Id] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]");
                //context.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Attachments]  WITH CHECK ADD  CONSTRAINT [FK_Attachments_Categories] FOREIGN KEY([CategoryId]) REFERENCES [dbo].[Categories] ([Id]); ALTER TABLE [dbo].[Attachments] CHECK CONSTRAINT [FK_Attachments_Categories]");
            }
        }

       
    }
}