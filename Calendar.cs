﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations.Schema;

namespace OH.DAL
{
    [Serializable]
    [Table("Calendars")] 
    public class Calendar
    {

        public Calendar()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public static Guid ClassGuid
        {
            get { return new Guid("b5ac9303-e9da-45ef-959e-d14f5d823d3a"); }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClusterId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; private set; }
        
    }
}
